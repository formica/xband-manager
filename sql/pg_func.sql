
--
-- Name: delete_xband_db(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE FUNCTION public.delete_xband_db() RETURNS character
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag char;
   BEGIN
      statusflag = '1';
      delete from xband_packet;
      delete from xband_binary_packet;
      delete from l0c_datfile;
      delete from l0d_file;
      delete from l0c_metadata;
      --delete from xband_packet_id;
      --delete from xband_apid;
      delete from tar_file;

      RETURN statusflag;
   END;
   $$;


ALTER FUNCTION public.delete_xband_db() OWNER TO svom_cea;

--
-- Name: delete_packetsbylcfilename(); Type: FUNCTION; Schema: public; Owner: svom_cea
-- Remarks: should not put any commit statement in functions. They already start a transaction.
--

CREATE OR REPLACE FUNCTION public.delete_packetsbylcfilename(lcfile character varying) RETURNS INTEGER
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag INTEGER;
     rec record;
     subrec record;
     query text;
     subqry text;
     lcfid numeric;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from l0c_datfile where lcfilename = $1';
	  for rec in execute query using lcfile
        loop
            statusflag := statusflag + 1;
            lcfid := rec.lcfile_id;
            raise notice 'remove frames, binary and notifications for lcfile id: %', lcfid;
            delete from xband_packet where lcfile_id = lcfid;
            delete from xband_binary_packet where lcfile_id = lcfid;
            delete from xband_notification where lcfile_id = lcfid;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing packets for lcfilename %', lcfile;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;

--
-- Name: delete_packetsbytarname(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE OR REPLACE FUNCTION public.delete_packetsbytarname(tname character varying) RETURNS INTEGER
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag INTEGER;
     rec record;
     subrec record;
     query text;
     subqry text;
     ndat INTEGER;
     lcfilename character varying;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from tar_file where tarname = $1';
	  for rec in execute query using tname
        loop
            subqry := 'select * from l0c_datfile where tarname = $1';
            for subrec in execute subqry using rec.tarname
                loop
                    statusflag := statusflag + 1;
                    lcfilename := subrec.lcfilename;
                    raise notice 'remove all content for l0c file: %', lcfilename;
                    select into ndat delete_packetsbylcfilename(lcfilename);
                    raise notice 'content of l0c removed : %', ndat;
            end loop;
            raise notice 'remove datfiles for tar: %', rec.tarname;
            delete from l0c_datfile ldf where ldf.tarname = rec.tarname;
      end loop;
      raise notice 'remove tarfile and metadata for %', tname;
      delete from l0c_metadata where tarname = tname;
      raise notice 'removed metadata';
      delete from tar_file where tarname = tname;
      raise notice 'removed tar';
      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing tar file %', tname;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


ALTER FUNCTION public.delete_packetsbylcfilename OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbylcfilename(character varying) TO xband_w;

ALTER FUNCTION public.delete_packetsbytarname OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbytarname(character varying) TO xband_w;
