--
-- PostgreSQL database dump
--

-- Dumped from database version 11.10 (Debian 11.10-1.pgdg90+1)
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: frame_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.frame_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frame_sequence OWNER TO svom_cea;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: l0c_datfile; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.l0c_datfile (
    lcfile_id bigint NOT NULL,
    apid_hexa character varying(255),
    end_time character varying(255),
    end_time_ts timestamp without time zone,
    error integer,
    lcfilepath character varying(255),
    filesize bigint,
    index integer,
    is_name_conform boolean,
    lcfilename character varying(255),
    orbit integer,
    vcid integer,
    reception_time timestamp without time zone,
    start_time character varying(255),
    start_time_ts timestamp without time zone,
    station character varying(255),
    type character varying(255),
    version character varying(255),
    tarname character varying(255)
);


ALTER TABLE public.l0c_datfile OWNER TO svom_cea;

--
-- Name: l0c_metadata; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.l0c_metadata (
    metadata_id bigint NOT NULL,
    address character varying(255),
    apid character varying(255),
    comment character varying(255),
    conf_level character varying(255),
    conf_visibility character varying(255),
    email character varying(255),
    fax character varying(255),
    gaps_percent character varying(255),
    has_gaps character varying(255),
    instrument character varying(255),
    is_conform boolean,
    level character varying(255),
    organization character varying(255),
    origin_time character varying(255),
    packet_length character varying(255),
    product_id character varying(255),
    product_md5 character varying(255),
    product_version character varying(255),
    production_date character varying(255),
    producer character varying(255),
    quality_level character varying(255),
    software_version character varying(255),
    satellite character varying(255),
    station_code character varying(255),
    tel character varying(255),
    sys_time character varying(255),
    total_packets character varying(255),
    type character varying(255),
    valid_packets character varying(255),
    vcid_name character varying(255),
    vcid_num character varying(255),
    antenna_code character varying(50),
    tarname character varying(255) NOT NULL
);


ALTER TABLE public.l0c_metadata OWNER TO svom_cea;

--
-- Name: l0d_file; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.l0d_file (
    ldfilename character varying(255) NOT NULL,
    ldurl character varying(255),
    nbpkt integer,
    pkt_id integer,
    tarname character varying(255)
);


ALTER TABLE public.l0d_file OWNER TO svom_cea;

--
-- Name: lc_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.lc_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lc_sequence OWNER TO svom_cea;

--
-- Name: lcmeta_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.lcmeta_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lcmeta_sequence OWNER TO svom_cea;

--
-- Name: notif_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.notif_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notif_sequence OWNER TO svom_cea;

--
-- Name: tar_file; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.tar_file (
    tarname character varying(255) NOT NULL,
    apidhexa character varying(255),
    is_conform boolean,
    nbdatfile integer,
    start_time character varying(255),
    start_time_ts timestamp without time zone,
    station character varying(255),
    orbit integer,
    reception_time timestamp without time zone,
    tarpath character varying(255)
);


ALTER TABLE public.tar_file OWNER TO svom_cea;

--
-- Name: xband_apid; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.xband_apid (
    apid integer NOT NULL,
    category character varying(50),
    class character varying(200),
    expected_packets integer,
    instrument character varying(100),
    descr character varying(100),
    alias character varying(100),
    details character varying(4000),
    name character varying(100) NOT NULL,
    pcat integer,
    pid integer,
    priority integer,
    tid integer,
    destination character varying(200)
);


ALTER TABLE public.xband_apid OWNER TO svom_cea;

--
-- Name: xband_binary_packet; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.xband_binary_packet (
    hash_id character varying(255) NOT NULL,
    packet bytea
);


ALTER TABLE public.xband_binary_packet OWNER TO svom_cea;

--
-- Name: xband_packet; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.xband_packet (
    frame_id bigint NOT NULL,
    hash_id character varying(512),
    ccsds_count integer,
    ccsds_gflag integer,
    ccsds_headflag integer,
    ccsds_plength integer,
    ccsds_type integer,
    ccsds_version integer,
    flag integer,
    is_duplicate boolean,
    is_valid boolean,
    is_known boolean,
    time_counter bigint,
    obsid bigint,
    obsidnum integer,
    obsidtype integer,
    packet_id integer NOT NULL,
    timesecond bigint,
    pkt_order integer,
    timemicro integer,
    apid integer NOT NULL,
    lcfile_id bigint NOT NULL
);


ALTER TABLE public.xband_packet OWNER TO svom_cea;

--
-- Name: xband_packet_id; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.xband_packet_id (
    packet_id integer NOT NULL,
    packet_name character varying(100),
    packet_notification character varying(100),
    apid integer NOT NULL
);


ALTER TABLE public.xband_packet_id OWNER TO svom_cea;

CREATE TABLE public.xband_notification (
    notif_id bigint NOT NULL,
    insertion_time timestamp without time zone,
    notif_size integer,
    notif_message character varying(1024),
    notif_name character varying(30),
    notif_format character varying(30),
    lcfile_id bigint NOT NULL
);

ALTER TABLE public.xband_notification OWNER TO svom_cea;

--
-- Name: xband_notification xband_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_notification
    ADD CONSTRAINT xband_notification_pkey PRIMARY KEY (notif_id);

--
-- Name: xband_notification fk_xband_notification_l0c_datfile_lcfileid; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_notification
    ADD CONSTRAINT fk_xband_notification_l0c_datfile_lcfileid FOREIGN KEY (lcfile_id) REFERENCES public.l0c_datfile(lcfile_id);

--
-- Name: l0c_datfile l0c_datfile_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0c_datfile
    ADD CONSTRAINT l0c_datfile_pkey PRIMARY KEY (lcfile_id);


--
-- Name: l0c_metadata l0c_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0c_metadata
    ADD CONSTRAINT l0c_metadata_pkey PRIMARY KEY (metadata_id);


--
-- Name: l0d_file l0d_file_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0d_file
    ADD CONSTRAINT l0d_file_pkey PRIMARY KEY (ldfilename);


--
-- Name: tar_file tar_file_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.tar_file
    ADD CONSTRAINT tar_file_pkey PRIMARY KEY (tarname);


--
-- Name: l0c_datfile uk_66uug01virb3x2mgjh98uksmc; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0c_datfile
    ADD CONSTRAINT uk_lcfilename UNIQUE (lcfilename);


--
-- Name: xband_apid uk_ms28jaukj08lh0glvf04ek0w1; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_apid
    ADD CONSTRAINT uk_name UNIQUE (name);


--
-- Name: xband_apid xband_apid_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_apid
    ADD CONSTRAINT xband_apid_pkey PRIMARY KEY (apid);


--
-- Name: xband_binary_packet xband_binary_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_binary_packet
    ADD CONSTRAINT xband_binary_packet_pkey PRIMARY KEY (hash_id);


--
-- Name: xband_packet_id xband_packet_id_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet_id
    ADD CONSTRAINT xband_packet_id_pkey PRIMARY KEY (packet_id);


--
-- Name: xband_packet xband_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet
    ADD CONSTRAINT xband_packet_pkey PRIMARY KEY (frame_id);


--
-- Name: l0d_file fk_l0d_file_tar_file_tarname; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0d_file
    ADD CONSTRAINT fk_l0d_file_tar_file_tarname FOREIGN KEY (tarname) REFERENCES public.tar_file(tarname);


--
-- Name: l0c_metadata fk_l0d_metadata_tar_file_tarname; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0c_metadata
    ADD CONSTRAINT fk_l0d_metadata_tar_file_tarname FOREIGN KEY (tarname) REFERENCES public.tar_file(tarname);


--
-- Name: xband_packet fk72cw3lodxn4ug1bfr4krbv9ao; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet
    ADD CONSTRAINT fk_xband_packet_xband_binary_packet_hashid FOREIGN KEY (hash_id) REFERENCES public.xband_binary_packet(hash_id);


--
-- Name: xband_packet fkb4mannqu6c9nvwlk4jxodut7x; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet
    ADD CONSTRAINT fk_xband_packet_l0c_datfile_lcfileid FOREIGN KEY (lcfile_id) REFERENCES public.l0c_datfile(lcfile_id);


--
-- Name: l0c_datfile fk_l0d_metadata_tar_file_tarname; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.l0c_datfile
    ADD CONSTRAINT fk_l0c_datfile_tar_file_tarname FOREIGN KEY (tarname) REFERENCES public.tar_file(tarname);


--
-- Name: xband_packet fk_xband_packet_xband_apid_apid; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet
    ADD CONSTRAINT fk_xband_packet_xband_apid_apid FOREIGN KEY (apid) REFERENCES public.xband_apid(apid);


--
-- Name: xband_packet_id fk_xband_packet_id_xband_apid_apid; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.xband_packet_id
    ADD CONSTRAINT fk_xband_packet_id_xband_apid_apid FOREIGN KEY (apid) REFERENCES public.xband_apid(apid);


--
-- Name: TABLE l0c_datfile; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.l0c_datfile TO xband_w;


--
-- Name: TABLE l0c_metadata; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.l0c_metadata TO xband_w;


--
-- Name: TABLE l0d_file; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.l0d_file TO xband_w;


--
-- Name: TABLE tar_file; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.tar_file TO xband_w;


--
-- Name: TABLE xband_apid; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.xband_apid TO xband_w;


--
-- Name: TABLE xband_binary_packet; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.xband_binary_packet TO xband_w;


--
-- Name: TABLE xband_packet; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.xband_packet TO xband_w;


--
-- Name: TABLE xband_packet_id; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.xband_packet_id TO xband_w;

--
-- Name: SEQUENCE frame_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.frame_sequence TO xband_w;

--
-- Name: SEQUENCE lc_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.lc_sequence TO xband_w;

--
-- Name: SEQUENCE lcmeta_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.lcmeta_sequence TO xband_w;

--
-- Name: TABLE xband_notification; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.xband_notification TO xband_w;

--
-- Name: SEQUENCE notif_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.notif_sequence TO xband_w;

--
-- PostgreSQL database dump complete
--

