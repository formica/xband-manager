package test.xml;

import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class RegexpExample {

    public static void main(String[] args) {

        String XML_REGEXP = "SVOM_([A-Z0-9]+)_([A-Z]+)_(\\d+)_(\\w+)_(\\d+)_(\\w+)"
                            + ".(xml|XML)";
        String filename = "SVOM_LOC_ED_0654_20210131T084346_000511_HK00M.xml";
        final Pattern pattern = Pattern.compile(XML_REGEXP);
        final Matcher matcher = pattern.matcher(filename);
        Boolean m = matcher.matches();
        System.out.println("is matching gives " + m);

    }
}

