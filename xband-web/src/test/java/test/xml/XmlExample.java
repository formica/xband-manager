package test.xml;

import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

@Slf4j
public class XmlExample {

    public static void main(String[] args) {
        try {
            File xmlfile = new File("/tmp/tar.xml");
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder dBuilder = dbf.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlfile);
            log.info("Doc: {}", xmlfile);
            doc.getDocumentElement().normalize();
            /** get satellite meta-data */
            log.info("Root: {}", doc.getDocumentElement().getNodeName());
            NodeList typelist = doc.getElementsByTagName("Type");
            log.info("type is {}", typelist.item(0).getTextContent());
            NodeList list = doc.getElementsByTagName("DatFile");
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    log.info("element: {}", element);
                    String fname = element.getTextContent();
                    log.info("File: {}", fname);
                }
            }
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

