package fr.svom.xband.tools;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.Random;

@Slf4j
public class DataGeneratorEntity {

    private final Random rnd = new Random();

    public void fillRandom(Object obj, Class<?> clazz) {
        try {
            Method[] publicMethods = clazz.getMethods();
            for (Method aMethod : publicMethods) {
                if (aMethod.getName().startsWith("set")
                    && aMethod.getParameterCount() == 1) {
                    Class<?> argtype = aMethod.getParameterTypes()[0];
                    if (argtype.equals(Double.class)) {
                        Double val = rnd.nextDouble();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Float.class)) {
                        Float val = rnd.nextFloat();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(BigDecimal.class)) {
                        aMethod.invoke(obj, BigDecimal.valueOf(rnd.nextDouble()));
                    } else if (argtype.equals(Long.class)) {
                        Long val = rnd.nextLong();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Integer.class)) {
                        Integer val = rnd.nextInt();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(String.class)) {
                        String val = String.valueOf(rnd.nextInt()); // TODO generate better string
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Date.class)) {
                        Date val = Date.from(Instant.ofEpochMilli(rnd.nextLong()));
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Timestamp.class)) {
                        Timestamp val = Timestamp.from(Instant.ofEpochMilli(rnd.nextLong()));
                        aMethod.invoke(obj, val);
                    } else {
                        log.warn("fillRandom: not calling setter method {}", aMethod);
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new InternalError(e);
        }
    }

    public Object createPojo(Class<?> pojoType) {
        try {
            Object item = pojoType.getDeclaredConstructor().newInstance();
            fillRandom(item, pojoType);
            return item;
        } catch (Exception e) {
            throw new InternalError(e);
        }
    }

}
