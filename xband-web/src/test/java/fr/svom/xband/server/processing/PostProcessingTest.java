package fr.svom.xband.server.processing;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PostProcessingTest {

    @Autowired
    public LcFileProcessing lcFileProcessing;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testsendNotification() {
        String res= lcFileProcessing.sendNotif("fileName");
        assertEquals("fileName", res);
    }
}
