package fr.svom.xband.server.swagger.api.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.svom.xband.data.model.ApidPacket;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApidsApiServiceImplTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private ApidPacket apidPacket;

    private ObjectMapper mapper;

    /**
     * logger.
     */
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public void setUp() throws Exception {

        mapper = new ObjectMapper();
        apidPacket = new ApidPacket();
        apidPacket.setApidValue(1617);
        apidPacket.setInstrument("ECL");
        apidPacket.setCategory("SCIENCE");
        apidPacket.setTid(1);
        apidPacket.setPid(18);
        apidPacket.setPcat(17);

    }

    /**
     *Test method for {@link ApidsApiServiceImpl
     * Test API: api/apids
     * (POST with json body: apidPacket)
     */
    @Test
    public void test1CreatePacketApid() {
        HttpEntity<String> entity = null;
        HttpHeaders headers =null;
        try {
            String jsonInString = mapper.writeValueAsString(apidPacket);
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            entity = new HttpEntity<String>(jsonInString, headers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        log.info("Create new packet APID {}", apidPacket);
        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/apids",
                entity, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        /*** null entity **/
        log.info("Create new packet APID with null entity");
        ResponseEntity<String> responseErr = testRestTemplate.postForEntity("/api/apids",
                null, String.class);
        assertThat(responseErr.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        /*** error entity **/
        log.info("Create new packet APID with error entity (not JSON)");
        entity = new HttpEntity<String>("apidPacket", headers);
        ResponseEntity<String> respExc = testRestTemplate.postForEntity("/api/apids",
                entity, String.class);
        assertThat(respExc.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }
    /**
     *Test method for {@link ApidsApiServiceImpl
     * Test API: api/apids/{id}
     * (GET with id : path parameter)
     */
    @Test
    public void test2FindPacketApids() {
        /** test id not exist **/
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/apids/5",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThan(HttpStatus.OK);

        /** test id exist **/
        final ResponseEntity<String> resp= testRestTemplate.exchange("/api/apids/1617",
                HttpMethod.GET, null, String.class);
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    /**
     *Test method for {@link ApidsApiServiceImpl
     * Test API: api/apids
     * (GET)
     */
    @Test
    public void test3ListPacketApids() {
        log.info(" ********* test3ListPacketApids ");
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/apids",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
