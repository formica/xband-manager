package fr.svom.xband.server.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DataHeaderPacketServiceTest {

    @Autowired
    DataHeaderPacketService dataHeaderPacketService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGetTime() {
        double expected=3254578.910235;
        assertEquals(expected ,dataHeaderPacketService.getTime(3254578, 910235), 0.0001 );
    }

    @Test
    public void testGetTargetIdHexa() {
        assertEquals("ffffffff", dataHeaderPacketService.getTargetIdHexa(255, 16777215));
    }

    @Test
    public void testIsSciencePacket() {
        assertEquals(true, dataHeaderPacketService.isSciencePacket("FFFFFFFF"));
        assertEquals(true, dataHeaderPacketService.isSciencePacket("ffffffff"));
    }

    @Test
    public void testHexObsidStr() {
        assertEquals("ffffffff", dataHeaderPacketService.hexObsidStr(255, 16777215));
    }

    @Test
    public void testLongObsid() {
        assertEquals(4294967295L, dataHeaderPacketService.longObsid(255, 16777215));
    }

    @Test
    public void testUnsignedObsid() {
        assertEquals(-1, dataHeaderPacketService.unsignedObsid(255, 16777215));
    }

    @Test
    public void testFullObsidUnsignedInt() {
        assertEquals("4294967295", dataHeaderPacketService.fullObsidUnsignedInt(255, 16777215));
    }


}
