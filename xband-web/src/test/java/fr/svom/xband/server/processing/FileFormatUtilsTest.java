package fr.svom.xband.server.processing;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class FileFormatUtilsTest {

    private File archive;

    private  ClassLoader classLoader;

    @Before
    public void setUp() throws Exception {
        classLoader = this.getClass().getClassLoader();
        String tarFileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        archive = new File(classLoader.getResource(tarFileName).getFile());
    }

    @Test
    public void testIsGZipped() {

        String gztarFileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar.gz";
        File gztarfile = new File(classLoader.getResource(gztarFileName).getFile());
        Boolean result = FileFormatUtils.isGZipped(gztarfile);
        assertThat(result).isTrue();

        Boolean resulttar = FileFormatUtils.isGZipped(archive);
        assertThat(resulttar).isFalse();
    }

    @Test
    public void testUntar() throws IOException {

        InputStream in = new FileInputStream(archive);
        File dest = new File("untarArchive");
        FileFormatUtils.untar(in, dest);

        Path path = Path.of("/tmp/xband.test");
        FileFormatUtils.writeFile(path, "test set of binary".getBytes(StandardCharsets.UTF_8));
        assertThat(path).isNotNull();
    }
}
