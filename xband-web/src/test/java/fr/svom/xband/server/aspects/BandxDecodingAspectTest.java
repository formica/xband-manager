package fr.svom.xband.server.aspects;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.xband.server.processing.LcFileProcessing;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BandxDecodingAspectTest {

    @Autowired
    public LcFileProcessing lcFileProcessing;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testsendldMessage() {
        String tarfileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        lcFileProcessing.sendNotif(tarfileName);
        assertEquals(tarfileName, lcFileProcessing.sendNotif(tarfileName));
    }

}
