/**
 *
 */
package fr.svom.xband.server.swagger.api.impl;

import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.server.externals.NatsController;
import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.server.processing.LcFileProcessing;
import fr.svom.xband.server.service.FramesBandxPacketService;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author etrigui
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@//DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class FramesApiServiceImplTest {

    @Mock
    private NatsController natsCtrl;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private LcFileProcessing lcFileProcessing;

    @Autowired
    private FramesBandxPacketService framesBandxPacketService;

    private File tar;

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    private File extractDir;

    private String tarFileName;

    private ClassLoader classLoader;

    /**
     * logger.
     */
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public void setUp() {

        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        classLoader = this.getClass().getClassLoader();
        tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        tar = new File(classLoader.getResource(tarFileName).getFile());

        String FileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001";
        extractDir = new File(classLoader.getResource(FileName).getFile());
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test API: api/frames
     * (POST with String body: fileLocation from ftp)
     */
    @Test
    public void test1saveBandxTransferFramesFromFTP() {
        log.info("******************************test1saveBandxTransferFrames : file location from ftp");

        /** test with file in distant FTP server **/
        final String ftpurl = "ftp://software.svom.fr/pub/";
        final String bodyStr = ftpurl + tarFileName;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(bodyStr, headers);
        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/frames",
                entity, String.class);
        assertThat(response.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        Integer code = response.getStatusCode().value();
        if (code > 500) {
            log.info("** problem with ftp server connection");
        }

        log.info("****************************** test with incorrect ftp server");
        /** test with file in distant FTP server **/
        final String falseftpurl = "ftp://wrong.svom.fr/";
        final String falsebodyStr = falseftpurl + tarFileName;
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers1.add("source", "remote");
        headers1.add("dataType", ".zip");
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("lcFileName", falsebodyStr);
        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                map, headers1);
        ResponseEntity<String> falseResponse = testRestTemplate.postForEntity("/api/frames",
                request, String.class);
        assertThat(falseResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test API: api/frames
     * (POST with String body: fileLocation from http)
     */
    @Test
    public void test1saveBandxTransferFramesFromHTTP() {
        log.info("******************************test1saveBandxTransferFrames : file location from http");
        /** test with file in distant HTTP server/ no adequate file **/
        final String httpurl = "https://files.mediaspip.net/mediaspip_0_2.zip";
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers1.add("source", "remote");
        headers1.add("dataType", ".zip");
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("lcFileName", httpurl);
        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                map, headers1);
        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/frames",
                request, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    /** test with local file
     * Test API: api/frames
     * (POST with body param : file location)
     * **/
    @Test
    public void test2saveBandxTransferFramesLocal() {
        log.info("*************************test2saveBandxTransferFrames: file location local");
        /** test with file in local**/
        final String bodyStrlocalfile = tar.getAbsolutePath();
        final String bodyname = tar.getName();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers1.add("source", "local");
        headers1.add("dataType", "tar");
        final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("lcFileName", bodyname);
        map.add("file", new FileSystemResource(new File(bodyStrlocalfile)));
        final HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(
                map, headers1);
        ResponseEntity<String> response2 = testRestTemplate.postForEntity("/api/frames",
                request, String.class);
        log.info("Received response {}", response2.getBody());
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        log.info("*************************test2saveBandxTransferFrames : bad file location");
        /** test exception (location null)**/
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers2.add("source", "remote");
        headers2.add("dataType", "tar");
        final MultiValueMap<String, String> map2 = new LinkedMultiValueMap<String, String>();
        map.add("lcFileName", null);
        final HttpEntity<MultiValueMap<String, String>> request2 = new HttpEntity<MultiValueMap<String, String>>(
                map2, headers2);
        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/frames",
                request2, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test API: api/frames/export/?apid=&obsid=?
     * (GET with pathParameters)
     */
    @Test
    public void test3ExportBandxTransferFrames() {
        log.info("*********************test3 export TransferFrame");
        ResponseEntity<String> resp = testRestTemplate.exchange("/api/frames?by=apid:1620",
                HttpMethod.GET, null, String.class);
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<String> resp2 =
                testRestTemplate.exchange("/api/frames?by=apid:1620,obsid:16777215",
                        HttpMethod.GET, null, String.class);
        assertThat(resp2.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Test API: api/frames
     * (GET: search list of BandxTransferFrame packets)
     * **/
    @Test
    public void test4ListBandxTransferFrames() {
        log.info("************************test4ListBandxTransferFrames");
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/frames",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test Api: api/frames
     * (POST with header: dataType: archive and body: the File)
     */
    @Test
    public void test5SaveBandxTransferFramesArchive() {
        log.info("***********************test5SaveBandxTransferFrames: datatype = archive");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("source", "local");
        headers.add("dataType", "tar");
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        final FileSystemResource file = new FileSystemResource(tar.getAbsoluteFile());
        map.add("lcFileName", file.getFilename());
        map.add("file", file);
        HttpEntity<MultiValueMap<String, Object>> request =
                new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        log.info("Sending post request for local archive");
        ResponseEntity<String> response = testRestTemplate.postForEntity("/api/frames",
                request, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test Api: api/frames/upload
     * (POST with header: dataType: dat and body: the File)
     */
    @Test
    public void test5SaveBandxTransferFramesDatFile() {
        log.info("***********************test5SaveBandxTransferFrames :datatype = dat");
        HttpHeaders headerdat = new HttpHeaders();
        headerdat.setContentType(MediaType.MULTIPART_FORM_DATA);
        headerdat.add("source", "local");
        headerdat.add("dataType", "dat");
        MultiValueMap<String, Object> mapdat = new LinkedMultiValueMap<String, Object>();
        String datfileName = "SVOM_L0C_ED_0654_20210102T001400_000225_KR000_000_1.DAT";
        File datfile = new File(classLoader.getResource(datfileName).getFile());
        final FileSystemResource dfile = new FileSystemResource(datfile.getAbsoluteFile());
        mapdat.add("lcFileName", dfile.getFilename());
        mapdat.add("file", dfile);
        HttpEntity<MultiValueMap<String, Object>> requestDat =
                new HttpEntity<MultiValueMap<String, Object>>(mapdat, headerdat);
        ResponseEntity<String> responseDat = testRestTemplate.postForEntity("/api/frames",
                requestDat, String.class);
        assertThat(responseDat.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        log.info("*************test5SaveBandxTransferFrames :test for index 2 without index 1*; datatype = dat");
        HttpHeaders headind2 = new HttpHeaders();
        headind2.setContentType(MediaType.MULTIPART_FORM_DATA);
        headind2.add("source", "local");
        headind2.add("dataType", "dat");
        MultiValueMap<String, Object> mapind2 = new LinkedMultiValueMap<String, Object>();
        String ind2Name = "SVOM_L0C_ED_0654_20170207T160259_20170207T170259_000001_KR000_001_2.DAT";
        File ind2file = new File(classLoader.getResource(ind2Name).getFile());
        final FileSystemResource ind2fileSys = new FileSystemResource(ind2file.getAbsoluteFile());
        mapind2.add("lcFileName", ind2fileSys.getFilename());
        mapind2.add("file", ind2fileSys);
        HttpEntity<MultiValueMap<String, Object>> requestind2 =
                new HttpEntity<MultiValueMap<String, Object>>(mapind2, headind2);
        ResponseEntity<String> responseInd2 = testRestTemplate.postForEntity("/api/frames",
                requestind2, String.class);
        assertThat(responseInd2.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     * Test Api: api/frames/upload
     * (POST with header: dataType: dat and body: the File)
     */
    @Test
    public void test5SaveBandxTransferFramesFileNull() {
        log.info("***********************test5SaveBandxTransferFrames : file null");
        HttpHeaders headernull = new HttpHeaders();
        headernull.setContentType(MediaType.MULTIPART_FORM_DATA);
        headernull.add("dataType", "dat");
        MultiValueMap<String, Object> mapNull = new LinkedMultiValueMap<String, Object>();
        mapNull.add("file", null);
        HttpEntity<MultiValueMap<String, Object>> requestNull =
                new HttpEntity<MultiValueMap<String, Object>>(mapNull, headernull);
        ResponseEntity<String> responseNull = testRestTemplate.postForEntity("/api/frames",
                requestNull, String.class);
        assertThat(responseNull.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        /*        log.info("***********************test5SaveBandxTransferFrames_1 : no dtataype param");
        HttpHeaders badheader = new HttpHeaders();
        badheader.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> badMap = new LinkedMultiValueMap<String, Object>();
        badMap.add("file", file);
        HttpEntity<MultiValueMap<String, Object>> requestbad =
                new HttpEntity<MultiValueMap<String, Object>>(badMap, badheader);
        ResponseEntity<String> responsebad = testRestTemplate.postForEntity("/api/frames/upload",
                requestbad, String.class);
        assertThat(responsebad.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
         */
    }

    /**
     * Test method for {@link FramesApiServiceImpl
     **/
    @Test
    public void test6FindBandxTransferFrame() {
        log.info("*********************test6FindBandxTransferFrame");
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/frames?by=id:1",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);

        log.info("***********************test5SaveBandxTransferFrames_1 : test index2 without index1");

        final ResponseEntity<String> respNotFound = testRestTemplate.exchange("/api/frames?by=id:12555",
                HttpMethod.GET, null, String.class);
        assertThat(respNotFound.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);

    }

    @Test
    public void test6FindObsidsApidsPacketIds() {
        log.info("*********************test6FindObsidsApidsPacketIds");
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/frames/obsids?pass_id=0001_KR000",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Test method for
     * @throws AbstractCdbServiceException
     */
    @Test
    public void test7DeleteFramesOfLcFile() throws AbstractCdbServiceException {
        log.info("***********************test7DeleteFramesOfLcFile");
        /** store dat file */

        //final TarFile tarfile = new TarFile(tarFileName, extractDir.getAbsolutePath());
        try {
            framesBandxPacketService.storeBandxPacket("tar", extractDir);
        }
        catch (RuntimeException e) {
            log.error("test7DeleteFramesOfLcFile: cannot create entries from tar " + extractDir.getAbsolutePath());
        }

        /** test*/
        final String bodyStr = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.DAT";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(bodyStr, headers);
        ResponseEntity<String> response = testRestTemplate.exchange("/api/frames?lcFileName="+bodyStr,
                HttpMethod.DELETE, null,
                String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        log.info("***********************test exception7 DeleteFramesOfLcFile: file not exist");
        final String bodyExc = "SVOM_L0C_ED_0654_20170207T160200_000001_002_1.DAT";
        HttpEntity<String> entityExc = new HttpEntity<String>(bodyExc, headers);
        ResponseEntity<String> responseExc = testRestTemplate.exchange("/api/frames?lcFileName="+bodyExc,
                HttpMethod.DELETE, null,
                String.class);
        assertThat(responseExc.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }

    @After
    public void setAfter() {

        File uploadedFile = new File(serverUploadLocationFolder);
        File[] newlistFiles = uploadedFile.listFiles();
        for (int i = 0; i < newlistFiles.length; i++) {
            if (newlistFiles[i].isDirectory()) {
                FileSystemUtils.deleteRecursively(newlistFiles[i]);
            }
            else {
                newlistFiles[i].delete();
            }
        }

    }
}
