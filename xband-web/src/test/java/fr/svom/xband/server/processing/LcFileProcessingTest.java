package fr.svom.xband.server.processing;

import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.LcMetadata;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.server.service.ApidPacketService;
import fr.svom.xband.server.service.LcFileService;
import fr.svom.xband.server.service.TarFileService;
import fr.svom.xband.server.service.UnitBandxPacketService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@Slf4j
public class LcFileProcessingTest {

    private File datfile;

    private File archive;

    private ClassLoader classLoader = this.getClass().getClassLoader();

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    @Autowired
    private LcFileProcessing lcFileProcessing;

    @Autowired
    private LcFileService lcFileService;

    @Autowired
    private ApidPacketService apidPacketService;

    @Autowired
    private UnitBandxPacketService unitBxPacketService;

    @Autowired
    private TarFileService tarFileService;

    @Before
    public void setUp() throws Exception {

        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        String tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        archive = new File(classLoader.getResource(tarFileName).getFile());
        log.info("Loaded tar file {}", archive.getAbsolutePath());
        //tarfile = new TarFile(tarFileName, archive.getAbsolutePath());

        String datFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.DAT";
        datfile = new File(classLoader.getResource(datFileName).getFile());
        log.info("Loaded dat file {}", datfile.getAbsolutePath());
    }

    @Test
    public void testTarFileProcessing() {

        InputStream tarFileStream;
        TarFile tarfile = lcFileProcessing.createTarfile(archive);
        tarfile = tarFileService.storeTarFile(tarfile);
        try {
            tarFileStream = new FileInputStream(archive);
            File extractDir = lcFileProcessing.extractArchive(archive, tarFileStream, serverUploadLocationFolder);
            List<File> fileList = lcFileProcessing.tarfileExtract(extractDir, tarfile);
            assertEquals(1, fileList.size());
            Integer nframes = lcFileProcessing.lcDataProcessing(fileList.get(0), tarfile);
            Integer resExpected = 4;
            assertEquals(resExpected, nframes);
        }
        catch (FileNotFoundException e) {
            log.error("Cannot find file...");
            e.printStackTrace();
        }
        catch (IOException e) {
            log.error("Cannot read archieve...");
            e.printStackTrace();
        }
    }

    @Test(expected = AbstractCdbServiceException.class)
    public void testLcDataProcessingLengthException()
            throws AbstractCdbServiceException {

        /** test empty dat file */
        Integer expected = 0;
        File emptydat =
                new File(classLoader.getResource("SVOM_L0C_ED_0654_20180228T235959_000001_KR000_999_1.DAT").getFile());
        Integer framesEmpty = lcFileProcessing.lcDataProcessing(emptydat, null);
        assertEquals(expected, framesEmpty);
        /** test corrupted dat file */
        File corrupted =
                new File(classLoader.getResource("SVOM_L0C_0640_20180131T235959_000001_KR000_999_1.DAT").getFile());
        Integer frameCorrupt = lcFileProcessing.lcDataProcessing(corrupted, null);
        assertEquals(expected, frameCorrupt);
    }

    @Test(expected = AbstractCdbServiceException.class)
    public void testLcDataProcessingNameException()
            throws AbstractCdbServiceException {
        /** test non conform dat file */
        Integer expected = 0;
        File nonConformdat =
                new File(classLoader.getResource("SVOM_L0C_ED_0654_20170207T160200_000001_000_001.DAT").getFile());
        Integer frameSize = lcFileProcessing.lcDataProcessing(nonConformdat, null);
        assertEquals(expected, frameSize);
    }

    @Test(expected = AbstractCdbServiceException.class)
    public void testLcDataProcessingApidException()
            throws AbstractCdbServiceException {

        /** test with apid exeption */
        String namedatfileBadAPid = "SVOM_L0C_SD_0670_20190128T113059_000026_KR000_002_1.DAT";
        File datfileBadAPid = new File(classLoader.getResource(namedatfileBadAPid).getFile());
        lcFileProcessing.lcDataProcessing(datfileBadAPid, null);
    }

    @Test
    public void testXmlDataProcessing() {

        /*        File emptyXmlFile = new File(classLoader.getResource
        ("SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.xml").getFile());
        LcMetadata emptylcMetadata = lcFileProcessing.xmlDataProcessing(emptyXmlFile);
        assertEquals( null, emptylcMetadata);*/

        File nonConformXmlFile =
                new File(classLoader.getResource("SVOM_L0C_ED_0654_20170207T160200_000001_K000.xml").getFile());
        LcMetadata lcMetadata = lcFileProcessing.xmlDataProcessing(nonConformXmlFile);
        assertEquals("FSC", lcMetadata.getProducer());
    }

    @Test
    public void testExtractArchive() throws IOException {

        InputStream tarFileStream;
        log.info("Test archive extraction for {} into {}", archive.getAbsolutePath(), serverUploadLocationFolder);
        tarFileStream = new FileInputStream(archive);
        File extractDir = lcFileProcessing.extractArchive(archive, tarFileStream, serverUploadLocationFolder);
        log.info("Tar file extracted in dir {}", extractDir.getAbsolutePath());
        File[] fileList = extractDir.listFiles();
        assertEquals(1, fileList.length);

        String gztarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar.gz";
        File gztarfile = new File(classLoader.getResource(gztarFileName).getFile());

        InputStream zipFileStream = new FileInputStream(gztarfile);
        File zipextractDir = lcFileProcessing.extractArchive(gztarfile, zipFileStream, serverUploadLocationFolder);
        File[] zipfileList = zipextractDir.listFiles();
        assertEquals(1, zipfileList.length);

    }

    @Test
    public void testCreateLcFile() {
        TarFile tarfile = lcFileProcessing.createTarfile(archive);
        tarfile = tarFileService.storeTarFile(tarfile);
        Date date = new Date();
        Timestamp rtime = new Timestamp(date.getTime());
        LcDatFile lcdatfile = lcFileProcessing.createLcDatFile(datfile, tarfile, rtime);
        LcDatFile expected = new LcDatFile();
        expected.setIndex(1);
        expected.setFileSize(4088L);
        expected.setLcfileName(datfile.getName());
        expected.setFilePath(datfile.getAbsolutePath());
        expected.setIsFilenameConform(true);
        expected.setType("ED");
        expected.setApidHexa("0654");
        expected.setStartTime("20170207T160200");
        expected.setOrbit(1);
        expected.setStation("KR000");
        expected.setVer("001");
        assertEquals(expected, lcdatfile);
    }

    @Test
    public void testCreateFrames() {
        TarFile tarfile = lcFileProcessing.createTarfile(archive);
        tarfile = tarFileService.storeTarFile(tarfile);
        Date date = new Date();
        Timestamp rtime = new Timestamp(date.getTime());

        LcDatFile lcdatfile = lcFileProcessing.createLcDatFile(datfile, tarfile, rtime);
        LcDatFile stored = lcFileService.storeLcfile(lcdatfile);
        String apidHexa = lcdatfile.getApidHexa();
        Integer nameApid = Integer.parseInt(apidHexa, 16);
        ApidPacket apidPacket;
        try {
            apidPacket = apidPacketService.findApid(nameApid);
        }
        catch (AbstractCdbServiceException e) {
            log.warn("Create apid");
            apidPacket = new ApidPacket();
            apidPacket.setApidValue(nameApid);
            apidPacket = apidPacketService.registerApid(apidPacket);
        }
        log.info("use apid {}", apidPacket);
        Integer framesize = lcFileProcessing.createAndStoreFrames(datfile, stored, apidPacket);
        Integer expected = 4;
        assertEquals(framesize, expected);

        /** test to throw exception **/
        log.info("Try to load a file which does not exists...");
        File falsefile = new File("file");
        lcFileProcessing.createAndStoreFrames(falsefile, stored, apidPacket);
    }

    /**
     * delete created and uploaded files for tests
     **/
    @After
    public void setAfter() {
        File uploadedFile = new File(serverUploadLocationFolder);
        File[] newlistFiles = uploadedFile.listFiles();
        for (int i = 0; i < newlistFiles.length; i++) {
            if (newlistFiles[i].isDirectory()) {
                FileSystemUtils.deleteRecursively(newlistFiles[i]);
            }
            else {
                newlistFiles[i].delete();
            }
        }
        File generatedFile = new File(saveLocationFolder);
        File[] listFiles = generatedFile.listFiles();
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isDirectory()) {
                FileSystemUtils.deleteRecursively(listFiles[i]);
            }
            else {
                listFiles[i].delete();
            }
        }
    }
}