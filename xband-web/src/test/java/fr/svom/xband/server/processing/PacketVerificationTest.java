/**
 *
 */
package fr.svom.xband.server.processing;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.PacketId;

/**
 * @author etrigui
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PacketVerificationTest {

    @Autowired
    private PacketVerification pktVerif;

    private BandxTransferFrame frame;

    private LcDatFile lcdatfile;

    @Value("${xband.upload.dir}")
    private String                       serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String                 saveLocationFolder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        ApidPacket apid = new ApidPacket();
        apid.setApidValue(1620);
        apid.setInstrument("ECL");
        apid.setCategory("AAV-PVT");
        PacketId pktid = new PacketId();
        pktid.setPktId(96);
        pktid.setApid(apid);
        lcdatfile = new LcDatFile();
        lcdatfile.setLcfileName("SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.DAT");
        lcdatfile.setType("ED");
        lcdatfile.setApidHexa("0654");
        lcdatfile.setStartTime("20170207T160200");
        lcdatfile.setOrbit(1);
        lcdatfile.setStation("KR000");
        lcdatfile.setVer("001");
        frame = new BandxTransferFrame();
        frame.setCcsdsVersion(0);
        frame.setCcsdsType(0);
        frame.setCcsdsHeadFlag(0);
        frame.setApidPacket(apid);
        frame.setCcsdsGFlag(3);
        frame.setCcsdsCounter(1);
        frame.setCcsdsPlength(1015);
        frame.setPacketTimeSec(0x3F7L);
        frame.setTimeMicroSec(0x00007793);
        frame.setPacketId(pktid.getPktId());
        frame.setObsidType(0xFFFF);
        frame.setObsidNum(0xFFFF);
    }
    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isapidExist(java.lang.Integer)}.
     */
    @Test
    public void testIsapidExist() {
        Integer apid = 1620;
        Boolean isexist= pktVerif.isapidExist(apid);
        assertEquals(true,isexist);
        Integer apidf = 1500;
        Boolean isexistf= pktVerif.isapidExist(apidf);
        assertEquals(false,isexistf);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isPrimaryHeaderValid(BandxTransferFrame)}.
     */
    @Test
    public void testIsprimaryHeaderValid() {
        Boolean isValid= pktVerif.isPrimaryHeaderValid(frame);
        assertEquals(true,isValid);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isApidConform(BandxTransferFrame, LcDatFile)}.
     */
    @Test
    public void testIsApidConform() {
        Boolean isConform= pktVerif.isApidConform(frame, lcdatfile);
        assertEquals(true,isConform);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isFrameValid(BandxTransferFrame, LcDatFile)}.
     */
    @Test
    public void testIsFrameValid() {
        Boolean isValid= pktVerif.isFrameValid(frame,lcdatfile );
        assertEquals(true,isValid);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isFrameKnown(BandxTransferFrame)}.
     */
    @Test
    public void testIsFrameKnown() {
        Boolean isFrameKnown= pktVerif.isFrameKnown(frame );
        assertEquals(true,isFrameKnown);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isPacketIDValid(java.lang.Integer, java.lang.Integer)}.
     */
    @Test
    public void testIsPacketIDValid() {
        Integer apid = 1620;
        Integer packetId = 30;
        Boolean isvalid= pktVerif.isPacketIDValid(packetId, apid);
        assertEquals(true,isvalid);

        Integer apidf = 1617;
        Integer packetIdf = 30;
        Boolean isvalidf= pktVerif.isPacketIDValid(packetIdf, apidf);
        assertEquals(false,isvalidf);
    }

    /**
     * Test method for {@link fr.svom.xband.server.processing.PacketVerification#isObsIDValid(java.lang.Integer, java.lang.Integer, java.lang.Integer)}.
     */
    @Test
    public void testIsObsIDValid() {
        Integer apid1 = 1617;
        Integer obsidType1 = 85;
        Integer obsidNum1 = 16777215;
        Boolean isObsValid1= pktVerif.isObsIDValid(apid1, obsidType1, obsidNum1);
        assertEquals(true,isObsValid1);

        Integer apid = 1620;
        Integer obsidType = 255;
        Integer obsidNum = 16777215;
        Boolean isObsValid= pktVerif.isObsIDValid(apid, obsidType, obsidNum);
        assertEquals(true,isObsValid);
    }
}
