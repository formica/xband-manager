package fr.svom.xband.server.externals;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.xband.swagger.messages.DataNotification;
import fr.svom.xband.swagger.messages.DataNotificationContent;
import fr.svom.xband.swagger.messages.DataProvider;
import fr.svom.xband.swagger.messages.DataStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class NatsControllerTest {

    @MockBean
    private NatsController nats;
    /**
     * Jackson Mapper.
     */
    @Autowired
    private ObjectMapper  jacksonMapper;


    /*    public void MockNats() {
        try ( MockServerClient mockNatsClient = new MockServerClient("svomtest.svom.eu",5522)){
            mockNatsClient.when(sc.publish("data.xband", any(), any())
                    .respond.(arg0)("OK");
        }
    }*/

    @Test
    public void testAsyncPublishPacket() throws IOException, InterruptedException {

        /** create Data notification */
        DataNotification dn = new DataNotification();
        Instant now = Instant.now();
        dn.setMessageDate(now.toString());
        DataNotificationContent content = new DataNotificationContent();
        content.setDate(now.atOffset(ZoneOffset.UTC));
        content.setDataProvider(DataProvider.XBANDDB);
        content.dataStream(DataStream.XBAND);
        content.productCard("L0D");
        content.setObsid("0");
        content.setResourceLocator("SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar");
        content.setMessage("a new l0d fits file is available via API REST of the "
                + "xbandManager");
        dn.content(content);
        String packet = jacksonMapper.writeValueAsString(dn);

        Mockito.when(nats.publishToQueue(anyString(), anyString())).thenReturn("SENT");
        String res = nats.publishToQueue("data.xband", packet);
        assertThat(res).isEqualTo("SENT");
    }
}
