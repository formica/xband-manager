package fr.svom.xband.server.service;

import static org.junit.Assert.assertEquals;

import fr.svom.xband.server.processing.StaticLists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApidPacketServiceTest {

    @Autowired
    ApidPacketService apidPacketService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testPidTOinstrument() {
        assertEquals("PDPU", StaticLists.PID_MAP.get(17));
        assertEquals("ECL", StaticLists.PID_MAP.get(18));
        assertEquals("MXT", StaticLists.PID_MAP.get(19));
        assertEquals("GRM", StaticLists.PID_MAP.get(24));
        assertEquals("VT", StaticLists.PID_MAP.get(28));
        assertEquals("PLATFORM", StaticLists.PID_MAP.get(0));
    }

    @Test
    public void testPcatTOType() {
        assertEquals("Science", StaticLists.PCAT_MAP.get(17));
        assertEquals("ScienceHK", StaticLists.PCAT_MAP.get(18));
        assertEquals("AAVPVT", StaticLists.PCAT_MAP.get(20));
        assertEquals("MemoryDump", StaticLists.PCAT_MAP.get(21));
        assertEquals("Test", StaticLists.PCAT_MAP.get(22));
        assertEquals("EngineeringHk", StaticLists.PCAT_MAP.get(23));

        assertEquals("PLATFORM", StaticLists.PCAT_MAP.get(0));
    }

}
