package fr.svom.xband.server.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.LcMetadata;
import fr.svom.xband.data.model.PacketId;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.data.repository.querydsl.SearchCriteria;
import fr.svom.xband.server.controllers.PageRequestHelper;
import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.swagger.messages.DataNotification;
import fr.svom.xband.tools.DataGeneratorEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Slf4j
public class GeneralServiceTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    BandxLogMessageService bandxLogMessageService;

    @Autowired
    FramesBandxPacketService framesBandxPacketService;

    @Autowired
    ApidPacketService apidPacketService;

    @Autowired
    LcFileService lcFileService;

    @Autowired
    TarFileService tarFileService;
    @Autowired
    private PageRequestHelper prh;
    @Autowired
    @Qualifier("tarFileFiltering")
    private IFilteringCriteria tarFileFiltering;

    private File tar;

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    private File extractDir;

    private String tarFileName;

    @Before
    public void setUp() {

        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        ClassLoader classLoader = this.getClass().getClassLoader();
        tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        tar = new File(classLoader.getResource(tarFileName).getFile());

        String FileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001";
        extractDir = new File(classLoader.getResource(FileName).getFile());
    }

    @Test
    public void testLogAndNotification() {

        BandxTransferFrame frame = new BandxTransferFrame();
        ApidPacket apidPacket = new ApidPacket();
        apidPacket.setApidValue(1620);
        apidPacket.setName("MyApid");
        apidPacket.setTid(1);
        apidPacket.setPid(18);
        apidPacket.setPcat(20);
        frame.setApidPacket(apidPacket);

        frame.setCcsdsVersion(0);
        frame.setCcsdsType(0);
        frame.setCcsdsHeadFlag(0);
        frame.setCcsdsGFlag(3);
        frame.setCcsdsCounter(0);
        frame.setCcsdsPlength(1015);

        frame.setPacketTimeSec(3254578L);
        frame.setTimeMicroSec(910235);
        frame.setLocalTimeCounter(999L);

        PacketId pktid = new PacketId();
        pktid.setPktId(30);
        pktid.setApid(apidPacket);
        frame.setPacketId(pktid.getPktId());
        frame.setObsidType(85);
        frame.setObsidNum(16777215);
        frame.setObsid(1442840575L);

        DataSourcePacket dataSourcePacket = new DataSourcePacket();
        byte[] pkt = new byte[1022];
        Random rnd = new Random();
        rnd.nextBytes(pkt);
        dataSourcePacket.setHashId("somehashid");
        dataSourcePacket.setPacket(pkt);
        frame.setDataSourcePacket(dataSourcePacket);
        // LcMedata
        LcMetadata lcmeta = new LcMetadata();
        lcmeta.setSatellite("SVOM");
        lcmeta.setApid(apidPacket.getName());
        lcmeta.setVcidName("VCID");
        lcmeta.setVcidNumber("12");
        lcmeta.setType("ATYPE");
        TarFile tf = new TarFile();
        tf.setTarName("atar.tar");
        lcmeta.setTarFile(tf);
        // Test log service
        log.info("Get message log");
        String somelog = bandxLogMessageService.getStoreMessage(frame);
        assertThat(somelog).contains("1442840575");
        // Test frame service
        log.info("Get notification");
        List<String> datfiles = new ArrayList<>();
        datfiles.add("SOME_DAT.DAT");
        datfiles.add("ANOTHER_DAT.DAT");
        DataNotification dn = framesBandxPacketService.createNotification(lcmeta, datfiles);
        log.info("Created notification {}", dn.getContent().getMessage());
        assertThat(dn.getContent().getMessage()).contains("apid");
        assertThat(dn.getContent().getMessage()).contains("datfiles");

        // Test frame service with null
        log.info("Method with null parameters");
        try {
            framesBandxPacketService.findFramesbyLOC(null);
        }
        catch (AbstractCdbServiceException e) {
            log.warn("cannot findFramesbyLOC with null");
        }
        try {
            framesBandxPacketService.findApidsList(null, null);
        }
        catch (AbstractCdbServiceException e) {
            log.warn("cannot findApidsList with null");
        }
    }

    @Test
    public void apidserviceTest() {
        DataGeneratorEntity dg = new DataGeneratorEntity();
        ApidPacket apidPacket = (ApidPacket) dg.createPojo(ApidPacket.class);
        ApidPacket saved = apidPacketService.registerApid(apidPacket);
        assertThat(saved.getApidValue()).isEqualTo(apidPacket.getApidValue());
        ApidPacket saved1 = apidPacketService.registerApid(apidPacket);
        assertThat(saved1.getApidValue()).isEqualTo(apidPacket.getApidValue());
        ApidPacket saved3 = apidPacketService.registerApid(null);
        assertThat(saved3).isNull();

        PacketId pid = (PacketId) dg.createPojo(PacketId.class);
        pid.setApid(saved);
        PacketId psaved = apidPacketService.registerPacketId(pid);
        assertThat(psaved.getPacketName()).isEqualTo(pid.getPacketName());
        try {
            apidPacketService.registerPacketId(psaved);
        }
        catch (AbstractCdbServiceException e) {
            log.warn("cannot register packet id twice");
        }
        List<PacketId> pktlist = apidPacketService.findPacketIdByApid(apidPacket);
        assertThat(pktlist).isNotNull();
        List<ApidPacket> apidlist = apidPacketService.findByPassId("KR001", 1);
        assertThat(apidlist).isNotNull();
    }

    @Test
    public void tarserviceTest() {
        DataGeneratorEntity dg = new DataGeneratorEntity();
        TarFile tarFile = (TarFile) dg.createPojo(TarFile.class);
        String tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        tarFile.setTarName(tarFileName);
        tarFile.setIsTarNameConform(Boolean.TRUE);
        log.info("Store tarfile entity {}", tarFile);
        TarFile saved = tarFileService.storeTarFile(tarFile);
        assertThat(saved.getTarName()).isEqualTo(tarFileName);
        // Create filters
        String by = "name:" + tarFileName;
        // Create pagination request
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(tarFileFiltering, by);
        }
        log.info("Search tarfile entity using {}", tarFileName);
        TarFile entity = tarFileService.getTarFile(tarFileName);
        assertThat(entity).isNotNull();

        log.info("Search tarfile entity list using {}", by);
        Page<TarFile> pagelist = tarFileService.findAllTarFiles(wherepred, null);
        log.info("Found list of pages {}", pagelist);
        List<TarFile> tarlist = pagelist.toList();
        log.info("List of tar files has length {}", tarlist.size());
        assertThat(pagelist.toList().size()).isGreaterThanOrEqualTo(0);

        tarFile.setApidHexa("0500");
        TarFile entity1 = tarFileService.updateTarFile(tarFile);
        assertThat(entity1).isNotNull();
        try {
            tarFileService.checkTarFile(tarFileName);
        }
        catch (AbstractCdbServiceException e) {
            log.info("Check tar file succeeded");
        }
    }

    @Test
    public void requestHelperTest() {
        String by = "name:somename,atime:20211001T100000Z";
        List<SearchCriteria> clist = prh.createMatcherCriteria(by, "yyyyMMdd'T'HHmmssz");
        assertThat(clist.size()).isPositive();

        String val = prh.getParam(clist, "atime");
        assertThat(val).isEqualTo("1633082400000");
    }
}
