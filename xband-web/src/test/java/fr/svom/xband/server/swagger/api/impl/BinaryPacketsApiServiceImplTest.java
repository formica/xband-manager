package fr.svom.xband.server.swagger.api.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.server.service.BinaryPacketService;
import fr.svom.xband.server.service.FramesBandxPacketService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileSystemUtils;

import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.server.processing.LcFileProcessing;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@Slf4j
public class BinaryPacketsApiServiceImplTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private LcFileProcessing lcFileProcessing;

    @Autowired
    private BinaryPacketService binaryPacketService;

    @Autowired
    private FramesBandxPacketService framesBandxPacketService;


    private String hashId = "4d510ccd88e0e23bdd446d207617734dd003e0b744297fe446289cc461574337";

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    @Before
    public void setUp() {
        log.info("SETUP: read file from {} and save it to {}", serverUploadLocationFolder, saveLocationFolder);
        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        ClassLoader classLoader = BinaryPacketsApiServiceImplTest.class.getClassLoader();
        String tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        File tar = new File(classLoader.getResource(tarFileName).getFile());

        String fileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001";
        File extractDir = new File(classLoader.getResource(fileName).getFile());

        try {
            log.info("Store and process tar file {}", tar);
            Integer nframes = framesBandxPacketService.storeBandxPacket("tar", tar);
            log.info("Stored n frames {}", nframes);
        }
        catch (AbstractCdbServiceException e) {
            log.error("BinaryPacketsApiServiceImplTest: cannot create entries from tar " + extractDir.getAbsolutePath());
        }
    }
    /**
     *Test method for {@link PacketsApiServiceImpl
     * Test API: api/binaryPackets/download/{id}
     * (GET with id : path parameter)
     */
    @Test
    public void test1DownloadBinaryPacket() {
        log.info("******************* test1 DownloadBinaryPacket");
        Page<DataSourcePacket> pages = binaryPacketService.findAllFrames(null, null);
        for (DataSourcePacket pkt : pages.toList()) {
            log.info("Existing data: found pkt {}", pkt.getHashId());
        }
        final ResponseEntity<String> response =
                testRestTemplate.exchange("/api/packets/"+ hashId,
                        HttpMethod.GET, null, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        /** test exception **/
        final ResponseEntity<String> responseErr =
                testRestTemplate.exchange("/api/packets/" + 1245,
                        HttpMethod.GET, null, String.class);
        assertThat(responseErr.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }
    /**
     *Test method for {@link PacketsApiServiceImpl
     * Test API: api/binaryPackets/{id}
     * (GET with id : path parameter)
     */
    @Test
    public void test2FindBandxBinaryPacket() {
        log.info("******************* test2 FindBandxBinaryPacket");
        /** test id not exist **/
        final ResponseEntity<String> respmon = testRestTemplate.exchange("/api/packets/1500",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThan(HttpStatus.OK);

        /** test id exist **/
        final ResponseEntity<String> response = testRestTemplate.exchange("/api/packets/"+hashId,
                HttpMethod.GET, null, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    /**
     * Test method for {@link PacketsApiServiceImpl
     * Test API: api/binaryPackets
     * (GET)
     */
    @Test
    public void test3ListBinaryPackets() {
        log.info("******************* test3 ListBinaryPackets");

        log.info("List binary packets without filters");
        final ResponseEntity<String> response = testRestTemplate.exchange("/api/packets",
                HttpMethod.GET, null, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Now download a file");
        HttpHeaders headers = new HttpHeaders();
        //headers.setAccept(MediaType.APPLICATION_OCTET_STREAM);
        headers.add("datatype", "file");
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        final ResponseEntity<String> response1 = testRestTemplate.exchange("/api/packets",
                HttpMethod.GET, entity, String.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @After
    public void setAfter() {
        log.info("AFTER test cleanup....");
        File generatedFile = new File(saveLocationFolder);
        File[] newlistFiles = generatedFile.listFiles();
        for (int i=0; i <  newlistFiles.length; i++) {
            if (newlistFiles[i].isDirectory()) {
                FileSystemUtils.deleteRecursively(newlistFiles[i]);
            }
            else {
                newlistFiles[i].delete();
            }
        }
    }

}
