package fr.svom.xband.server.swagger.api.impl;

import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.server.processing.LcFileProcessing;
import fr.svom.xband.server.service.FramesBandxPacketService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author etrigui
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@//DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class L0dfilesApiServiceImplTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private LcFileProcessing lcFileProcessing;

    @Autowired
    private FramesBandxPacketService framesBandxPacketService;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    //@//Before
    public void setUp() throws Exception {
        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        ClassLoader classLoader = getClass().getClassLoader();


        String tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        File localtarfile =
                new File(classLoader.getResource(tarFileName).getFile());
        TarFile tarfile = lcFileProcessing.createTarfile(localtarfile);
        File extractedFile =
                new File(classLoader.getResource("SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001").getFile());
        try {
            Integer nframes = framesBandxPacketService.storeBandxPacket("tar", extractedFile);
            log.info("Stored n frames {}", nframes);
        }
        catch (AbstractCdbServiceException e) {
            log.error("test7DeleteFramesOfLcFile: cannot create entries from tar " + extractedFile.getAbsolutePath());
        }
    }

    @Test
    public void test0Empty() {
        log.info("******************************test empty");
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/l0c/dat",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testL0cPassIds() {
        log.info("******************************testL0cPassIds");
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/l0c/passids",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
