package fr.svom.xband.server.swagger.api.impl;

import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.server.service.LcFileService;
import fr.svom.xband.server.service.TarFileService;
import fr.svom.xband.tools.DataGeneratorEntity;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author etrigui
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class TarfilesApiServiceImplTest {

    /**
     * LOGGER.
     */
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TestRestTemplate testRestTemplate;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    TarFileService tarFileService;
    @Autowired
    LcFileService lcFileService;

    private File tar;

    @Value("${xband.upload.dir}")
    private String serverUploadLocationFolder;

    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    private File extractDir;

    private String tarFileName;

    @Before
    public void setUp() {

        File uploadDir = new File(serverUploadLocationFolder);
        if (!uploadDir.exists()) {
            FileFormatUtils.createDir(uploadDir);
        }
        File saveDir = new File(saveLocationFolder);
        if (!saveDir.exists()) {
            FileFormatUtils.createDir(saveDir);
        }
        ClassLoader classLoader = this.getClass().getClassLoader();
        tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        tar = new File(classLoader.getResource(tarFileName).getFile());

        String FileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001";
        extractDir = new File(classLoader.getResource(FileName).getFile());
    }

    /**
     * Test /api/tarfiles
     * (GET) list of Tar files in the DB
     */
    @Test
    public void testStoreAndGetTarfiles() {
        DataGeneratorEntity dg = new DataGeneratorEntity();
        TarFile tarFile = (TarFile) dg.createPojo(TarFile.class);
        String tarFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        tarFile.setTarName(tarFileName);
        tarFile.setIsTarNameConform(Boolean.TRUE);
        log.info("Store tarfile entity {}", tarFile);
        TarFile saved = tarFileService.storeTarFile(tarFile);
        assertThat(saved.getTarName()).isEqualTo(tarFileName);
        log.info("Stored entity {}", saved);

        log.info("******************************test Get List of tar files");
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/l0c",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Response is {}", respmon);
    }

    @Test
    public void datfileTest() {
        log.info("*************************test2saveBandxTransferFrames: file location local");
        /** test with file in local**/
        final String bodyStrlocalfile = tar.getAbsolutePath();
        final String bodyname = tar.getName();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers1.add("source", "local");
        headers1.add("dataType", "tar");
        final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("lcFileName", bodyname);
        map.add("file", new FileSystemResource(new File(bodyStrlocalfile)));
        final HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(
                map, headers1);
        ResponseEntity<String> response2 = testRestTemplate.postForEntity("/api/frames",
                request, String.class);
        log.info("Received response {}", response2.getBody());
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Page<LcDatFile> pagelist = lcFileService.findAllDatFiles(null, null);
        log.info("Found list of pages {}", pagelist);
        List<LcDatFile> datlist = pagelist.toList();
        log.info("List of dat files has length {}", datlist.size());
        assertThat(pagelist.toList().size()).isGreaterThanOrEqualTo(0);
        for (LcDatFile dat : datlist) {
            Boolean tripcheck = lcFileService.tripletNameExist(dat);
            assertThat(tripcheck).isTrue();
        }
    }
}
