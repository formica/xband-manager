package fr.svom.xband.server.processing;

import lombok.extern.slf4j.Slf4j;
import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;

/**
 * Class to deal with formatting.
 *
 * @author etrigui
 * <p>
 * Utility class. Only static methods.
 */
@Slf4j
public final class FileFormatUtils {

    /**
     * Default Ctor. Hide the ctor.
     */
    private FileFormatUtils() {
    }

    /**
     * Untar an inputstream.
     *
     * @param tarFileStream tar file.
     * @param dest          destination file.
     * @throws IOException exception.
     */
    public static void untar(InputStream tarFileStream, File dest) throws IOException {
        if (!dest.exists()) {
            log.debug("Destination file or directory {} does not exists...create it", dest.getName());
            createDir(dest);
        }
        try (TarInputStream zis = new TarInputStream(tarFileStream)) {
            TarEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String name = entry.getName();
                File destPath = new File(dest.getAbsolutePath() + File.separatorChar + name);
                if (entry.isDirectory()) {
                    log.debug("Entry is a directory....we do nothing for now: {}", name);
                    createDir(destPath);
                }
                else {
                    if (name.contains("._") || name.contains("PaxHeader")) {
                        log.warn("Ignoring entry with name {}", name);
                        continue;
                    }
                    log.debug("Create file {}", destPath.getAbsolutePath());
                    File parent = destPath.getParentFile();
                    if (!parent.exists()) {
                        Boolean dircreated = parent.mkdirs();
                        if (!dircreated) {
                            throw new IOException("Cannot create directory " + parent.getAbsolutePath());
                        }
                    }
                    log.debug("Create output stream for {} and copy to {}", name, destPath);
                    try (FileOutputStream fout = new FileOutputStream(destPath);) {
                        zis.copyEntryContents(fout);
                    }
                }
            }
        }
    }

    /**
     * create directory.
     *
     * @param dest File to be created.
     */
    public static void createDir(File dest) {
        try {
            dest.mkdir();
        }
        catch (SecurityException e) {
            // In case is not possible to create the directory.
            log.error("Cannot create directory {} : {}", dest.getName(), e);
        }
    }

    /**
     * @param file      Path.
     * @param filebytes binary data.
     */
    public static void writeFile(Path file, byte[] filebytes) {
        try {
            Files.write(file, filebytes);
        }
        catch (IOException e) {
            // In case is not possible to write the file.
            log.error("Cannot create .bin file {} : {}", file.getFileName(), e);
        }
    }

    /**
     * Check if it is a zipped file.
     *
     * @param file file.
     * @return BOOLEAN True means the file is zipped.
     */
    public static boolean isGZipped(File file) {
        log.info("isGZipped function");
        int gzip = 0;
        // check if it is a zip.
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            gzip = (raf.read() & 0xff | ((raf.read() << 8) & 0xff00));
        }
        catch (IOException e) {
            log.error("exception in isGZipped function {}", e);
        }
        return gzip == GZIPInputStream.GZIP_MAGIC;
    }
    /**
     * Get file extension.
     *
     * @param file file.
     * @return string file extension or empty string.
     */
    public static String getFileExtension(File file) {
        String fileName = file.getName();
        // Get extension using the last "." char.
        if (fileName.lastIndexOf('.') != -1 && fileName.lastIndexOf('.') != 0) {
            return fileName.substring(fileName.lastIndexOf('.') + 1);
        }
        else {
            // No extension found. Return empty string.
            return "";
        }
    }
}
