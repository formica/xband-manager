package fr.svom.xband.server.processing;

import fr.svom.xband.swagger.model.PacketApidIdDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 * <p>
 * | APID | PACKET_ID |      PACKET_NAME      |   DATA_NOTIFICATION   |
 * |------|-----------|-----------------------|-----------------------|
 * | 1588 |   1,2     | TmXbPdpuAavPvt        |                       |
 * | 1620 |    30     | TmXbEclairsAavPvt     | TmXbAavPvt            |
 * | 1652 |    159    | TmXbMxtAavPvt         |                       |
 * |------|-----------|-----------------------|-----------------------|
 * | 1617 |    96     | TmXbEclairsSCRawData  | TmXbEclairsSCRawData  |
 * |------|-----------|-----------------------|-----------------------|
 * | 1649 |    225    | TmXbMxtScEvtOpen      | TmXbMxtScEvtOpen      |
 * | 1649 |    226    | TmXbMxtScEvtFilter    | TmXbMxtScEvtFilter    |
 * | 1649 |    227    | TmXbMxtScEvtSrcE      | TmXbMxtScEvtSrcE      |
 * | 1649 |    228    | TmXbMxtScEvtClsd      | TmXbMxtScEvtClsd      |
 * | 1649 |    229    | TmXbMxtScEvtUNKWN     | TmXbMxtScEvtUNKWN     |
 * |------|-----------|-----------------------|-----------------------|
 * | 1649 |    230    | TmXbMxtScFFOpen       |                       |
 * | 1649 |    231    | TmXbMxtScFFFilter     |                       |
 * | 1649 |    232    | TmXbMxtScFFSrcE       | TmXbMxtScEvtFullFrame |
 * | 1649 |    233    | TmXbMxtScFFClsd       |                       |
 * | 1649 |    234    | TmXbMxtScFFUNKWN      |                       |
 */
@Service
@Slf4j
public class NotificationProcessing {

    public Map<String, List<PacketApidIdDto>> getNotificationCondition(List<PacketApidIdDto> dtolist) {
        Map<String, List<PacketApidIdDto>> notmap = new HashMap<>();
        // Check notification conditions
        for (PacketApidIdDto dto : dtolist) {
            log.info("Check notif on packet apid {}", dto);
            if (dto.getApidValue() == 1588 || dto.getApidValue() == 1620 || dto.getApidValue() == 1652) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbAavPvt")) {
                    pktlist = notmap.get("TmXbAavPvt");
                }
                pktlist.add(dto);
                // Add new notification or update existing one
                log.info("TmXbAavPvt notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbAavPvt", pktlist);
            }
            if (dto.getApidValue() == 1617) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbEclairsSCRawData")) {
                    pktlist = notmap.get("TmXbEclairsSCRawData");
                }
                pktlist.add(dto);
                log.info("TmXbEclairsSCRawData notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbEclairsSCRawData", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() == 225) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtOpen")) {
                    pktlist = notmap.get("TmXbMxtScEvtOpen");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtOpen notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtOpen", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() == 226) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtFilter")) {
                    pktlist = notmap.get("TmXbMxtScEvtFilter");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtFilter notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtFilter", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() == 227) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtSrcE")) {
                    pktlist = notmap.get("TmXbMxtScEvtSrcE");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtSrcE notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtSrcE", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() == 228) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtClsd")) {
                    pktlist = notmap.get("TmXbMxtScEvtClsd");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtClsd notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtClsd", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() == 229) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtUNKWN")) {
                    pktlist = notmap.get("TmXbMxtScEvtUNKWN");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtUNKWN notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtUNKWN", pktlist);
            }
            if (dto.getApidValue() == 1649 && dto.getPacketId() >= 230 && dto.getPacketId() <= 234) {
                List<PacketApidIdDto> pktlist = new ArrayList<>();
                if (notmap.containsKey("TmXbMxtScEvtFullFrame")) {
                    pktlist = notmap.get("TmXbMxtScEvtFullFrame");
                }
                pktlist.add(dto);
                log.info("TmXbMxtScEvtFullFrame notif: put new packet list {}", pktlist.size());
                notmap.put("TmXbMxtScEvtFullFrame", pktlist);
            }
        }
        return notmap;
    }
}
