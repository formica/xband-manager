package fr.svom.xband.server.config;

import fr.svom.xband.server.swagger.api.ApidsApi;
import fr.svom.xband.server.swagger.api.FramesApi;
import fr.svom.xband.server.swagger.api.L0cApi;
import fr.svom.xband.server.swagger.api.PacketsApi;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author etrigui
 * JerseyConfig class.
 */
@Component
@Slf4j
public class JerseyConfig extends ResourceConfig {

    /**
     * Default Ctor.
     * In constructor we can define Jersey Resources &amp; Other Components.
     */
    public JerseyConfig() {
        super();
    }

    /**
     * INITIALISATION.
     * Register API
     */
    @PostConstruct
    public void init() {

        log.debug("Jersey Config init");

        // Exception handler
        super.register(JerseyExceptionHandler.class);
        // Feature.
        register(MultiPartFeature.class);
        /** Add Apids Api **/
        register(ApidsApi.class);
        /** Add FramesApi**/
        register(FramesApi.class);
        /** Add BinaryPackets Api**/
        register(PacketsApi.class);
        /** Add L0dfiles Api **/
        //register(L0dfilesApi.class);
        /** Add Tarfiles Api ***/
        register(L0cApi.class);
    }

}
