package fr.svom.xband.server.service;

import com.querydsl.core.types.Predicate;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.repository.DataSourceRepository;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author etrigui
 */
@Service
@Slf4j
public class BinaryPacketService {

    /**
     * data Source Repository.
     */
    @Autowired
    private DataSourceRepository dataSourceRepository;

    /**
     * mapper facade qualifier.
     */
    @Autowired
    private MapperFacade mapper;

    /**
     * @param hashid Hash identifier.
     * @return DataSourcePacketDto
     */
    public DataSourcePacket findbinaryByhash(String hashid) {

        log.debug("Search for binary packet {} ", hashid);
        Optional<DataSourcePacket> entity = dataSourceRepository.findById(hashid);
        if (entity.isPresent()) {
            log.debug("Found packet for hash {}", hashid);
            return entity.get();
        }
        return null;
    }

    /**
     * Find all transfer frames.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page<BandxTransferFrame>
     */
    public Page<DataSourcePacket> findAllFrames(Predicate qry, Pageable req) {
        Page<DataSourcePacket> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = dataSourceRepository.findAll(req);
        }
        else {
            entitylist = dataSourceRepository.findAll(qry, req);
        }
        // Return a list of binary packets.
        return entitylist;
    }

    /**
     * Save a binary packet.
     *
     * @param pkt Data Source packet.
     */
    public void save(DataSourcePacket pkt) {
        dataSourceRepository.save(pkt);
    }
}
