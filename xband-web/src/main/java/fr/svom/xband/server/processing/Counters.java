package fr.svom.xband.server.processing;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

/**
 * @author etrigui
 *
 */
@Component
@Data
@Accessors(fluent = false)
public class Counters {

    /** bad header: first header BAD format : isValid = false.*/
    private Integer badHeader = 0;

    /** 2.Unexpected Apid, nb unexpected valid APID : isValid = false and isKnown = true.  */
    private Integer badApid = 0;

    /** 3.second header BAD format: (flag & 0x60) > 0. */
    private Integer secHead = 0;

    /** 4.decreasingPPS: should be 0, nb decreasing PPS time ((flag & 0x09) == 0x09). */
    private Integer decPPS = 0;

    /** 5.unexpected pktId : should be 0, isValid = true and isKnown = false. */
    private Integer badPktId = 0;

    /** 6.badDuplicate: should be 0, nb of identical packets : idDuplicate = True. **/
    private Integer badDuplicate = 0;

    /** 7.identical packet: should be 0, packets having a doublon but not identical. */
    private Integer identicalPkt = 0;

    /** 8.tmGap: should be 0, nb Tm Gaps (flag & 0x03) == 0x03). */
    private Integer nbTmGaps = 0;

    /**
     * ERROR FLAG is initially equal to à 0(1 byte = 0000000 )
     * ERRRO: 0_0_0_NBERRH1 _NBBADDUPLI_NBERRH2 _NBPPSDECR _NBPACKID
     * NBERRH1 = 1 if the nb of packets with invalid primary header is > 0
     * NBBADDUPLI = 1 if the nb of packets with bad duplicated is > 0
     * NBERRH2 = 1    if the nb of packets with invalid secondary is > 0
     * NBPPSDECR = 1  if the nb of packets with pps decreasing is > 0
     * NBPACKID = 1   if the nb of packets with unknown packetid is > 0.
     */
    private Integer error = 0;

}
