package fr.svom.xband.server.processing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 */
public final class StaticLists {
    /**
     * BandX_APID_LIST.
     */
    public static final List<Integer> BX_APID_LIST = Arrays.asList(1585, 1586, 1588, 1589, 1590,
            1617, 1618, 1620, 1621, 1622, 1649, 1650, 1652, 1653, 1654, 1809, 1810, 1812, 1813,
            1814, 1937, 1938, 1940, 1941, 1942);

    /**
     * apid PacketId Map.
     */
    public static final Map<Integer, List<Integer>> APID_PKTID_MAP = new HashMap<>();

    /**
     * apid Science Map.
     */
    public static final Map<Integer, Boolean> APID_SCIENCE_MAP = new HashMap<>();

    /**
     * pid Name Map.
     */
    public static final Map<Integer, String> PID_MAP = Map.of(17, "PDPU", 18, "ECL", 19, "MXT",
            24, "GRM", 28, "VT", 0, "PLATFORM");
    /**
     * pcat Name Map.
     */
    public static final Map<Integer, String> PCAT_MAP = Map.of(17, "Science", 18, "ScienceHK", 20, "AAVPVT", 21,
            "MemoryDump", 22, "Test", 23, "EngineeringHk", 0, "PLATFORM");

    /**
     * List of ObsidType.
     */
    public static final List<Integer> OBSID_TYPE_LIST = List.of(0xE7, 0x0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
            0x88, 0xAA, 0xBB, 0xCC, 0xFF);

    /**
     * APID VALUEs FOR XBand SCIENCE PACKETS.
     */
    public static final List<Integer> BX_APID_SCIENCELIST = List.of(1617, 1649, 1585, 1809, 1937);

    /**
     * Init map.
     */
    static {
        /** packetId possible values for apid = 1617 : {0x60} */
        List<Integer> pktIdList1617 = new ArrayList<>();
        pktIdList1617.add(96);
        APID_PKTID_MAP.put(1617, pktIdList1617);
        APID_SCIENCE_MAP.put(1617, Boolean.TRUE);
        /** packetId possible values for apid = 1618: {0x03, 0x05, 0x06} */
        List<Integer> pktIdList1618 = new ArrayList<>();
        pktIdList1618.add(3);
        pktIdList1618.add(5);
        pktIdList1618.add(6);
        APID_PKTID_MAP.put(1618, pktIdList1618);
        APID_SCIENCE_MAP.put(1618, Boolean.FALSE);

        /** packetId possible values for apid = 1620: {0x1E} */
        List<Integer> pktIdList1620 = new ArrayList<>();
        pktIdList1620.add(30);
        APID_PKTID_MAP.put(1620, pktIdList1620);
        APID_SCIENCE_MAP.put(1620, Boolean.FALSE);

        /** packetId possible values for apid = 1621: {0x40} */
        List<Integer> pktIdList1621 = new ArrayList<>();
        pktIdList1621.add(64);
        APID_PKTID_MAP.put(1621, pktIdList1621);
        APID_SCIENCE_MAP.put(1621, Boolean.FALSE);

        /** packetId possible values for apid = 1622: {0x00} */
        List<Integer> pktIdList1622 = new ArrayList<>();
        pktIdList1622.add(0);
        APID_PKTID_MAP.put(1622, pktIdList1622);
        APID_SCIENCE_MAP.put(1622, Boolean.FALSE);

        /** packetId possible values for apid = 1649: {0xE2, 0xE1, 0xE4, 0xE7} */
        List<Integer> pktIdList1649 = new ArrayList<>();
        pktIdList1649.add(225);
        pktIdList1649.add(226);
        pktIdList1649.add(228);
        pktIdList1649.add(231);
        APID_PKTID_MAP.put(1649, pktIdList1649);
        APID_SCIENCE_MAP.put(1649, Boolean.TRUE);

        /** packetId possible values for apid = 1650: {0x81, 0x82, 0x84} */
        List<Integer> pktIdList1650 = new ArrayList<>();
        pktIdList1650.add(129);
        pktIdList1650.add(130);
        pktIdList1650.add(132);
        APID_PKTID_MAP.put(1650, pktIdList1650);
        APID_SCIENCE_MAP.put(1650, Boolean.FALSE);

        /** packetId possible values for apid = 1652: {0x9F} */
        List<Integer> pktIdList1652 = new ArrayList<>();
        pktIdList1652.add(159);
        APID_PKTID_MAP.put(1652, pktIdList1652);
        APID_SCIENCE_MAP.put(1652, Boolean.FALSE);

        /** packetId possible values for apid = 1653: {0xC1} */
        List<Integer> pktIdList1653 = new ArrayList<>();
        pktIdList1653.add(193);
        APID_PKTID_MAP.put(1653, pktIdList1653);
        APID_SCIENCE_MAP.put(1653, Boolean.FALSE);

        /** packetId possible values for apid = 1654: {0x00} */
        List<Integer> pktIdList1654 = new ArrayList<>();
        pktIdList1654.add(0);
        APID_PKTID_MAP.put(1654, pktIdList1654);
        APID_SCIENCE_MAP.put(1654, Boolean.FALSE);

        /** packetId possible values for apid = 1588: {0x01, 0x02} */
        List<Integer> pktIdList1588 = new ArrayList<>();
        pktIdList1588.add(1);
        pktIdList1588.add(2);
        APID_PKTID_MAP.put(1588, pktIdList1588);
        APID_SCIENCE_MAP.put(1588, Boolean.FALSE);

        APID_SCIENCE_MAP.put(1585, Boolean.TRUE);
        APID_SCIENCE_MAP.put(1809, Boolean.TRUE);
        APID_SCIENCE_MAP.put(1937, Boolean.TRUE);
    }

    /**
     * Protect ctor.
     */
    protected StaticLists() {
        // Empty.
    }
}
