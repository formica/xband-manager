package fr.svom.xband.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.NotExistsPojoException;
import fr.svom.xband.data.exceptions.XbandRequestFormatException;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.model.views.FrameObsid;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.server.controllers.EntityDtoHelper;
import fr.svom.xband.server.controllers.PageRequestHelper;
import fr.svom.xband.server.service.FramesBandxPacketService;
import fr.svom.xband.server.service.LcFileService;
import fr.svom.xband.server.service.TarFileService;
import fr.svom.xband.server.swagger.api.ApiResponseMessage;
import fr.svom.xband.server.swagger.api.FramesApiService;
import fr.svom.xband.server.swagger.api.NotFoundException;
import fr.svom.xband.swagger.messages.DataNotification;
import fr.svom.xband.swagger.model.BandxTransferFrameDto;
import fr.svom.xband.swagger.model.BandxTransferFrameSetDto;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.ObsidSetDto;
import fr.svom.xband.swagger.model.ObsidSetDtoAllOfResources;
import fr.svom.xband.swagger.model.PacketApidIdDto;
import fr.svom.xband.swagger.model.PacketApidIdSetDto;
import fr.svom.xband.swagger.model.PacketApidIdSetDtoAllOf;
import fr.svom.xband.swagger.model.RespPage;
import ma.glasnost.orika.MapperFacade;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 * Implementation class for FramesApi.
 */
@Component
public class FramesApiServiceImpl extends FramesApiService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(FramesApiServiceImpl.class);

    /**
     * FramesBandxPacketService.
     */
    @Autowired
    FramesBandxPacketService bandxPacketService;

    /**
     * Service.
     */
    @Autowired
    LcFileService lcFileService;

    /**
     * Service.
     */
    @Autowired
    TarFileService tarFileService;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("bandxTransferFrameFiltering")
    private IFilteringCriteria bandxTransferFrameFiltering;

    @Autowired
    EntityDtoHelper edh;


    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.FramesApiService#listBandxTransferFrames(java.lang.Integer, java.lang
     * .Integer, java.lang.String, java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listBandxTransferFrames(String by, Integer page, Integer size, String sort,
                                            String format, SecurityContext securityContext, UriInfo info) {
        /** Search transfer frame list */
        log.info("Search resource list by {}", by);
        // Create filters
        Map<String, Object> filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(bandxTransferFrameFiltering, by);
        }
        // Retrieve frame list using filtering.
        Page<BandxTransferFrame> entitypage = bandxPacketService.findAllFrames(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        List<BandxTransferFrameDto> dtolist = edh.entityToDtoList(entitypage.toList(), BandxTransferFrameDto.class);
        // Create the Set.
        final BaseResponseDto setdto = buildEntityResponse(dtolist, filters);
        setdto.page(respPage);
        // Send response.
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.FramesApiService#deleteFramesOfLcFile(java.lang.String, javax.ws.rs.core
     * .SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response deleteFramesOfLcFile(String lcFileName, SecurityContext securityContext,
                                         UriInfo info) throws NotFoundException {
        /**BandxApiService processing request to delete all frames in the L0c tar file. */
        log.info("BandxApiService processing request to delete all frames in the L0c tar file: "
                 + lcFileName);
        LcDatFile lcfile = lcFileService.findLcfileByName(lcFileName);
        List<BandxTransferFrame> framelist = bandxPacketService.findFramesbyLOC(lcFileName);
        if (framelist.isEmpty()) {
            /** return error message as response **/
            log.info("Removing dat file if still present {}", lcFileName);
            lcFileService.deleteDatFile(lcFileName);
            throw new NotExistsPojoException("Frames list is empty but dat file was deleted");
        }
        else {
            log.info("Delete frames and packets from list of size {}", framelist.size());
            //List<String> packetList = bandxPacketService.deleteFramesByL0C(framelist);
            bandxPacketService.deleteFramesByLcFileId(lcfile.getLcfileId());
            bandxPacketService.deletePacketsByLcFileId(lcfile.getLcfileId());
            //bandxPacketService.deletePackets(packetList);
            log.info("Removing dat file {}", lcFileName);
            lcFileService.deleteDatFile(lcFileName);
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.INFO,
                    "Removed " + framelist.size() + " frames from DB");
            ResponseBuilder response = Response.ok(resp);
            /** return ok message as reponse **/
            return response.build();
        }
    }

    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.FramesApiService#saveBandxTransferFrames_1(java.lang.String, java.io
     * .InputStream, org.glassfish.jersey.media.multipart.FormDataContentDisposition, javax.ws.rs.core
     * .SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response saveBandxTransferFrames(String dataType, String source,
                                            FormDataBodyPart fileBodypart,
                                            String lcFileName,
                                            SecurityContext securityContext, UriInfo info) {
        log.info("FramesApiServiceImpl processing request: store frames from the tar/dat file: {}, {}, {}",
                lcFileName, dataType, source);
        FormDataContentDisposition fileDetail = null;
        InputStream fileInputStream = null;
        String name = lcFileName;
        if (fileBodypart != null) {
            fileDetail = fileBodypart.getFormDataContentDisposition();
            fileInputStream = fileBodypart.getValueAs(InputStream.class);
            if (name == null) {
                String fileName = new File(fileDetail.getFileName()).getName();
                log.info("lcFileName is null in input...use {}", fileName);
                name = fileName;
            }
        }
        if (name == null) {
            /** return request error message **/
            throw new XbandRequestFormatException(
                    "NO FILE RECEIVED IN THE REQUEST BODY! VERIFY REQUEST FORM : "
                    + "curl -X POST -F 'file=@filePath.tar' "
                    + "-H \"Content-Type: multipart/form-data\" -H \"dataType: archive\" "
                    + "http://server:port/api/frames \n");
        }
        /** store received file**/
        File receivedfile = null;
        if ("local".equals(source)) {
            // The file should be present in the upload directory itself.
            // The request contains the tar or dat file.
            receivedfile = bandxPacketService.getFile(name, fileInputStream);
            log.debug("File downloded from request: length is {}", receivedfile.length());
        }
        else if ("db".equals(source)) {
            // The file is a tar and should be taken from DB location.
            // Send exception if the tar file name does not exists in DB.
            TarFile tarfile = tarFileService.getTarFile(name);
            receivedfile = new File(tarfile.getTarPath() + "/" + tarfile.getTarName());
            log.debug("File retrieved from db: length is {}", receivedfile.length());
        }
        else {
            // The file should be taken from remote location.
            // If the tar exists send an exception.
            tarFileService.checkTarFile(name);
            receivedfile = bandxPacketService.downloadFile(name);
        }
        /*** verify file extension **/
        String verifieddataType = bandxPacketService.datafileType(receivedfile);
        log.debug("Verified data is {}", verifieddataType);
        if (dataType.startsWith("arch")) {
            dataType = "tar";
        }
        if (!dataType.equalsIgnoreCase(verifieddataType)) {
            throw new XbandRequestFormatException("The data type in the request does not correspond to the file "
                                                  + "extension");
        }
        /*** file pre-processing **/
        return processingResponse(dataType, receivedfile);
    }

    @Override
    public Response listObsidsInFrames(@NotNull String passId, Long obsid, Integer apid,
                                       Integer packetid, SecurityContext securityContext,
                                       UriInfo info)
            throws NotFoundException {
        /** Search transfer frame list for Obsids and Apids */
        log.info("Search resource list by passid={}, apid={}, packetid={}", passId, apid, packetid);
        // Split Pass_id
        String station = null;
        Integer orbit = null;
        if (!"none".equalsIgnoreCase(passId)) {
            String[] passid_arr = passId.split("_");
            station = passid_arr[1];
            orbit = Integer.valueOf(passid_arr[0]);
        }
        final PageRequest preq = prh.createPageRequest(0, 1000, null);
        Page<FrameObsid> entitylist = bandxPacketService.findObsidList(obsid, apid, station, orbit, packetid, preq);
        List<ObsidSetDtoAllOfResources> dtolist = edh.entityToDtoList(entitylist, ObsidSetDtoAllOfResources.class);
        RespPage respPage = new RespPage().size(dtolist.size())
                .totalElements(Long.valueOf(dtolist.size())).totalPages(1)
                .number(0);
        // Create the Set.
        final ObsidSetDto setdto = new ObsidSetDto();
        setdto.setResources(dtolist);
        setdto.page(respPage);
        setdto.size(respPage.getSize().longValue());
        // Send back response set.
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    @Override
    public Response listApidPacketIdsInFrames(@NotNull String passId, String xNotification,
                                              SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /** Search transfer frame list for Apids and PacketIds */
        log.info("Search resource list by passid {}", passId);
        // Split Pass_id
        String[] passid_arr = passId.split("_");
        String station = passid_arr[1];
        Integer orbit = Integer.valueOf(passid_arr[0]);
        List<PacketApidIdDto> dtolist = bandxPacketService.findApidPacketIdList(station, orbit);
        RespPage respPage = new RespPage().size(dtolist.size())
                .totalElements(Long.valueOf(dtolist.size())).totalPages(1)
                .number(0);
        // Create the Set.
        final PacketApidIdSetDto setdto = new PacketApidIdSetDto();
        setdto.setResources(dtolist);
        setdto.page(respPage);
        // Verify if we need notifications to be generated
        if ("generate".equalsIgnoreCase(xNotification)) {
            log.info("Generate notifications....");
            List<DataNotification> dnlist = bandxPacketService.createNotification(dtolist, passId);
            log.info("list of notifications of size {}", dnlist.size());
            for (DataNotification dn : dnlist) {
                bandxPacketService.publishNotification(dn);
            }
        }
        // Send back response set.
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    /**
     * @param dataType     type of received file (.dat or archive .tar).
     * @param receivedfile the received File.
     * @return Response.
     */
    protected Response processingResponse(String dataType, File receivedfile) {
        Integer nbCreatedFrames = 0;
        log.debug("Processing file {} of type {}", receivedfile.getName(), dataType);
        /** pre-processing of stored raw data file.**/
        nbCreatedFrames = bandxPacketService.storeBandxPacket(dataType, receivedfile);
        log.debug("Created number of frames {}", nbCreatedFrames);
        /** if successful processing **/
        if (nbCreatedFrames != null && nbCreatedFrames != 0) {
            /** return Ok message as response **/
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.OK,
                    "PRE-PROCESSING SUCCESSFULL and new received xband packets are "
                    + "stored in the Database");
            return Response.status(Response.Status.CREATED).entity(resp).build();
        }
        else {
            /** if problem when processing **/
            log.warn("No frames have been inserted");
            /** return error message as response **/
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.INFO,
                    "FRAMES EMPTY: problem when creating frames from file " + receivedfile);
            return Response.status(Response.Status.NOT_MODIFIED).entity(resp).build();
        }
    }

    /**
     * Factorise code to build the BandxTransferFrameSetDto.
     *
     * @param dtolist the List<BandxTransferFrameDto>
     * @param filters the Map
     * @return BandxTransferFrameSetDto
     */
    protected BandxTransferFrameSetDto buildEntityResponse(List<BandxTransferFrameDto> dtolist,
                                                           Map<String, Object> filters) {
        final BandxTransferFrameSetDto respdto = new BandxTransferFrameSetDto();
        // Create the Set for the response.
        ((BandxTransferFrameSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

}
