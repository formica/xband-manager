package fr.svom.xband.server.config;

import fr.svom.xband.server.externals.NatsConnectionFactory;
import javassist.bytecode.stackmap.TypeData.ClassName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author etrigui
 */

/**
 * ServicesConfig class.
 */
@Configuration
@ComponentScan("fr.svom.xband.server")
@EnableAspectJAutoProxy
public class ServicesConfig {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ClassName.class);

    /**
     * @param natsprops nats properties.
     * @return Nats
     * Connection Factory
     */
    @Bean(name = "natsConnectionFactory")
    public NatsConnectionFactory getNatsConnectionFactory(@Autowired NatsProperties natsprops) {
        log.info("start NatsConnectionFactory Bean");
        /** get nats properties values from NatsProperties class **/
        String natsurl = "nats://" + natsprops.getServer() + ":" + natsprops.getPort();
        if (!("none").equals(natsprops.getUser())) {
            natsurl = "nats://" + natsprops.getUser() + ":" + natsprops.getPassword() + "@"
                      + natsprops.getServer() + ":" + natsprops.getPort();
        }
        /** create NatsConncetionFactory**/
        log.debug("Creating NatsConnectionFactory Bean : {}", natsurl);
        // Build the factorry.
        final NatsConnectionFactory natsFactory = new NatsConnectionFactory(natsprops.getCluster(),
                natsprops.getClientId(),
                natsurl,
                natsprops.getXbandQueue());
        natsFactory.initNatsConnectionFactory();

//        NatsConnectionFactory natsFactory = new NatsConnectionFactory();
//        natsFactory.setFactoryParameters(natsprops.getCluster(), natsprops.getClientId(), natsprops.getXbandQueue(),
//                natsprops.getListening(), natsurl, natsprops.getListeningChannel());
        return natsFactory;
    }
}
