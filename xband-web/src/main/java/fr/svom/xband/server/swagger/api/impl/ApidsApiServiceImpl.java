package fr.svom.xband.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.XbandRequestFormatException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.PacketId;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.server.controllers.EntityDtoHelper;
import fr.svom.xband.server.controllers.PageRequestHelper;
import fr.svom.xband.server.service.ApidPacketService;
import fr.svom.xband.server.swagger.api.ApiResponseMessage;
import fr.svom.xband.server.swagger.api.ApidsApiService;
import fr.svom.xband.server.swagger.api.NotFoundException;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.PacketApidSetDto;
import fr.svom.xband.swagger.model.PacketIdDto;
import fr.svom.xband.swagger.model.PacketIdSetDto;
import fr.svom.xband.swagger.model.RespPage;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 */

/**
 * implementation class for Apids Api service.
 */

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2018-05-22T14:07"
                                                                                                   + ":31.807+02:00")
@Component
public class ApidsApiServiceImpl extends ApidsApiService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ApidsApiServiceImpl.class);

    /**
     * packetApidService.
     */
    @Autowired
    ApidPacketService packetApidService;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("packetapidFiltering")
    private IFilteringCriteria packetapidFiltering;

    @Autowired
    EntityDtoHelper edh;


    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.ApidsApiService#createPacketApid(fr.svom.bandx.swagger.model
     * .PacketApidDto, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response createPacketApid(PacketApidDto body, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /**create new apid Packet**/
        log.info("createPacketApid using request {}", body);
        /** return apid entity created as response**/
        ApidPacket apidPacket = mapper.map(body, ApidPacket.class);
        ApidPacket entity = packetApidService.registerApid(apidPacket);
        PacketApidDto dto = mapper.map(entity, PacketApidDto.class);
        return Response.ok().entity(dto).build();
    }

    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.ApidsApiService#findPacketApids(java.lang.Integer, javax.ws.rs.core
     * .SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response findPacketApids(Integer id, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /** find apid packet by id */
        log.info("Search packet APID using ID {}", id);
        Map<String, Object> filters = new HashMap<>();
        filters.put("apidValue", id);
        ApidPacket entity = packetApidService.findApid(id);
        if (entity == null) {
            String message = "No resource has been found";
            log.debug(message);
            /** return error message as response**/
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.WARNING, message);
            return Response.status(Response.Status.NOT_FOUND).entity(resp).build();
        }
        PacketApidDto dto = mapper.map(entity, PacketApidDto.class);
        List<PacketApidDto> dtolist = new ArrayList<>();
        dtolist.add(dto);
        final BaseResponseDto resp = buildEntityResponse(dtolist, filters);
        // Send a response and status 200.
        return Response.status(Response.Status.OK).entity(resp).build();
    }

    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.ApidsApiService#listPacketApids(java.lang.String, java.lang.Integer,
     * java.lang.Integer, java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listPacketApids(String by, Integer page, Integer size, String sort, SecurityContext securityContext,
                                    UriInfo info) throws NotFoundException {
        /** search list of packet apid. **/
        log.info("Search resource list by {}", by);
        // Create filters
        Map<String, Object> filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(packetapidFiltering, by);
        }
        // Check if the request is using pass_id
        BaseResponseDto setdto = null;
        List<PacketApidDto> dtolist = null;
        RespPage respPage = null;
        if (by.contains("pass_id")) {
            // Split Pass_id
            String station = (String) filters.get("station");
            Integer orbit = Integer.valueOf((String) filters.get("orbit"));

            List<ApidPacket> entitylist = packetApidService.findByPassId(station, orbit);
            respPage = new RespPage().size(entitylist.size())
                    .totalElements((long) entitylist.size()).totalPages(1)
                    .number(0);
            dtolist = edh.entityToDtoList(entitylist, PacketApidDto.class);

        }
        else {
            // Retrieve apid list using filtering.
            Page<ApidPacket> entitypage = packetApidService.findAllApids(wherepred, preq);
            respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());
            dtolist = edh.entityToDtoList(entitypage.toList(), PacketApidDto.class);
            // Create the Set.
            setdto = buildEntityResponse(dtolist, filters);
            setdto.page(respPage);
        }
        // Create the Set.
        setdto = buildEntityResponse(dtolist, filters);
        setdto.page(respPage);
        Response.Status rstatus = Response.Status.OK;
        // Send a response and status 200.
        return Response.status(rstatus).entity(setdto).build();
    }

    @Override
    public Response createPacketId(Integer apid, PacketIdDto packetIdDto, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        if (packetIdDto == null) {
            throw new XbandRequestFormatException("Bad request format for packetId: null not allowed");
        }
        /** return apid entity created as response**/
        ApidPacket apidentity = packetApidService.findApid(apid);
        if (apidentity == null) {
            String message = "No apid resource has been found for " + apid;
            log.debug(message);
            /** return error message as response**/
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.WARNING, message);
            return Response.status(Response.Status.NOT_FOUND).entity(resp).build();
        }
        PacketId tbs = mapper.map(packetIdDto, PacketId.class);
        tbs.setApid(apidentity);
        PacketId entity = packetApidService.registerPacketId(tbs);
        PacketIdDto dto = mapper.map(entity, PacketIdDto.class);
        return Response.ok().entity(dto).build();
    }

    @Override
    public Response findPacketIdsByApid(Integer apid, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        if (apid == null) {
            throw new XbandRequestFormatException("Bad request format for apid: null not allowed");
        }
        /**
         * Get the ApidPacket
         */
        ApidPacket entity = packetApidService.findApid(apid);
        /** return apid entity created as response**/
        List<PacketId> entitylist = packetApidService.findPacketIdByApid(entity);
        List<PacketIdDto> dtolist = edh.entityToDtoList(entitylist, PacketIdDto.class);
        Map<String, Object> filters = new HashMap<>();
        filters.put("apid", apid);
        PacketIdSetDto resp = buildPacketIdEntityResponse(dtolist, filters);
        return Response.ok().entity(resp).build();
    }

    /**
     * Factorise code to build the PacketApidSetDto.
     *
     * @param dtolist the List<PacketApidDto>
     * @param filters the Map
     * @return PacketApidSetDto
     */
    protected PacketApidSetDto buildEntityResponse(List<PacketApidDto> dtolist,
                                                   Map<String, Object> filters) {
        final PacketApidSetDto respdto = new PacketApidSetDto();
        // Create the Set for the response.
        ((PacketApidSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

    /**
     * Factorise code to build the PacketApidSetDto.
     *
     * @param dtolist the List<PacketApidDto>
     * @param filters the Map
     * @return PacketApidSetDto
     */
    protected PacketIdSetDto buildPacketIdEntityResponse(List<PacketIdDto> dtolist,
                                                         Map<String, Object> filters) {
        final PacketIdSetDto respdto = new PacketIdSetDto();
        // Create the Set for the response.
        ((PacketIdSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

}
