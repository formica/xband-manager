package fr.svom.xband.server.service;

import com.querydsl.core.types.Predicate;
import fr.svom.xband.data.exceptions.AlreadyExistsPojoException;
import fr.svom.xband.data.exceptions.NotExistsPojoException;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.repository.TarFileRepository;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.Optional;

/**
 * @author etrigui
 */
@Service
public class TarFileService {
    /**
     * L0d data repository.
     */
    @Autowired
    private TarFileRepository tarFileRepository;

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TarFileService.class);

    /**
     * mapper facade qualifier.
     */
    @Autowired
    private MapperFacade mapper;

    /**
     * Default Ctor.
     */
    public TarFileService() {
        super();
    }


    /**
     * Find all tar files.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page<TarFile>
     */
    public Page<TarFile> findAllTarFiles(Predicate qry, Pageable req) {
        Page<TarFile> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = tarFileRepository.findAll(req);
        }
        else {
            entitylist = tarFileRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Get tar file.
     *
     * @param name the string name.
     * @return TarFile
     */
    @Transactional(Transactional.TxType.REQUIRED)
    public TarFile getTarFile(String name) {
        return tarFileRepository.findById(name).orElseThrow(
                () -> new NotExistsPojoException("Cannot find tar name " + name + " in DB")
        );
    }

    /**
     * Store tar file if not present.
     *
     * @param tarFile
     * @return TarFile
     */
    @Transactional(Transactional.TxType.REQUIRED)
    public TarFile storeTarFile(TarFile tarFile) {
        Optional<TarFile> opttar = tarFileRepository.findById(tarFile.getTarName());
        if (opttar.isPresent()) {
            log.debug("Send the existing tar file from DB.");
            return opttar.get();
        }
        return tarFileRepository.save(tarFile);
    }

    /**
     * Update tar file info.
     *
     * @param tarFile
     * @return TarFile
     * @throws NotFoundException
     */
    @Transactional(Transactional.TxType.REQUIRED)
    public TarFile updateTarFile(TarFile tarFile) throws NotFoundException {
        TarFile entity = tarFileRepository.findById(tarFile.getTarName()).orElseThrow(
                () -> new NotFoundException("Tar file " + tarFile.getTarName() + " not found for update.")
        );
        entity.setNbDatfile(tarFile.getNbDatfile());
        return tarFileRepository.save(entity);
    }

    /**
     * Find a tar file. Send exception if exists.
     *
     * @param tarname
     */
    public void checkTarFile(String tarname) {
        Optional<TarFile> opttar = tarFileRepository.findById(tarname);
        if (opttar.isPresent()) {
            throw new AlreadyExistsPojoException("Tar file " + tarname + " already in DB");
        }
        return;
    }
}
