package fr.svom.xband.server.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;
import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.exceptions.BandxServiceException;
import fr.svom.xband.data.exceptions.FileDownloadException;
import fr.svom.xband.data.exceptions.XbandDecodingException;
import fr.svom.xband.data.exceptions.XbandRequestFormatException;
import fr.svom.xband.data.exceptions.XbandSQLException;
import fr.svom.xband.data.helpers.LcMetadataParser;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.LcMetadata;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.model.views.FrameObsid;
import fr.svom.xband.data.repository.BandxTransferFrameRepository;
import fr.svom.xband.data.repository.DataSourceRepository;
import fr.svom.xband.data.repository.IJdbcRepository;
import fr.svom.xband.data.repository.custom.FrameObsidRepository;
import fr.svom.xband.data.serialization.BandxFrameDeserializer;
import fr.svom.xband.httpclient.HttpClient;
import fr.svom.xband.server.config.NatsQueue;
import fr.svom.xband.server.externals.NatsController;
import fr.svom.xband.server.processing.FileFormatUtils;
import fr.svom.xband.server.processing.LcFileProcessing;
import fr.svom.xband.server.processing.NotificationProcessing;
import fr.svom.xband.swagger.messages.DataNotification;
import fr.svom.xband.swagger.messages.DataNotificationContent;
import fr.svom.xband.swagger.messages.DataProvider;
import fr.svom.xband.swagger.messages.DataStream;
import fr.svom.xband.swagger.model.PacketApidIdDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service for xband frames.
 *
 * @author etrigui
 * @version %I%, %G%
 */
@Service
@Slf4j
public class FramesBandxPacketService {

    /**
     * folder Location for uploaded files.
     */
    @Value("${xband.upload.dir}")
    private String serverUploadFolder = null;

    /**
     * folder Location for downloaded files.
     */
    @Value("${xband.download.dir}")
    private String saveLocationFolder = null;

    /**
     * L0c file processing object.
     */
    @Autowired
    private LcFileProcessing lcfileProcessing;

    /**
     * xband Packet Repository.
     */
    @Autowired
    private BandxTransferFrameRepository bandxPacketRepository;

    /**
     * frame Obsid Repository.
     */
    @Autowired
    private FrameObsidRepository frameObsidRepository;

    /**
     * Data Packet Repository.
     */
    @Autowired
    private DataSourceRepository dataSourceRepository;

    /**
     * Jdbc Repository.
     */
    @Autowired
    private IJdbcRepository jdbcRepository;

    /**
     * Lc dat file Repository.
     */
    @Autowired
    private LcFileService lcFileService;

    /**
     * Tar file service.
     */
    @Autowired
    private TarFileService tarFileService;

    /**
     * Unit x-band packet service.
     */
    @Autowired
    private UnitBandxPacketService unitBxPacketService;
    /**
     * L0c meta-data parser.
     */
    @Autowired
    private LcMetadataParser lcMetadataParser;

    /**
     * Notification generator.
     */
    @Autowired
    private NotificationProcessing notificationProcessing;
    /**
     * HTTP Client object.
     */
    @Autowired
    private HttpClient httpClient;

    /**
     * NATs controller.
     */
    @Autowired
    private NatsController natsController;

    /**
     * Jackson Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;
    /**
     * Constant XBAND_PKT_SIZE : packet size.
     */
    private static final int XBAND_PKT_SIZE = 1022;
    /**
     * X-band Frame deserializer class.
     */
    @Autowired
    private BandxFrameDeserializer bandxFrameDeserializer;


    /**
     * Find all transfer frames.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page<BandxTransferFrame>
     */
    public Page<BandxTransferFrame> findAllFrames(Predicate qry, Pageable req) {
        Page<BandxTransferFrame> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = bandxPacketRepository.findAll(req);
        }
        else {
            entitylist = bandxPacketRepository.findAll(qry, req);
        }
        // Return frames.
        return entitylist;
    }

    /**
     * Find by ID.
     *
     * @param frameId frame Identifier.
     * @return BandxTransferFrameDto DTO.
     * @throws BandxServiceException exception.
     */
    @Transactional
    public BandxTransferFrame findBandxTransferFrame(Long frameId) throws BandxServiceException {
        log.info("Search for bandx Transfer frame  {}", frameId);
        if (frameId == null) {
            throw new XbandSQLException("Bad parameter for query findBandxTransferFrame: id cannot be null");
        }
        Optional<BandxTransferFrame> entity = bandxPacketRepository.findById(frameId);
        if (!entity.isPresent()) {
            throw new NotFoundException("Cannot find bandx transfer frame " + frameId);
        }
        return entity.orElse(null);
    }

    /**
     * @param apid  decimal apid value.
     * @param obsid decimal obsid.
     * @param t1    time interval borne 1.
     * @param t2    time interval borne 2.
     * @return List<BandxTransferFrame>
     */
    public List<BandxTransferFrame> findBandxTransferFrames(Integer apid, Integer obsid,
                                                            String t1, String t2) {
        List<BandxTransferFrame> entitylist = null;

        /* query with apid filtering only , no obsid query parameter */
        if (apid != 0 && obsid == 0 && ("none").equals(t1) && ("none").equals(t2)) {
            entitylist = bandxPacketRepository.findByApidPacket(apid);
            return entitylist;
        }
        /* query with time filtering only*/
        if (apid == 0 && obsid == 0 && !("none").equals(t1) && !("none").equals(t2)) {
            Long elapst1 = Long.parseLong(t1);
            Long elapst2 = Long.parseLong(t2);
            entitylist = bandxPacketRepository.findByTime(elapst1, elapst2);
            return entitylist;
        }
        /* query with time filtering only*/
        if (apid != 0 && obsid == 0 && !("none").equals(t1) && !("none").equals(t2)) {
            Long elapst1 = Long.parseLong(t1);
            Long elapst2 = Long.parseLong(t2);
            entitylist = bandxPacketRepository.findByApidAndTime(apid, elapst1, elapst2);
            return entitylist;
        }
        //entitylist = bandxPacketRepository.findAll();

        return entitylist;

    }

    /**
     * @param lcfilename L0C file name.
     * @return List<BandxTransferFrame>
     */
    @Transactional
    public List<BandxTransferFrame> findFramesbyLOC(String lcfilename) {
        if (lcfilename == null) {
            throw new XbandSQLException("Bad query parameter for findFramesbyLOC: lcfilename cannot be null");
        }
        return bandxPacketRepository.findByL0cFilename(lcfilename);
    }

    /**
     * @param frameList frames list.
     * @return List<String>
     */
    @Transactional
    public List<String> deleteFramesByL0C(List<BandxTransferFrame> frameList) {
        log.info(" delete frames and packets");
        List<String> packetList = new ArrayList<>();
        for (BandxTransferFrame frame : frameList) {
            log.debug(" - remove frame {}", frame);
            packetList.add(frame.getBinaryHash());
            bandxPacketRepository.delete(frame);
        }
        log.info(" finish deletion process for frames {}", frameList.size());
        return packetList;
    }

    /**
     * Delete list of packets.
     *
     * @param packetList
     */
    @Transactional
    public void deletePackets(List<String> packetList) {
        log.info(" delete packets");
        for (String hashid : packetList) {
            log.debug(" - remove packet {}", hashid);
            dataSourceRepository.deleteById(hashid);
        }
        log.info(" finish deletion process for packets = {}", packetList.size());
    }

    /**
     * Delete list of frames using lcfile_id.
     *
     * @param lcfileid
     */
    @Transactional
    public void deleteFramesByLcFileId(Long lcfileid) {
        log.info(" delete frames by lcfile ID");
        long ndel = bandxPacketRepository.deleteByLcDatFileId(lcfileid);
        log.info(" finish deletion process for frames = {}", ndel);
    }


    /**
     * Delete list of packets using lcfile_id.
     *
     * @param lcfileid
     */
    @Transactional
    public void deletePacketsByLcFileId(Long lcfileid) {
        log.info(" delete packets by lcfile ID");
        long ndel = dataSourceRepository.deleteByLcfileId(lcfileid);
        log.info(" finish deletion process for packets = {}", ndel);
    }

    /**
     * @param dataType     type of received file (.dat or archive .tar).
     * @param receivedFile the received File.
     * @return nb frames created.
     * @throws AbstractCdbServiceException If exception occurs.
     */
    public Integer storeBandxPacket(String dataType, File receivedFile) throws AbstractCdbServiceException {
        log.debug("Store xband packets from file in path:{}", receivedFile.getAbsolutePath());
        File extractedArchive = null;
        TarFile tarfile = null;
        List<File> lcdatfilelist = new ArrayList<>();
        if (FileType.ARCHIVE.ftype.equals(dataType)) {
            log.info("Body Parameter: Archive  (.tar) File ");
            /** extract and create tar file **/
            tarfile = lcfileProcessing.createTarfile(receivedFile);
            /** store tar file: if already in DB it will retrieve the entity */
            tarfile = tarFileService.storeTarFile(tarfile);

            /*** extract tar file archive  **/
            try (InputStream uploadedIn = new FileInputStream(receivedFile)) {
                extractedArchive = lcfileProcessing.extractArchive(receivedFile, uploadedIn,
                        serverUploadFolder);
                log.info("archive is stored in {}", extractedArchive.getAbsolutePath());
            }
            catch (IOException e) {
                throw new XbandDecodingException("Cannot read from " + receivedFile, e);
            }
            lcdatfilelist = lcfileProcessing.tarfileExtract(extractedArchive, tarfile);
            log.debug("Extracted list of files from tar : {} elements", lcdatfilelist.size());
        }
        else if (FileType.DAT.ftype.equals(dataType)) {
            log.info("Body Parameter: .Dat File ");
            lcdatfilelist.add(receivedFile);
            log.debug("Add one entry to list of files : {} elements", lcdatfilelist.size());
        }
        log.info("Start processing list of n={} files ", lcdatfilelist.size());
        Integer nbFrames = 0;
        DataNotification dn = null;
        for (File afile : lcdatfilelist) {
            log.info("Analyse file {} ", afile.getName());
            String fileExt = FileFormatUtils.getFileExtension(afile);
            if (("xml").equalsIgnoreCase(fileExt)) {
                // Processing the xml.
                log.info(" >>> process metadata File");
                LcMetadata l0cMetadata = lcfileProcessing.xmlDataProcessing(afile);
                if (l0cMetadata == null || tarfile == null) {
                    log.error("Wrong XML file name or content or tar file value is null....cannot process...skip "
                              + "metadata");
                    continue;
                }
                log.debug("Associate metadata with tar file {}", tarfile);
                l0cMetadata.setTarFile(tarfile);
                l0cMetadata.setIsXmlNameConform(Boolean.TRUE);
                l0cMetadata.setProductId("ED_0654");
                unitBxPacketService.storeLcMetadata(l0cMetadata);
                // Create notification. This is done only when we have an XML file.
                // When the processing of all files is finished we send the notification.
                List<String> datfiles = lcMetadataParser.getDatfileList(afile);
                dn = createNotification(l0cMetadata, datfiles);
            }
            else if (("dat").equalsIgnoreCase(fileExt)) {
                // Processing a dat.
                log.info(" >>> process .dat File ");
                Long pktNb = afile.length() / XBAND_PKT_SIZE;
                if (afile.length() == 0
                    || afile.length() - (pktNb * XBAND_PKT_SIZE) != 0) {
                    log.error("corrupted binary packet...skip file {}", afile.getName());
                    continue;
                }
                nbFrames += lcfileProcessing.lcDataProcessing(afile, tarfile);
            }
            else {
                // May be we can skip processing instead of sending an exception.
                log.error("Cannot process file with extension {}...skip it", fileExt);
            }
        }
        // Clean up the extracted archieve.
        if (extractedArchive != null) {
            FileSystemUtils.deleteRecursively(extractedArchive);
        }
        if (nbFrames > 0 && dn != null) {
            log.info("Send notification {} after archive file processing", dn);
            this.publishNotification(dn);
        }
        log.debug("Return number of frames {}", nbFrames);
        return nbFrames;
    }

    /**
     * Create the data notification object using lc_metadata information.
     *
     * @param lcMetadata
     * @param datfiles
     * @return DataNotification
     */
    protected DataNotification createNotification(LcMetadata lcMetadata, List<String> datfiles) {
        DataNotification dn = new DataNotification();

        Instant now = Instant.now();
        dn.setMessageDate(now.toString());
        DataNotificationContent content = new DataNotificationContent();

        content.setDate(now.atOffset(ZoneOffset.UTC));
        content.setDataProvider(DataProvider.XBANDDB);
        content.dataStream(DataStream.XBAND);
        content.productCard(lcMetadata.getType());
        content.setObsid("0");
        content.setResourceLocator(lcMetadata.getTarFile().getTarName());
        StringBuilder msgbuilder = new StringBuilder();
        msgbuilder.append("{ \"apid\": \"" + lcMetadata.getApid()
                          + "\", \"datfiles\": [");
        for (String fn : datfiles) {
            msgbuilder.append("\"" + fn + "\",");
        }
        msgbuilder.deleteCharAt(msgbuilder.length() - 1);
        msgbuilder.append("]}");
        content.setMessage(msgbuilder.toString());
        dn.content(content);
        return dn;
    }

    /**
     * Create the data notification object using list of packet id apids information.
     *
     * @param dtolist List of PacketApidIdDto
     * @param passid  the pass ID
     * @return List of DataNotification
     */
    public List<DataNotification> createNotification(List<PacketApidIdDto> dtolist, String passid) {
        List<DataNotification> dnlist = new ArrayList<>();
        Instant now = Instant.now();
        // The logic here needs to be more complex, for the moment just send a
        // notification for each packetApidId pair. In future one should trigger
        // a notification only if all needed packet ids are present for a given apid
        // and a given notification.
        Map<String, List<PacketApidIdDto>> notmap = notificationProcessing.getNotificationCondition(dtolist);
        for (Map.Entry<String, List<PacketApidIdDto>> entry : notmap.entrySet()) {
            log.info("Create notification: {}", entry);
            String notifName = entry.getKey();
            DataNotification dn = new DataNotification();

            dn.setMessageDate(now.toString());
            DataNotificationContent content = new DataNotificationContent();

            content.setDate(now.atOffset(ZoneOffset.UTC));
            content.setDataProvider(DataProvider.XBANDDB);
            content.dataStream(DataStream.XBAND);
            content.productCard(notifName);
            content.setObsid("0");
            content.setResourceLocator(passid);
            StringBuilder msgbuilder = new StringBuilder();
            msgbuilder.append("{ \"pass_id\": \"" + passid + "\"}");
            content.setMessage(msgbuilder.toString());
            dn.content(content);
            log.info("Add notification to list : {}", dn);
            dnlist.add(dn);
        }
        log.info("Return notification list of size : {}", dnlist.size());
        return dnlist;
    }

    /**
     * Publish notification via nats. Use xband_data queue.
     *
     * @param dn
     */
    public void publishNotification(DataNotification dn) {
        /** json format **/
        try {
            if (dn == null) {
                log.error("Cannot publish null notification");
                return;
            }
            String jsonNotif = jacksonMapper.writeValueAsString(dn);
            log.info("Sending notification to jetstream in channel {}: {}", NatsQueue.XBAND_DATA.getQueue(), jsonNotif);
            natsController.publishToQueue(NatsQueue.XBAND_DATA.getQueue(), jsonNotif);
        }
        catch (JsonProcessingException e) {
            log.error("Error in message conversion {}", e);
        }
    }

    /**
     * @param obsid
     * @param apid
     * @param station
     * @param orbit
     * @param packetid
     * @param preq     the Pageable
     * @return list of obsid.
     */
    public Page<FrameObsid> findObsidList(Long obsid, Integer apid, String station, Integer orbit, Integer packetid,
                                          Pageable preq) {
        if (preq == null) {
            throw new XbandRequestFormatException("Pageable object is null");
        }
        return frameObsidRepository.listObsidInDatFiles(obsid, apid, station, orbit, packetid, preq);
    }

    /**
     * @param station
     * @param orbit
     * @return list of apids.
     * @throws AbstractCdbServiceException If an exception occur.
     */
    public List<Integer> findApidsList(String station, Integer orbit) {
        if (station == null || orbit == null) {
            throw new XbandSQLException("bad arguments for findApidsList: station and orbit cannot be null");
        }
        return bandxPacketRepository.apidsListByPassId(station, orbit);
    }


    /**
     * @param fList packet list.
     * @return byte[]
     */
    public byte[] createNewBinaryFile(List<DataSourcePacket> fList) {

        /** retrieve corresponding binary packets**/
        byte[] fileBytes = new byte[XBAND_PKT_SIZE * fList.size()];
        for (int i = 0; i < fList.size(); i++) {
            //String filePath = fList.get(i).getData();
            //byte[] frameBytes = ByteOperationServices.readBinaryFile(new File(filePath));
            byte[] frameBytes = fList.get(i).getPacket();
            //log.info("DUMP: 0x{}", DatatypeConverter.printHexBinary(frameBytes));
            int k = 0;
            for (int j = XBAND_PKT_SIZE * i; j < XBAND_PKT_SIZE * (i + 1); j++) {
                fileBytes[j] = frameBytes[k];
                k++;
            }
        }
        return fileBytes;
    }

    /**
     * Generate a string from the file extension to indicate the data type.
     * Based on this we will perform different kind of processing.
     *
     * @param receivedFile filename.
     * @return string the data type.
     * @throws AbstractCdbServiceException exception of file name extension.
     */
    public String datafileType(File receivedFile) throws AbstractCdbServiceException {

        /*** get data type ***/
        /** check if filename extension is conform **/
        String fileExt = FileFormatUtils.getFileExtension(receivedFile);
        log.debug("File extension is {}", fileExt);
        if (!("DAT").equalsIgnoreCase(fileExt) && !("TAR").equalsIgnoreCase(fileExt)) {
            // Wrong extension.
            throw new XbandDecodingException("FILENAME EXTENSION IS INCORRECT: "
                                             + "accepted extension: .dat, .DAT, .TAR, .tar");
        }
        /** file type check to assign data type value */
        if (("dat").equalsIgnoreCase(fileExt)) {
            return FileType.DAT.getFtype();
        }
        return FileType.ARCHIVE.getFtype();
    }

    /**
     * @param fileLocation L0C file location.
     * @return number of frames created.
     * @throws AbstractCdbServiceException exception when downloading file.
     */
    @Deprecated
    @Transactional
    public File downloadFile(String fileLocation) throws AbstractCdbServiceException {
        log.info("download file from distant serevr");
        /** file location example:
         *       ftp://software.svom.fr/pub/SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar
         *       uri.getScheme():ftp
         *       uri.getAuthority: software.svom.fr
         *       uri.getPath():pub/SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar
         * */
        try {
            URI uri = URI.create(fileLocation);
            // check protocol and path.
            String protocol = uri.getScheme();
            String newfilePath = uri.getPath();
            // protocol can be http or https.
            if (protocol != null) {
                log.debug("Exchange protocol is {}", protocol);
                if ("http".equals(protocol) || "https".equals(protocol)) {
                    newfilePath = downloadArchiveFromHTTP(uri);
                }
            }
            // Return the file.
            return new File(newfilePath);
        }
        catch (IOException e) {
            // Problem in downloading.
            throw new FileDownloadException("Cannot download file from " + fileLocation, e);
        }
    }

    /**
     * @param uri uri of file location.
     * @return downloaded file path.
     * @throws IOException exception.
     */
    public String downloadArchiveFromHTTP(URI uri) throws IOException {
        log.info("http client get file");
        httpClient.httpFileDownload(uri.toString(), serverUploadFolder);
        String path = uri.getPath();
        String remoteFileName = path.split("/")[path.split("/").length - 1];
        return serverUploadFolder + '/' + remoteFileName;
    }

    /**
     * @param fileName    received archive name
     * @param inputStream tar file input stream.
     * @return file
     * @throws AbstractCdbServiceException exception.
     */
    public File getFile(String fileName, InputStream inputStream) throws AbstractCdbServiceException {

        log.info("save file {} from body param in memory ", fileName);
        try {
            String tarpath = serverUploadFolder + '/' + fileName;
            File targetFile = new File(tarpath);
            java.nio.file.Files.copy(
                    inputStream,
                    targetFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            IOUtils.closeQuietly(inputStream);

            log.info("file downloaded into {}", tarpath);
            return targetFile;
        }
        catch (IOException e) {
            throw new FileDownloadException("Cannot get file from input", e);
        }
    }

    /**
     * Retrieve a list of packet ids and apids for a given pass_id.
     *
     * @param station
     * @param orbit
     * @return List of PacketApidIdDto
     */
    public List<PacketApidIdDto> findApidPacketIdList(String station, Integer orbit) {
        log.info("Retrieve list of apids and packet ids in pass = {} {}", station, orbit);
        return jdbcRepository.findApidPacketByPassId(station, orbit);
    }

    /**
     * Enumeration.
     */
    public enum FileType {
        /**
         * archive.
         */
        ARCHIVE("tar"),
        /**
         * archive.
         */
        DAT("dat");
        /**
         * ftype.
         */
        private String ftype;

        /**
         * @param type type of file.
         */
        FileType(String type) {
            this.ftype = type;
        }

        /**
         * @return type
         */
        public String getFtype() {
            return ftype;
        }

    }
}
