package fr.svom.xband.server.service;

import com.querydsl.core.types.Predicate;
import fr.svom.xband.data.exceptions.NotExistsPojoException;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.repository.LcDatFileRepository;
import fr.svom.xband.data.repository.custom.PassIdRepository;
import fr.svom.xband.swagger.model.PassIdDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author etrigui.
 */
@Service
public class LcFileService {
    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LcFileService.class);

    /**
     * lcdat repository object.
     */
    @Autowired
    private LcDatFileRepository lcRepository;
    /**
     * passid repository object.
     */
    @Autowired
    private PassIdRepository passIdRepository;

    /**
     * @param lcdatfile L0C file.
     * @return Boolean
     * True if index =2 and it dont exist same lcdatfile with index=1
     */
    public Boolean indexConform(LcDatFile lcdatfile) {
        Boolean isconform = true;
        if (lcdatfile.getIndex() == 2) {
            log.debug("Search for lcdatfile with index 1");

            String lcfilename1 = lcdatfile.getLcfileName().replace("_2.", "_1.");
            LcDatFile entity = findLcfileByName(lcfilename1);
            if (entity != null) {
                isconform = false;
            }
        }
        log.info("indexConform function: {}", isconform);
        return isconform;
    }

    /**
     * @param lcdatfile Lc File.
     * @return Boolean
     * True if exist Lcdatfile with same <orbit>,<station>,<index>
     */
    public Boolean tripletNameExist(LcDatFile lcdatfile) {
        log.info("Search for lcdatfile with same <orbit>,<station>,<index> as lcdatfile ");
        Boolean result = false;
        // Check triplet.
        String apid = lcdatfile.getApidHexa();
        Integer orbit = lcdatfile.getOrbit();
        String station = lcdatfile.getStation();
        Integer index = lcdatfile.getIndex();
        List<LcDatFile> entity = lcRepository.findByApidOrbitStationIndex(apid, orbit,
                station, index);
        if (!entity.isEmpty()) {
            log.debug("no another lcdatfile having same <orbit,station,index>");
            result = true;
        }
        log.info("tripletNameExist : {}", result);
        return result;
    }

    /**
     * @param lcfilename L0C .dat file name.
     * @return LcDatFile.
     */
    public LcDatFile findLcfileByName(String lcfilename) {
        Optional<LcDatFile> lcdatfileopt = lcRepository.findByLcfileName(lcfilename);
        if (lcdatfileopt.isPresent()) {
            return lcdatfileopt.get();
        }
        return null;
    }

    /**
     * Find all dat files.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page<LcDatFile>
     */
    public Page<LcDatFile> findAllDatFiles(Predicate qry, Pageable req) {
        Page<LcDatFile> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = lcRepository.findAll(req);
        }
        else {
            entitylist = lcRepository.findAll(qry, req);
        }
        // Return the dat files.
        return entitylist;
    }

    /**
     * Delete a dat file.
     *
     * @param filename
     */
    @Transactional
    public void deleteDatFile(String filename) {
        // Find the lcDat entity.
        LcDatFile lcdatfile = lcRepository.findByLcfileName(filename).orElseThrow(
                () -> new NotExistsPojoException("Cannot find dat file to delete for name " + filename)
        );
        lcRepository.delete(lcdatfile);
    }

    /**
     * @param lcDatFile L0C file (.DAT).
     * @return Boolean
     */
    public Boolean existsLcfile(LcDatFile lcDatFile) {
        log.info("Check if lcDatFile is in DB: {}", lcDatFile.getLcfileName());
        // Check if entity exists.
        LcDatFile lcdatfile = this.findLcfileByName(lcDatFile.getLcfileName());
        if (lcdatfile != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * @param lcDatFile L0C file (.DAT).
     * @return lcDatFile
     */
    public LcDatFile storeLcfile(LcDatFile lcDatFile) {
        log.info("Store lc datfile {} - {}", lcDatFile.getLcfileName(), lcDatFile.getLcfileId());
        // Find if already exists.
        LcDatFile lcdatfile = this.findLcfileByName(lcDatFile.getLcfileName());
        if (lcdatfile != null) {
            return lcdatfile;
        }
        // If not then save it.
        return lcRepository.save(lcDatFile);
    }

    /**
     * Search pass ids list.
     *
     * @param orbit
     * @param apid
     * @param station
     * @param starttime
     * @param endtime
     * @param preq
     * @return Page of PassIdDto
     */
    public Page<PassIdDto> findAllPassIds(Integer orbit, String apid, String station, String starttime,
                                          String endtime, Pageable preq) {
        if ("all".equalsIgnoreCase(station)) {
            station = null;
        }
        return passIdRepository.listPassIdInDatFiles(apid, station, orbit, starttime, endtime, preq);
    }
}
