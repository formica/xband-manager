/**
 * 
 */
package fr.svom.xband.server.autoconfig;

import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.exceptions.BandxServiceException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.PropertySources;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Post processor to retrieve secrets from Docker.
 *
 * @author formica
 *
 */
public class DockerEnvironmentPostProcessor implements EnvironmentPostProcessor {

    /**
     * PROPERTY_SOURCE_NAME.
     */
    private static final String PROPERTY_SOURCE_NAME = "vhfpassProperties";
    /**
     * The map containing the secrets.
     */
    private static final Map<String, String> SECRET_MAP;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DockerEnvironmentPostProcessor.class);

    static {
        SECRET_MAP = new HashMap<>();
        SECRET_MAP.put("/run/secrets/svom-pg-xband", "svom.service.postgres_password");
        SECRET_MAP.put("/run/secrets/nats_password", "svom.nats.password");
        SECRET_MAP.put("/run/secrets/xbandtruststore_password", "store.password");
        SECRET_MAP.put("/run/secrets/xbandkeystore_password", "key.password");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.boot.env.EnvironmentPostProcessor#postProcessEnvironment(
     * org.springframework.core.env.ConfigurableEnvironment,
     * org.springframework.boot.SpringApplication)
     */
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment,
            SpringApplication application) {
        log.info("POSTPROCESS ENV is configuring {}", PROPERTY_SOURCE_NAME);
        final Map<String, Object> map = new HashMap<>();
        try {
            for (final Map.Entry<String, String> entry : SECRET_MAP.entrySet()) {
                final String springkey = entry.getValue();
                final String respath = entry.getKey();
                loadSecret(respath, springkey, environment, map);
            }
        }
        catch (final AbstractCdbServiceException e) {
            log.error("POSTPROCESS ENV Exception {}", e);
        }
    }

    /**
     * Load secrets from environment.
     *
     * @param secpath
     *            the String
     * @param springkey
     *            the String
     * @param environment
     *            the ConfigurableEnvironment
     * @param map
     *            the Map<String, Object>
     * @throws BandxServiceException
     *             If an Exception occurred
     */
    private void loadSecret(String secpath, String springkey, ConfigurableEnvironment environment,
            Map<String, Object> map) throws BandxServiceException {
        final Resource resource = new FileSystemResource(secpath);
        try {
            if (resource.exists()) {
                String mPassword = getStringFromInputStream(resource.getInputStream());
                mPassword = mPassword.replace("\n", "");
                map.put(springkey, mPassword);
                if ("store.password".equals(springkey)) {
                    System.setProperty("javax.net.ssl.trustStorePassword", mPassword);
                    System.setProperty("javax.net.ssl.trustStore", "/run/secrets/truststore.jks");
                }
                addOrReplace(environment.getPropertySources(), map);
            }
            else {
                log.warn("CANNOT FIND secret in {} for {} ", secpath, PROPERTY_SOURCE_NAME);
            }
        }
        catch (final IOException e) {
            throw new BandxServiceException("Error getting secrets", e);
        }
    }

    /**
     * Read the input stream.
     *
     * @param input
     *            the InputStream
     * @return String
     * @throws IOException
     *             If an Exception occurred
     */
    private String getStringFromInputStream(InputStream input) throws IOException {
        final StringWriter writer = new StringWriter();
        IOUtils.copy(input, writer, "UTF-8");
        return writer.toString();
    }

    /**
     * Add or replace values in secrets map.
     *
     * @param propertySources
     *            the MutablePropertySources
     * @param map
     *            the Map<String, Object>
     */
    private void addOrReplace(MutablePropertySources propertySources, Map<String, Object> map) {
        MapPropertySource target = null;
        if (propertySources.contains(PROPERTY_SOURCE_NAME)) {
            final PropertySource<?> source = propertySources.get(PROPERTY_SOURCE_NAME);
            if (source instanceof MapPropertySource) {
                target = (MapPropertySource) source;
                for (final Map.Entry<String, Object> entry : map.entrySet()) {
                    final String key = entry.getKey();
                    if (!target.containsProperty(key)) {
                        target.getSource().put(key, entry.getValue());
                    }
                    else {
                        log.debug("Key {} is already in {}", key, target.getName());
                    }
                }
            }
        }
        if (target == null) {
            target = new MapPropertySource(PROPERTY_SOURCE_NAME, map);
        }
        if (!propertySources.contains(PROPERTY_SOURCE_NAME)) {
            propertySources.addFirst(target);
        }
    }

    /**
     * Utility method to dump content of PropertySources.
     *
     * @param propertySources
     *            the PropertySources
     */
    @SuppressWarnings("unused")
    private void dumpAll(PropertySources propertySources) {
        for (final PropertySource<?> propertySource : propertySources) {
            log.info("Found property source: {} : {}", propertySource.getName(), propertySource);
            if (propertySource instanceof MapPropertySource) {
                final String[] keys = ((MapPropertySource) propertySource).getPropertyNames();
                for (int i = 0; i < keys.length; i++) {
                    final String key = keys[i];
                    final Object val = ((MapPropertySource) propertySource).getProperty(key);
                    log.info("   contains property: {} : {} ", key, val);
                }
            }
        }
    }
}
