package fr.svom.xband.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author etrigui
 */

@Service
public class DataHeaderPacketService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DataHeaderPacketService.class);

    /**
     * Default Ctor.
     */
    public DataHeaderPacketService() {
        super();
        log.debug("DataHeaderPacketService constructor");
    }

    /**
     * @param tsec   packet time in seconds.
     * @param tmicro packet time in microseconds.
     * @return double
     * Time.
     */
    public double getTime(Integer tsec, Integer tmicro) {
        final int dix = 10;
        return tsec.doubleValue() + tmicro / Math.pow(dix, String.valueOf(tmicro).length());
    }

    /**
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return string
     * TargetId
     */
    public String getTargetIdHexa(Integer obsidType, Integer obsidNum) {
        return Integer.toHexString(obsidType) + Integer.toHexString(obsidNum);

    }

    /**
     * @param targetId target identifier.
     * @return boolean
     * true if a science packet
     */
    public boolean isSciencePacket(String targetId) {
        return ("FFFFFFFF").equalsIgnoreCase(targetId);

    }

    /**
     * conversion du obsid t type + Num) en string (hexa).
     *
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return string
     * obsid hexadecimal
     */
    public String hexObsidStr(Integer obsidType, Integer obsidNum) {

        String obsType = Integer.toHexString(obsidType);
        String obsNum = Integer.toHexString(obsidNum);
        String hexObsid = obsType.concat(obsNum);
        log.debug("full obsid string hexa: {}", hexObsid);
        return hexObsid;
    }

    /**
     * conversion du obsid t type + Num) en long.
     *
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return long
     * obsid
     */
    public long longObsid(Integer obsidType, Integer obsidNum) {

        String hexObsid = hexObsidStr(obsidType, obsidNum);
        final int decimalBase = 16;
        long obsid = Long.parseLong(hexObsid, decimalBase);
        log.debug("full obsid long: {}", obsid);
        return obsid;
    }

    /**
     * conversion du obsid t type + Num) en unsigned int.
     *
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return int
     * obsid
     */
    public int unsignedObsid(Integer obsidType, Integer obsidNum) {

        String hexObsid = hexObsidStr(obsidType, obsidNum);
        final int decimalBase = 16;
        Integer obsid = Integer.parseUnsignedInt(hexObsid, decimalBase);
        log.debug("full obsid int: {}", obsid);
        return obsid;
    }

    /**
     * conversion du obsid (type + Num) en string.
     *
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return string
     * obsid
     */
    public String fullObsidUnsignedInt(Integer obsidType, Integer obsidNum) {

        long lObsid = longObsid(obsidType, obsidNum);
        String decimalObsid = Long.toString(lObsid);
        log.debug("full obsid string long decimal: {}", decimalObsid);
        int vInt = Integer.parseUnsignedInt(decimalObsid); // == -1
        String sInt = Integer.toUnsignedString(vInt);
        log.debug("full obsid string int decimal: {}", sInt);
        return sInt;
    }
}
