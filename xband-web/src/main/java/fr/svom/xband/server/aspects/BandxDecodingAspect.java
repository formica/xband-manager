package fr.svom.xband.server.aspects;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.xband.server.externals.NatsController;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author etrigui.
 */

/**
 * * BandxDecodingAspect class.
 */
@Aspect
@Component
@Slf4j
public class BandxDecodingAspect {

    /**
     * Nats controller.
     */
    @Autowired
    private NatsController nats;

    /**
     * Jackson Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;

    /**
     * Ctor.
     *
     * @param nats          NatsController.
     * @param jacksonMapper ObjectMapper.
     */
    @Inject
    public BandxDecodingAspect(NatsController nats, ObjectMapper jacksonMapper) {
        super();
        this.nats = nats;
        this.jacksonMapper = jacksonMapper;
    }

    @Around("execution(* fr.svom.xband.server.service.FramesBandxPacketService.storeBandxPacket(..))")
    public Object sendNotificationOnL0cProcessing(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Nats messaging : send notif around storeBandxPacket");
        Object[] args = joinPoint.getArgs();
        Object ret = joinPoint.proceed(args);
        Integer nframes = (Integer) ret;
        log.info("Aspect has seen {} frames processed", nframes);
        return ret;
//        if (nframes > 0) {
//            DataNotification dn = new DataNotification();
//
//            Instant now = Instant.now();
//            dn.setMessageDate(now.toString());
//            DataNotificationContent content = new DataNotificationContent();
//
//            content.setDate(now.toString());
//            content.setSource(DataSource.fromValue("XDB"));
//            content.setDataType(dataType);
//            content.setObsid("0");
//            content.setResourceLocator(receivedFile.getName());
//            content.setMessage("l0d file is processed");
//            dn.content(content);
//            String jsonNotif = null;
//            /** json format **/
//            try {
//                jsonNotif = jacksonMapper.writeValueAsString(dn);
//            }
//            catch (JsonProcessingException e) {
//                log.error("Error in message conversion {}", e);
//            }
//            nats.publishToQueue(NatsQueue.XBAND_DATA.getQueue(), jsonNotif);
//        }
    }

    /**
     * * send message after tarfile processing.
     *
     * @param res result.
     */
    @AfterReturning(pointcut =
            "execution(* fr.svom.xband.server.service.FramesBandxPacketService.storeBandxPacket(..))",
            returning = "res")
    public void sendldMessage(Object res) {
        log.info("Nats messaging : send notif after returning from storeBandxPacket");
        String result = res.toString();
        log.warn("For the moment we skip this...returned frames are {}", result);
//        String result = res.toString();
//        /** create Data notification */
//        DataNotification dn = new DataNotification();
//
//        Instant now = Instant.now();
//        dn.setMessageDate(now.toString());
//        DataNotificationContent content = new DataNotificationContent();
//
//        content.setDate(now.toString());
//        content.setSource(DataSource.fromValue("XDB"));
//        content.setDataType("L0D");
//        content.setObsid("0");
//        content.setResourceLocator(result);
//        content.setMessage("l0d file is stored");
//        dn.content(content);
//        String jsonNotif = null;
//        /** json format **/
//        try {
//            jsonNotif = jacksonMapper.writeValueAsString(dn);
//        }
//        catch (JsonProcessingException e) {
//            log.error("Error in message conversion {}", e);
//        }
//        nats.publishToQueue(NatsQueue.XBAND_DATA.getQueue(), jsonNotif);
    }
}
