package fr.svom.xband.server.service;

import org.springframework.stereotype.Service;

import fr.svom.xband.data.model.BandxTransferFrame;

/**
 * @author etrigui
 *
 */
@Service
public class BandxLogMessageService {

    /**
     * Default Ctor.
     */
    public BandxLogMessageService() {
        super();
    }

    /**
     * @param frame
     *        BandxTransferFrame.
     * @return String
     *        message that will be stored
     */
    public String getStoreMessage(BandxTransferFrame frame) {
        return "APID=" + frame.getApidPacket().getApidValue().toString()
                + ";INSTRUMENT=" + frame.getApidPacket().getInstrument()
                + ";CATEGORY=" + frame.getApidPacket().getCategory()
                + ";PKTTIME=" + frame.getPacketTimeSec()
                + ";OBSID=" + frame.getObsid();
    }

}
