package fr.svom.xband.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.BandxServiceException;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.data.repository.querydsl.SearchCriteria;
import fr.svom.xband.server.controllers.EntityDtoHelper;
import fr.svom.xband.server.controllers.PageRequestHelper;
import fr.svom.xband.server.service.BinaryPacketService;
import fr.svom.xband.server.service.FramesBandxPacketService;
import fr.svom.xband.server.swagger.api.ApiResponseMessage;
import fr.svom.xband.server.swagger.api.NotFoundException;
import fr.svom.xband.server.swagger.api.PacketsApiService;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.DataSourcePacketDto;
import fr.svom.xband.swagger.model.DataSourceSetDto;
import fr.svom.xband.swagger.model.RespPage;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2018-07-16T16:10"
                                                                                                   + ":55.895+02:00")

@Component
public class PacketsApiServiceImpl extends PacketsApiService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PacketsApiServiceImpl.class);

    /**
     * BinaryPacket Service.
     */
    @Autowired
    BinaryPacketService binaryPacketService;

    /**
     * FramesBandxPacketService.
     */
    @Autowired
    FramesBandxPacketService bandxPacketService;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("dataSourceFiltering")
    private IFilteringCriteria dataSourceFiltering;

    /**
     * Helper.
     */
    @Autowired
    EntityDtoHelper edh;

    /**
     * folder Location for downloaded files.
     */
    @Value("${xband.download.dir}")
    private String saveLocationFolder;


    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.BinaryPacketsApiService#findBandxBinaryPacket(java.lang.String, javax.ws
     * .rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response findBandxBinaryPacket(String hashid, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        try {
            log.info("Search binary packet using hash ID {}", hashid);
            /**find binary packet by hashid  */
            DataSourcePacket entity = binaryPacketService.findbinaryByhash(hashid);
            if (entity == null) {
                log.error("Cannot find hash {}", hashid);
                throw new BandxServiceException("Cannot find binary hash "+hashid);
            }
            /** return BinaryPacket entity as response message **/
            DataSourcePacketDto dto = mapper.map(entity, DataSourcePacketDto.class);
            return Response.ok().entity(dto).build();
        }
        catch (BandxServiceException e) {
            /** return exception error message **/
            String message = "No resource has been found";
            ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.ERROR, message);
            return Response.status(Response.Status.NOT_FOUND).entity(resp).build();
        }
    }

    /* (non-Javadoc)
     * @see fr.svom.xband.server.swagger.api.BinaryPacketsApiService#listBinaryPackets(java.lang.String, java.lang
     * .Integer, java.lang.Integer, java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listBinaryPackets(String by, Integer page, Integer size, String sort, String datatype,
                                      SecurityContext securityContext, UriInfo info) throws NotFoundException {
        /** search list of packet apid. **/
        log.info("Search resource list by {}", by);
        try {
            if (datatype == null) {
                datatype = "dto";
            }
            // Create filters
            List<SearchCriteria> criteria = prh.createMatcherCriteria(by);
            Map<String, Object> filters = prh.getFilters(criteria);
            String outfilename = prh.getCriteriaAsString(criteria);
            if (outfilename.isEmpty()) {
                outfilename = "binarypackets.bin";
            }
            log.info("Use file name {}", outfilename);
            // Create pagination request
            final PageRequest preq = prh.createPageRequest(page, size, sort);
            BooleanExpression wherepred = null;
            if (!"none".equals(by)) {
                // Create search conditions for where statement in SQL
                wherepred = prh.buildWhere(dataSourceFiltering, by);
            }
            // Retrieve frame list using filtering.
            Page<DataSourcePacket> entitypage = binaryPacketService.findAllFrames(wherepred, preq);
            RespPage respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());
            List<DataSourcePacketDto> dtolist = edh.entityToDtoList(entitypage.toList(), DataSourcePacketDto.class);
            Response.Status rstatus = Response.Status.OK;
            if ("dto".equals(datatype)) {
                // Create the Set.
                final BaseResponseDto setdto = buildEntityResponse(dtolist, filters);
                setdto.page(respPage);
                // Send a response and status 200.
                return Response.status(rstatus).entity(setdto).build();
            }
            else {
                String binFilename = saveLocationFolder + '/' + outfilename + ".dat";
                //File outfile = bandxPacketService.createNewBinaryFile(result, outfilename + ".dat");
                // Now we need to loop over the pages
                int ip = page;
                log.info("using file {}", binFilename);
                Path fich = Paths.get(binFilename);
                log.info("create new");
                if (fich.toFile().exists()) {
                    Files.deleteIfExists(fich);
                    fich = Paths.get(binFilename);
                }
                OutputStream os = new FileOutputStream(fich.toFile());
                do {
                    log.info("dump page {}: {} - {}", ip, entitypage.getTotalPages(), entitypage.getNumber());
                    byte[] farr = bandxPacketService.createNewBinaryFile(entitypage.toList());
                    log.info("load binary data arr of size {}", farr.length);
                    //log.info("0x{}", DatatypeConverter.printHexBinary(farr));
                    try {
                        os.write(farr);
                    }
                    catch (IOException e) {
                        log.error(" exception: {} ", e);
                    }
                    final PageRequest preqin = prh.createPageRequest(++ip, size, sort);
                    entitypage = binaryPacketService.findAllFrames(wherepred, preqin);
                }
                while (ip < entitypage.getTotalPages());
                log.info("dump file in response... {}", ip);
                os.close();
                return dumpBinaryPackets(fich.toFile());
            }
        }
        catch (IOException e) {
            // Exception. Send a 500.
            final String msg = e.getMessage();
            log.error("Api method listPacketApids got exception : {}", msg);
            throw new BandxServiceException("IO error in method listPacketApids",e);
        }
    }

    /**
     * Factorise code to build the PacketApidSetDto.
     *
     * @param dtolist the List<PacketApidDto>
     * @param filters the Map
     * @return PacketApidSetDto
     */
    protected DataSourceSetDto buildEntityResponse(List<DataSourcePacketDto> dtolist,
                                                   Map<String, Object> filters) {
        final DataSourceSetDto respdto = new DataSourceSetDto();
        // Create the Set for the response.
        ((DataSourceSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

    /**
     *
     * @param packetlist
     * @return Response
     */
    protected Response dumpBinaryPackets (File outfile)
            throws FileNotFoundException {
        log.info("Reading binary file {}", outfile.getName());
        final InputStream in = new FileInputStream(outfile);
        // Stream data in output.
        final StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                try {
                    int read = 0;
                    final byte[] bytes = new byte[2048];
                    // Read input bytes and write in output stream
                    while ((read = in.read(bytes)) != -1) {
                        os.write(bytes, 0, read);
                        log.trace("Copying {} bytes into the output...", read);
                    }
                    // Flush data
                    os.flush();
                }
                catch (final Exception e) {
                    throw new WebApplicationException(e);
                }
                finally {
                    // Close all streames to avoid memory leaks.
                    log.debug("closing streams...");
                    if (os != null) {
                        os.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    Boolean cleanup = outfile.delete();
                    if (cleanup) {
                        log.info("Binary file requested is deleted");
                    }
                    else {
                        log.warn("Binary file {} cannot be deleted", outfile.getAbsolutePath());
                    }
                }
            }
        };
        log.debug("Send back the stream....");
        // Get media type
        final String rettype = MediaType.APPLICATION_OCTET_STREAM_TYPE.toString();
        // Get extension
        final String ext = "dat";
        final String fname = outfile.getName() + "." + ext;
        // Set the content type in the response, and the file name as well.
        return Response.ok(stream) /// MediaType.APPLICATION_JSON_TYPE)
                .header("Content-type", rettype)
                .header("Content-Disposition", "Inline; filename=\"" + fname + "\"")
                // .header("Content-Length", new
                // Long(f.length()).toString())
                .build();

    }
}
