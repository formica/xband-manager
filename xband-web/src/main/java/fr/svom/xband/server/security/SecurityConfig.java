package fr.svom.xband.server.security;

import fr.svom.xband.data.config.XbandProperties;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

/**
 * Web security configuration.
 *
 * @author formica
 * @version %I%, %G%
 */
@Profile({"keycloak"})
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SecurityConfig.class);

    /**
     * Properties.
     */
    @Autowired
    private XbandProperties cprops;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        // Configure internal rules using external properties defined.
        if ("active".equals(cprops.getSecurity())) {
            http.authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/**").permitAll()
                    .antMatchers(HttpMethod.POST, "/**").hasAnyRole("guest")
                    .antMatchers(HttpMethod.DELETE, "/**").hasAnyRole("guest")
                    .anyRequest()
                    .permitAll();
            // Disable csrf.
            http.csrf().disable();
        }
        else if ("none".equals(cprops.getSecurity())) {
            // No security is enabled.
            log.info("No security enabled for this server....");
            http.authorizeRequests().antMatchers("/**").permitAll().and().httpBasic().disable().csrf()
                    .disable();
        }
    }

    /**
     * The global configuration.
     *
     * @param auth
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        // configure keycloak.
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        // Set the provider.
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Create the bean for keycloak provider.
     *
     * @return KeycloakAuthenticationProvider
     */
    @Bean
    @Override
    protected KeycloakAuthenticationProvider keycloakAuthenticationProvider() {
        // Return the custom auth provider.
        return new fr.svom.xband.server.security.CustomKeycloakAuthenticationProvider();
    }

    /**
     * Create the bean for session authentication strategy.
     *
     * @return SessionAuthenticationStrategy
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

}
