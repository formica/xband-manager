package fr.svom.xband.server.processing;

import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.LcDatFile;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author etrigui
 */
@Component
public class PacketVerification {

    /**
     * Gflag.
     */
    private static final int XB_GFLAG = 3;
    /**
     * Packet length.
     */
    private static final int XB_PLENGTH = 1015;

    /**
     * apid exists if it belongs to the list of possible apid.
     * see StaticLists.BX_APID_LIST
     *
     * @param apid decimal apid value.
     * @return boolean
     * true if apid value is expected.
     */
    public boolean isapidExist(Integer apid) {
        boolean ifExist = false;
        if (StaticLists.APID_PKTID_MAP.containsKey(apid)) {
            ifExist = true;
        }
        return ifExist;
    }

    /**
     * Verify id the apid value corresponds to science packet.
     * see StaticLists.BX_APID_SCIENCELIST
     *
     * @param apid decimal apid value.
     * @return boolean
     * true if apid value is expected.
     */
    public boolean isApidScience(Integer apid) {
        boolean ifExist = false;
        if (StaticLists.APID_SCIENCE_MAP.containsKey(apid)) {
            ifExist = StaticLists.APID_SCIENCE_MAP.get(apid);
        }
        return ifExist;
    }

    /**
     * Test Primary header.
     *
     * @param frame BandxTrasfer frame.
     * @return boolean
     * true if primary header (ccsdc) is valid:
     * (test1) 5 firts bits =0
     * (test2) 11 bits of apid are known (apid exist)
     * (test3) 2 bits of Gflag à 11
     * (test4) packet data length =1015.
     */
    public boolean isPrimaryHeaderValid(BandxTransferFrame frame) {

        boolean test1 = (frame.getCcsdsVersion() == 0
                         && frame.getCcsdsType() == 0
                         && frame.getCcsdsHeadFlag() == 0);
        boolean test2 = isapidExist(frame.getApidPacket().getApidValue());
        boolean test3 = (frame.getCcsdsGFlag() == XB_GFLAG);
        boolean tes4 = (frame.getCcsdsPlength() == XB_PLENGTH);
        return (test1 && test2 && test3 && tes4);
    }

    /**
     * Apid is conform if:
     * apid (hexa) in datfile is egal to apid value (decimal) inside of the binary packet.
     *
     * @param frame   Bandx transer frame.
     * @param datfile L0c datfile.
     * @return boolean
     * true if apid is conform :
     */
    public boolean isApidConform(BandxTransferFrame frame, LcDatFile datfile) {

        /*** extract apid value from binary file */
        Integer apidFromBinaryData = frame.getApidPacket().getApidValue();
        /*** extract apid value from datfile parsing */
        String apidHexa = datfile.getApidHexa();
        Integer nameApid = Integer.parseInt(apidHexa, 16);
        return (nameApid.intValue() == apidFromBinaryData.intValue());
    }

    /**
     * Frame isValid if primaryHeader is valid and apid is conform.
     *
     * @param frame   Bandx Transfer frame.
     * @param datfile LC datfile.
     * @return boolean
     * true if frmae is valid.
     */
    public boolean isFrameValid(BandxTransferFrame frame, LcDatFile datfile) {
        return (isPrimaryHeaderValid(frame) && isApidConform(frame, datfile));
    }

    /**
     * @param entity BandxTransferFrame.
     * @return boolean
     * is known.
     */
    public boolean isFrameKnown(BandxTransferFrame entity) {
        boolean isknown = false;
        boolean isValid = entity.isFrameValid();
        Integer apid = entity.getApidPacket().getApidValue();
        Integer pktId = entity.getPacketId();
        if ((!isValid) && (isapidExist(apid))) {
            isknown = true;
        }
        if ((isValid) && (isPacketIDValid(pktId, apid))) {
            isknown = true;
        }
        return isknown;
    }

    /**
     * @param pktId The packet Identifier.
     * @param apid  Apid decimal value.
     * @return boolean
     * true if packetID is known
     */
    public boolean isPacketIDValid(Integer pktId, Integer apid) {
        boolean ifExist = false;
        List<Integer> listpktID = StaticLists.APID_PKTID_MAP.get(apid);
        if (listpktID.contains(pktId)) {
            ifExist = true;
        }
        return ifExist;
    }

    /**
     * @param apid      apid value (decimal).
     * @param obsidType obsid type.
     * @param obsidNum  obsid num.
     * @return boolean
     * True if Obsid is valid.
     */
    public boolean isObsIDValid(Integer apid, Integer obsidType, Integer obsidNum) {
        boolean isValid = false;

        /* list of possible obsid type**/
        final List<Integer> listObsidType = StaticLists.OBSID_TYPE_LIST;

        /*list of apid of science packet **/
        final List<Integer> apidScienceList = StaticLists.BX_APID_SCIENCELIST;

        if (apidScienceList.contains(apid)) {

            if (listObsidType.contains(obsidType)) {
                isValid = true;
            }
        }
        else {
            /** For all xband packets except science, the obsid = FF FFFFFF**/
            final Integer xbObsidType = 0xFF;
            final Integer xbObsidTNum = 0xFFFFFF;
            if (obsidType.equals(xbObsidType) && obsidNum.equals(xbObsidTNum)) {
                isValid = true;
            }
        }
        return isValid;
    }
}
