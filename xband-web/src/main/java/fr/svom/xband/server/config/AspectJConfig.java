package fr.svom.xband.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author etrigui
 * AspectJConfig class
 * Spring AOP.
 */
@Configuration
@ComponentScan("fr.svom.xband.server")
@EnableAspectJAutoProxy
public class AspectJConfig {

}
