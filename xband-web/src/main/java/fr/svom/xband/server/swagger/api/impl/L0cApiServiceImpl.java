package fr.svom.xband.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.server.controllers.EntityDtoHelper;
import fr.svom.xband.server.controllers.PageRequestHelper;
import fr.svom.xband.server.service.LcFileService;
import fr.svom.xband.server.service.TarFileService;
import fr.svom.xband.server.swagger.api.L0cApiService;
import fr.svom.xband.server.swagger.api.NotFoundException;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.DatFileDto;
import fr.svom.xband.swagger.model.DatFileSetDto;
import fr.svom.xband.swagger.model.PassIdDto;
import fr.svom.xband.swagger.model.PassIdSetDto;
import fr.svom.xband.swagger.model.RespPage;
import fr.svom.xband.swagger.model.TarFileDto;
import fr.svom.xband.swagger.model.TarFileSetDto;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.Map;

/**
 * @author etrigui
 */
@Component
public class L0cApiServiceImpl extends L0cApiService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(L0cApiServiceImpl.class);

    /**
     * Service.
     */
    @Autowired
    TarFileService tarFileService;

    /**
     * Service.
     */
    @Autowired
    LcFileService lcFileService;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("tarFileFiltering")
    private IFilteringCriteria tarFileFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("datFileFiltering")
    private IFilteringCriteria datFileFiltering;

    /**
     * Helper.
     */
    @Autowired
    EntityDtoHelper edh;

    @Override
    public Response listTarFiles(String by, Integer page, Integer size, String sort, String format,
                                 SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /** Search tar file list */
        log.info("Search resource list by {}", by);
        // Create filters
        Map<String, Object> filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(tarFileFiltering, by);
        }
        // Retrieve frame list using filtering.
        Page<TarFile> entitypage = tarFileService.findAllTarFiles(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        List<TarFileDto> dtolist = edh.entityToDtoList(entitypage.toList(), TarFileDto.class);
        // Create the Set.
        final BaseResponseDto setdto = buildEntityResponse(dtolist, filters);
        setdto.page(respPage);
        //
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    @Override
    public Response listDatFiles(String by, Integer page, Integer size, String sort, String format,
                                 SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /** Search tar file list */
        log.info("Search resource list by {}", by);
        // Create filters
        Map<String, Object> filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(datFileFiltering, by);
        }
        // Retrieve frame list using filtering.
        Page<LcDatFile> entitypage = lcFileService.findAllDatFiles(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        List<DatFileDto> dtolist = edh.entityToDtoList(entitypage.toList(), DatFileDto.class);
        // Create the Set.
        final BaseResponseDto setdto = buildDatEntityResponse(dtolist, filters);
        setdto.page(respPage);
        //
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    @Override
    public Response listPassIds(Integer orbit, String apid, String station, String starttime,
                                String endtime,
                                Integer page,
                                Integer size,
                                SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        /** Search tar file list */
        log.info("Search resource list by orbit={}, apid={}, station={}", orbit, apid, station);
        // Retrieve passid list using filtering.
        final PageRequest preq = prh.createPageRequest(page, size, null);
        Page<PassIdDto> entitypage = lcFileService.findAllPassIds(
                orbit, apid, station, starttime, endtime, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the Set.
        PassIdSetDto setdto = new PassIdSetDto();
        setdto.page(respPage);
        setdto.resources(entitypage.toList());
        //
        return Response.status(Response.Status.OK).entity(setdto).build();
    }

    @Override
    public Response deleteDatLcFile(@NotNull String datFileName, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        throw new NotFoundException(Response.Status.NOT_IMPLEMENTED.getStatusCode(), "This end point is not available");
    }

    @Override
    public Response deleteTarLcFile(@NotNull String tarFileName, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        throw new NotFoundException(Response.Status.NOT_IMPLEMENTED.getStatusCode(), "This end point is not available");
    }

    /**
     * Factorise code to build the TarFileSetDto.
     *
     * @param dtolist the List<TarFileDto>
     * @param filters the Map
     * @return TarFileSetDto
     */
    protected TarFileSetDto buildEntityResponse(List<TarFileDto> dtolist,
                                                Map<String, Object> filters) {
        final TarFileSetDto respdto = new TarFileSetDto();
        // Create the Set for the response.
        ((TarFileSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

    /**
     * Factorise code to build the DatFileSetDto.
     *
     * @param dtolist the List<DatFileSetDto>
     * @param filters the Map
     * @return DatFileSetDto
     */
    protected DatFileSetDto buildDatEntityResponse(List<DatFileDto> dtolist,
                                                   Map<String, Object> filters) {
        final DatFileSetDto respdto = new DatFileSetDto();
        // Create the Set for the response.
        ((DatFileSetDto) respdto).resources(dtolist)
                .size((long) dtolist.size());
        respdto.filters(filters);
        return respdto;
    }

}
