package fr.svom.xband.server.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration for web services.
 *
 * @author formica
 *
 */
@Configuration
@Slf4j
class WebConfigurer implements WebMvcConfigurer {

    /**
     * folder Location for uploaded files.
     */
    @Value("${xband.upload.dir}")
    private String  serverUploadFolder = "/tmp";
    /**
     * folder Location for uploaded files.
     */
    @Value("${xband.download.dir}")
    private String  serverDownloadFolder = "/tmp";

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#
     * addResourceHandlers(org.springframework.web.servlet.config.annotation.
     * ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        final String upfpath = "file://" + serverUploadFolder;
        final String dwnfpath = "file://" + serverDownloadFolder;
        log.info("Adding external path for static web resources => {}", upfpath);
        registry.addResourceHandler("/ext/upload/**").addResourceLocations(upfpath);
        registry.addResourceHandler("/ext/packets/**").addResourceLocations(dwnfpath);
    }
}
