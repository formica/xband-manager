package fr.svom.xband.server.processing;

import fr.svom.frame.utils.ByteOperationServices;
import fr.svom.xband.data.exceptions.AbstractCdbServiceException;
import fr.svom.xband.data.exceptions.AlreadyExistsPojoException;
import fr.svom.xband.data.exceptions.BandxServiceException;
import fr.svom.xband.data.exceptions.XbandDecodingException;
import fr.svom.xband.data.helpers.HashGenerator;
import fr.svom.xband.data.helpers.LcMetadataParser;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.LcMetadata;
import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.repository.TarFileRepository;
import fr.svom.xband.data.serialization.BandxFrameDeserializer;
import fr.svom.xband.server.service.ApidPacketService;
import fr.svom.xband.server.service.BinaryPacketService;
import fr.svom.xband.server.service.LcFileService;
import fr.svom.xband.server.service.TarFileService;
import fr.svom.xband.server.service.UnitBandxPacketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.zip.GZIPInputStream;

/**
 * @author etrigui
 */
@Component
public class LcFileProcessing {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LcFileProcessing.class);
    /**
     * save Location Folder.
     */
    @Value("${xband.download.dir}")
    private String saveLocationFolder;

    /**
     * batch Size for file reading; for best memory management.
     */
    private static final int BATCH_SIZE = 1000;

    /**
     * Constant XBAND_PKT_SIZE : packet size.
     */
    private static final int XBAND_PKT_SIZE = 1022;

    /**
     * Constant: Maximum value of PSC: Packet Sequence COunter.
     */
    public static final int PSC_MAX = 16383;                                   // 14
    // bits
    // à 1
    /**
     * Store packet on disk.
     */
    public static final int DISK_STORE = 0;
    /**
     * Compute flag.
     */
    public static final int FLAG_CALCULATOR = 0;
    /**
     * Constant: Maximum value of PPS microseconds(packet per second time).
     */
    public static final int T_PPS_MAX_MICRO = 999999;

    /**
     * X-band Frame deserializer class.
     */
    @Autowired
    private BandxFrameDeserializer bandxFrameDeserializer;

    /**
     * packet verification class.
     */
    @Autowired
    private PacketVerification packetVerification;

    /**
     * Apid Service class.
     */
    @Autowired
    private ApidPacketService apidService;

    /**
     * Unit x-band packet service.
     */
    @Autowired
    private UnitBandxPacketService unitBxPacketService;

    /**
     * tar file service.
     */
    @Autowired
    private TarFileService tarFileService;

    /**
     * tar file repository.
     */
    @Autowired
    private TarFileRepository tarFileRepository;

    /**
     * Unit x-band packet service.
     */
    @Autowired
    private BinaryPacketService binaryPacketService;

    /**
     * L0c meta-data parser.
     */
    @Autowired
    private LcMetadataParser lcMetadataParser;

    /**
     * lcFileService.
     */
    @Autowired
    private LcFileService lcFileService;

    /**
     * The datetime formatter.
     */
    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    /**
     * Default Ctor.
     */
    public LcFileProcessing() {
        super();
    }

    /**
     * @param extractedArchive the extracted archive.
     * @param tarfile          The archive ; object TarFile.
     * @return List<File>
     */
    public List<File> tarfileExtract(File extractedArchive, TarFile tarfile) {
        log.info("tarfileExtract process will create a list of files to be processed");
        if (extractedArchive == null) {
            log.warn("Cannot find dat files from null archive");
            return new ArrayList<>();
        }
        log.info("   >>> read directory {} for tar {}", extractedArchive.getAbsolutePath(), tarfile.getTarName());
        File sourcedir = extractedArchive;
        List<File> tarcontent = new ArrayList<>();
        Path path = Paths.get(sourcedir.getAbsolutePath());
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
            Iterator<Path> iterator = ds.iterator();
            int nbDatfile = 0;
            while (iterator.hasNext()) {
                Path p = iterator.next();
                String s = "";
                Path fn = p.getFileName();
                if (fn != null) {
                    s = fn.toString();
                }
                log.debug("      +++ add file {}", s);
                tarcontent.add(p.toFile());
                if (s.toLowerCase(Locale.ROOT).contains("dat")) {
                    nbDatfile++;
                    log.debug("          found dat file: increment counter {}", nbDatfile);
                }
            }
            if (nbDatfile > 1) {
                // Update information on the TarFile entity.
                tarfile.setNbDatfile(nbDatfile);
                tarFileService.updateTarFile(tarfile);
            }
            return tarcontent;
        }
        catch (IOException e) {
            throw new BandxServiceException("Cannot read directory stream from uploaded tar");
        }
    }

    /**
     * @param receivedFile received .Tar file.
     * @return TarFile Object.
     */
    public TarFile createTarfile(File receivedFile) {
        log.info("create TarFile if not already in DB...else throw exception");
        String outfname = receivedFile.getName();

        TarFile tarReceived = new TarFile();
        tarReceived.setTarName(outfname);
        tarReceived.setTarPath(receivedFile.getAbsolutePath());
        Optional<TarFile> opttar = tarFileRepository.findById(tarReceived.getTarName());
        if (opttar.isPresent()) {
            log.warn("Tar file {} is already in DB, send back the entity", tarReceived.getTarName());
            return opttar.get();
        }
        tarReceived.setNbDatfile(1);
        Boolean isConform = bandxFrameDeserializer.isTarFileNameCoform(outfname);
        tarReceived.setIsTarNameConform(isConform);
        log.debug("isConform {}", isConform);
        if (isConform) {
            log.debug("Update tar file fields from filename : {}", tarReceived);
            tarReceived = bandxFrameDeserializer.tarNameDecoderRegexp(tarReceived);
        }
        log.debug("Created TarFile entity in {} from input file path {}", outfname, receivedFile.getAbsolutePath());
        return tarReceived;
    }

    /**
     * @param tarfile         received .tar file.
     * @param inputStream     received .tar data.
     * @param extractFromPath path of file to extract
     * @return file: extracted archive.
     * @throws IOException exception
     */
    public File extractArchive(File tarfile, InputStream inputStream, String extractFromPath)
            throws IOException {
        log.info("extractArchive start: stored {} into {}", tarfile.getName(), extractFromPath);
        Date now = new Date();
        String tarName = tarfile.getName();
        String extractedArchiveName = tarName.substring(0, tarName.indexOf('.')) + "-"
                                      + now.getTime() + "-uploaded";
        String extractedArchivePath = extractFromPath + '/' + extractedArchiveName;
        // Start reading the archive.
        File extractedArchive = new File(extractedArchivePath);
        boolean isTarZipped = FileFormatUtils.isGZipped(tarfile);
        log.info("is tar GZipped : {}", isTarZipped);
        // Tar is zipped.
        if (isTarZipped) {
            log.debug("decompress then untar");
            inputStream = new GZIPInputStream(inputStream);
        }
        log.debug("extract Archive to: {}", extractedArchive);
        FileFormatUtils.untar(inputStream, extractedArchive);
        log.debug("extractedArchive:  {}", extractedArchive);
        return extractedArchive;
    }

    /**
     * @param datfile L0C .dat file.
     * @param tarfile L0C tar file received from SSDC.
     * @return nb BandxTransferFrame stored
     * @throws AbstractCdbServiceException If an exception occurred.
     */
    @Transactional(Transactional.TxType.REQUIRED)
    public Integer lcDataProcessing(File datfile, TarFile tarfile) throws AbstractCdbServiceException {
        log.info("start .dat File processing for {} ", datfile.getName());
        Date date = new Date();
        long time = date.getTime();
        Timestamp rtime = new Timestamp(time);

        /** decoding and saving LC FILE in DB **/
        LcDatFile lcDatFile = createLcDatFile(datfile, tarfile, rtime);
        if (lcFileService.existsLcfile(lcDatFile)) {
            log.error("LC DAT file already in DB, assume frames are there...skip this one");
            throw new AlreadyExistsPojoException("LcDatFile " + lcDatFile.getLcfileName() + " already exists.");
        }
        /**
         * Filename verification :
         * if filename is non conform no data processing.
         **/
        if (!bandxFrameDeserializer.isDatFileNameCoform(datfile.getName())) {
            log.error("The .dat Filename is non conform");
            throw new XbandDecodingException("Dat Filename is non conform: "
                                             + "dat file is stored but no data processing ");
        }
        /** Apid verification **/
        String apidHexa = lcDatFile.getApidHexa();
        Integer nameApid = Integer.parseInt(apidHexa, 16);
        ApidPacket apidPacket = null;
        try {
            apidPacket = apidService.findApid(nameApid);
        }
        catch (AbstractCdbServiceException e) {
            // TO DO: this part should really trigger an exception in future.
            log.warn("The apid {} was not found, we create it for the moment...: {}", nameApid, e);
            apidPacket = new ApidPacket();
            apidPacket.setApidValue(nameApid);
            apidPacket = apidService.registerApid(apidPacket);
        }
        /*if (!packetVerification.isapidExist(nameApid)) {
            log.warn("no preprocessing for this datfile; apid hexa of lc filename does not exists ");
            throw new ApidException("apid hexa of lc filename dont exist");}
        }*/
        /**
         *  If the packet is science packet (type = SD)
         *  The apid hexa in the filename must belongs to the list of apid science
         *  else no processing
         **/
        if (!packetVerification.isApidScience(nameApid) && ("SD").equals(lcDatFile.getType())) {
            log.error("no preprocessing for this datfile; apid is not possible for science pkt");
            throw new XbandDecodingException("apid (hexa of lc filename) is not possible for a SCIENCE");
        }
        /**
         * Test index conformity:
         *  if the index =2 and there is no packet with same name and inex =1
         *  so no processing
         **/
        if (!lcFileService.indexConform(lcDatFile) && lcDatFile.getIndex() == 2) {
            // || lcFileService.tripletNameExist(lcDatFile)) {
            log.error("no preprocessing for this datfile (index not conform) ");
            throw new XbandDecodingException("we dont found index 1 for the same pkt name");
        }
        /** read datfile by batch to store packets **/
        LcDatFile stored = lcFileService.storeLcfile(lcDatFile);
        return createAndStoreFrames(datfile, stored, apidPacket);
    }

    /**
     * @param datfile L0C .dat file.
     * @param tarFile L0C tar file received from SSDC.
     * @param rt      Timestamp.
     * @return LcDatFile
     * @throws DateTimeParseException
     */
    public LcDatFile createLcDatFile(File datfile, TarFile tarFile, Timestamp rt) throws DateTimeParseException {
        log.info("create lc file ");
        LcDatFile lcDatFile = new LcDatFile();

        lcDatFile.setFilePath(datfile.getAbsolutePath());
        lcDatFile.setLcfileName(datfile.getName());
        lcDatFile.setTarFile(tarFile);
        lcDatFile.setReceptionTime(rt);
        lcDatFile.setFileSize(datfile.length());

        Boolean isConform = bandxFrameDeserializer.isDatFileNameCoform(datfile.getName());
        lcDatFile.setIsFilenameConform(isConform);

        if (isConform) {
            /** DAT FILENAME FORMAT:
             ** "SVOM_L0C_TYPE_APID_START-TIME_END-TIME_ORBIT_STATION_VER_INDEX.dat" **/
            LcDatFile decodedFile = bandxFrameDeserializer.fileNameDecoderRegexp(datfile.getName());
            lcDatFile.setType(decodedFile.getType());
            lcDatFile.setApidHexa(decodedFile.getApidHexa());
            lcDatFile.setStartTime(decodedFile.getStartTime());
            lcDatFile.setEndTime(decodedFile.getEndTime());
            lcDatFile.setOrbit(decodedFile.getOrbit());
            lcDatFile.setStation(decodedFile.getStation());
            lcDatFile.setVer(decodedFile.getVer());
            lcDatFile.setIndex(decodedFile.getIndex());

            if (decodedFile.getStartTime() != null) {
                log.debug("Parse start time string {}", decodedFile.getStartTime());
                LocalDateTime ldt = LocalDateTime.parse(decodedFile.getStartTime(), dateTimeFormatter);
                Instant sinst = ldt.toInstant(ZoneOffset.UTC);
                lcDatFile.setStartTimeTs(Timestamp.from(sinst));
            }
            if (decodedFile.getEndTime() != null) {
                log.debug("Parse end time string {}", decodedFile.getEndTime());
                LocalDateTime edt = LocalDateTime.parse(decodedFile.getEndTime(), dateTimeFormatter);
                Instant einst = edt.toInstant(ZoneOffset.UTC);
                lcDatFile.setEndTimeTs(Timestamp.from(einst));
            }
        }
        log.debug("lcDatFile created : {}", lcDatFile);
        return lcDatFile;
    }

    /**
     * @param xmlfile XML file containing l0c meta-data.
     * @return LcMetadata
     */
    public LcMetadata xmlDataProcessing(File xmlfile) {
        LcMetadata lcMetadata = null;

        if (bandxFrameDeserializer.isXMLFileNameCoform(xmlfile.getName())) {
            log.info("The xml Filename is conform");
            lcMetadata = lcMetadataParser.fillLcMetadata(xmlfile);
        }
        else {
            log.info("The xml Filename is non conform");
        }

        return lcMetadata;
    }

    /*
     *//**
     * @param datfile
     *            L0C file.
     * @param lcDatFile
     *            L0C .dat file.
     * @return nb of frames stored in the DB.
     *
     *//*
    public Integer createAndStoreFrames1(File datfile, LcDatFile lcDatFile) {

        Integer storedframes = 0;
      *//**
     * read file by batch
     *//*
        try (InputStream inputstream = new FileInputStream(datfile)) {

            Integer pktNb = (int) (lcDatFile.getFileSize() / XBAND_PKT_SIZE);
            Integer restPkt = pktNb % BATCH_SIZE;
            Integer batchNb = pktNb / BATCH_SIZE;
            if (restPkt > 0) {
                batchNb++;
            }
            int bytesRead = 0;
            BandxTransferFrame lastFrame = null;
            for (int index = 0; index < batchNb; index++) {
                if (bytesRead == -1) {
                    log.info("stop lecture ");
                }
                byte[] data;
                if (restPkt != 0 && batchNb - 1 == index) {
                    data = new byte[XBAND_PKT_SIZE * restPkt];
                }
                else {
                    data = new byte[XBAND_PKT_SIZE * BATCH_SIZE];
                }
                bytesRead = inputstream.read(data);
                log.info("----- Batch i for Data processing ----");
                BandxTransferFrame[] framesData = processData(data, lcDatFile, lastFrame, index);

                for (int i = 0; i < framesData.length; i++) {
                    BandxTransferFrame frame = framesData[i];
                    unitBxPacketService.storeBinarybandxPkt(frame);
                    storedframes++;
                    log.info(logmsg.getStoreMessage(frame));
                }
                lastFrame = framesData[framesData.length - 1];

            }
        }
        catch (IOException e1) {
            log.error(" file not found for input stream:{}", e1);
        }

        return storedframes;
    }
       */

    /**
     * @param datfile    L0C file.
     * @param lcDatFile  L0C .dat file.
     * @param apidPacket The Apid.
     * @return nb of frames stored in the DB.
     */
    public Integer createAndStoreFrames(File datfile, LcDatFile lcDatFile, ApidPacket apidPacket) {

        Integer storedframes = 0;
        /**
         * read file by batch
         */
        try (InputStream inputstream = new FileInputStream(datfile)) {

            Integer pktNb = (int) (lcDatFile.getFileSize() / XBAND_PKT_SIZE);
            int bytesRead = 0;
            int index = 0;
            BandxTransferFrame lastFrame = null;
            int counter = 0;
            while (bytesRead != -1 && pktNb > 0) {
                counter++;
                int dataSize = pktNb < BATCH_SIZE ? pktNb : BATCH_SIZE;
                byte[] data = new byte[XBAND_PKT_SIZE * dataSize];

                bytesRead = inputstream.read(data);
                log.info("----- Batch i={} nbytes={} for Data processing ----", counter, bytesRead);
                BandxTransferFrame[] framesData = processData(data, lcDatFile, apidPacket, lastFrame, index);
                log.info("----- Processed n={} frames ----", framesData.length);
                storedframes += unitBxPacketService.storeFrameArray(framesData);
                log.info("Stored n={} frames", storedframes);
                lastFrame = framesData[framesData.length - 1];
                pktNb = pktNb - BATCH_SIZE;
                index++;
            }
            /** processs last packets */
        }
        catch (IOException e1) {
            log.error(" file not found for input stream: {}", e1);
        }
        return storedframes;
    }

    /**
     * @param data       data.
     * @param lcDatFile  L0C .dat file.
     * @param apidPacket the apid.
     * @param lastFrame  previousFrame of these Data.
     * @param batchIndex num of the batch.
     * @return BandxTransferFrame list.
     */
    public BandxTransferFrame[] processData(byte[] data, LcDatFile lcDatFile, ApidPacket apidPacket,
                                            BandxTransferFrame lastFrame, Integer batchIndex) {

        BandxTransferFrame[] framesData;

        int pktNb = data.length / XBAND_PKT_SIZE;
        List<byte[]> pktBinaryList = cutBandxPackage(pktNb, data);
        framesData = new BandxTransferFrame[pktBinaryList.size()];

        /** create folder to stock .bin binary packet files **/
        File binRep = null;
        if (DISK_STORE > 0) {
            File newRep = new File(saveLocationFolder + '/' + "BIN");
            if (!newRep.exists() || !newRep.isDirectory()) {
                FileFormatUtils.createDir(newRep);
            }
            String binRepfilename = lcDatFile.getApidHexa() + "_" + lcDatFile.getStartTime();
            binRep = new File(newRep.getAbsolutePath() + '/' + binRepfilename);
            log.debug("binRep:  {}", binRep.getAbsolutePath());
            if (!binRep.exists() || !binRep.isDirectory()) {
                FileFormatUtils.createDir(binRep);
            }
        }
        BandxTransferFrame previousFrame = lastFrame;
        for (int i = 0; i < pktBinaryList.size(); i++) {

            /** store individual packet in bin files **/
            String datFilename = lcDatFile.getLcfileName();
            String fpathname = "none";
            /** stock decoded data in the database **/
            // log.debug("Decode: 0x{}", DatatypeConverter.printHexBinary(pktBinaryList.get(i)));
            BandxTransferFrame entity = createFrameFromBinary(datFilename,
                    fpathname, lcDatFile.getLcfileId(), pktBinaryList.get(i));
            if (entity != null) {
                if (i == 0) {
                    Integer apid = entity.getApidPacket().getApidValue();
                    ApidPacket savedapidPacket = null;
                    try {
                        savedapidPacket = apidService.findApid(apid);
                    }
                    catch (AbstractCdbServiceException e) {
                        log.warn("The apid {} was not found, we create it for the moment...: {}", apid, e);
                        savedapidPacket = entity.getApidPacket();
                        savedapidPacket = apidService.registerApid(savedapidPacket);
                    }
                    if (savedapidPacket != null && savedapidPacket.getInstrument() == null) {
                        Integer pid = entity.getApidPacket().getPid();
                        Integer pcat = entity.getApidPacket().getPcat();
                        savedapidPacket.setCategory(StaticLists.PID_MAP.get(pid));
                        savedapidPacket.setInstrument(StaticLists.PCAT_MAP.get(pcat));
                        savedapidPacket = apidService.updateApid(savedapidPacket);
                    }
                    apidPacket = savedapidPacket;
                }
                entity.setApidPacket(apidPacket);
                entity.setLcDatFile(lcDatFile);
                Integer pktNum = i + BATCH_SIZE * batchIndex;
                entity.setPktOrder(pktNum);
                entity.setFrameValid(packetVerification.isFrameValid(entity, lcDatFile));
                entity.setKnown(packetVerification.isFrameKnown(entity));
                entity.setFlag(0);
                /** calcul doublons */
                if (FLAG_CALCULATOR > 0) {
                    List<BandxTransferFrame> doublons = unitBxPacketService
                            .sameIdentifierPktList(entity);
                    Integer dVers = doublons.size();
                    /** calcul FLAG */
                    Integer flag = computeFlag(entity, previousFrame, dVers);
                    entity.setFlag(flag);
                }
                framesData[i] = entity;
            }
            else {
                log.error("Cannot create frame: entity is null");
            }
            previousFrame = entity;
        }
        return framesData;
    }

    /**
     * @param pktNb      number of packets
     * @param binaryData bytes list of data
     * @return List<byte [ ]> list of binary packets
     */
    private List<byte[]> cutBandxPackage(Integer pktNb, byte[] binaryData) {
        List<byte[]> listbinarypkt = new ArrayList<>();
        for (int i = 0; i < pktNb; i++) {
            byte[] b = ByteOperationServices.cutBinaryArray(binaryData, i * XBAND_PKT_SIZE,
                    XBAND_PKT_SIZE);
            listbinarypkt.add(b);
            log.debug("new bandx pkt added ");
        }
        return listbinarypkt;
    }

    /**
     * @param datFilename L0C .dat file name.
     * @param binpath     bin file upload location.
     * @param lcFileid    the id of the .dat file.
     * @param binaryData  binary data.
     * @return BandxTransferFrame.
     */
    private BandxTransferFrame createFrameFromBinary(String datFilename, String binpath,
                                                     Long lcFileid,
                                                     byte[] binaryData) {

        log.debug("create Frame From Binary");
        BandxTransferFrame entity = new BandxTransferFrame();
        try {
            log.debug("Deserialize binary packet data of length {} ", binaryData.length);
            entity = bandxFrameDeserializer.deserialize(binaryData);
            log.debug("Deserialized ... created frame for apid {}", entity.getApidPacket());
            String hash = HashGenerator.shaJava(binaryData);
            DataSourcePacket dp = binaryPacketService.findbinaryByhash(hash);
            if (dp != null) {
                log.warn("Found duplicate for hash {}", hash);
                entity.setFrameDuplicate(Boolean.TRUE);
            }
            else {
                dp = new DataSourcePacket();
                dp.setHashId(hash);
                dp.setLcfileId(lcFileid);
                dp.setPacket(binaryData);
                entity.setFrameDuplicate(Boolean.FALSE);
            }
            entity.setDataSourcePacket(dp);
            entity.setBinaryHash(hash);
        }
        catch (AbstractCdbServiceException e) {
            log.error("createFrameFromBinary exception, returned frame will be empty :{}", e);
        }
        return entity;
    }

    /**
     * FLAG contains a byte indicator for packet quality (0 to 3 if all OK): # 1st
     * bit is 1 if packet has previous packet in DB (3 next bits are meaningful), #
     * 2nd bit is 1 for PSC gap, 3rd bit is 1 for PSC roll over in same second, #
     * 4th bit is 1 for PPS decreasing, 5th bit is always Ok=0 (known packetID), #
     * 6th bit is 0 for valid PPS microsecond, 7th bit is 0 for valid ObsID # 7th
     * bit : 0 if the packet is not a bad duplicate).
     *
     * @param frame         BandxTransferFrame object.
     * @param previousframe precedent BandxTransferFrame object.
     * @param dVers         number of doublons (packet with same identifier)
     * @return Integer flag.
     */
    public Integer computeFlag(BandxTransferFrame frame, BandxTransferFrame previousframe,
                               Integer dVers) {
        log.debug("compute flag of :{}", frame.getFrameId());
        String flagString = "";

        Integer hasPrevious = 1;

        Integer order = frame.getPktOrder();
        log.debug("frame order :{}", frame.getPktOrder());
        if (order == 0) { // && previousframe == null)
            hasPrevious = 0;
        }
        flagString = String.valueOf(hasPrevious) + flagString;

        Integer hasGap = 0;
        Integer rollOver = 0;

        if (order != 0) {
            Integer previousPsc = previousframe.getCcsdsCounter();
            Integer psc = frame.getCcsdsCounter();
            if (previousPsc != psc - 1) {
                if (psc == 0 && previousPsc.equals(PSC_MAX)) {
                    rollOver = 1;
                }
                else {
                    hasGap = 1;
                }
            }
        }
        flagString = String.valueOf(hasGap) + flagString;
        flagString = String.valueOf(rollOver) + flagString;

        Integer ppsDecreasing = 0;
        if (order != 0) {
            Long pps = frame.getPacketTimeSec();
            Long previousPps = previousframe.getPacketTimeSec();
            if (pps < previousPps) {
                ppsDecreasing = 1;
            }
        }
        flagString = String.valueOf(ppsDecreasing) + flagString;

        Integer pktId = frame.getPacketId();
        Integer apid = frame.getApidPacket().getApidValue();
        Boolean isUnvalidPktId = !packetVerification.isPacketIDValid(pktId, apid);
        Integer unknownPktId = isUnvalidPktId.compareTo(false);
        flagString = String.valueOf(unknownPktId) + flagString;

        Integer unvalidPpsMicro = 0;
        Integer ppsMicro = frame.getTimeMicroSec();
        if (ppsMicro > T_PPS_MAX_MICRO) {
            unvalidPpsMicro = 1;
        }
        flagString = String.valueOf(unvalidPpsMicro) + flagString;

        Integer obsidType = frame.getObsidType(); // 1 bits
        Integer obsidNum = frame.getObsidNum(); // 3 bits
        Boolean isvalidObsid = packetVerification.isObsIDValid(apid, obsidType, obsidNum);
        log.debug("isvalidObsid {}:", isvalidObsid);
        Integer obsidFlag = 0;
        if (isvalidObsid.equals(false)) {
            obsidFlag = 1;
        }
        flagString = String.valueOf(obsidFlag) + flagString;

        Integer duplicateFlag = 0;
        if (!frame.isFrameDuplicate() && dVers > 0) {
            duplicateFlag = 1;
        }
        flagString = String.valueOf(duplicateFlag) + flagString;

        log.debug("flagString {}:", flagString);
        Integer flag = Integer.parseInt(flagString, 2);
        log.debug("flag {}:", flag);

        return flag;
    }

    /**
     * This notification is ensured by Spring AOP. For more detail see
     * fr.svom.xband.server.aspects.BandxDecodingAspect.
     *
     * @param fileName name of received archive (L0C).
     * @return tar file Name.
     */
    public String sendNotif(String fileName) {
        log.info("Nats notification trigger ");
        return fileName;
    }
}
