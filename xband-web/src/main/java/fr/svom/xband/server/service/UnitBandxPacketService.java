package fr.svom.xband.server.service;

import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.LcMetadata;
import fr.svom.xband.data.repository.ApidRepository;
import fr.svom.xband.data.repository.BandxTransferFrameRepository;
import fr.svom.xband.data.repository.DataSourceRepository;
import fr.svom.xband.data.repository.LcDatFileRepository;
import fr.svom.xband.data.repository.LcMetadataRepository;
import fr.svom.xband.data.repository.PacketIdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author etrigui
 */
@Service
@Transactional
public class UnitBandxPacketService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(UnitBandxPacketService.class);

    /**
     * log message.
     */
    @Autowired
    private BandxLogMessageService logmsg;

    /**
     * l0c file Repsitory.
     */
    @Autowired
    private LcDatFileRepository lcDataRepository;

    /**
     * apid Packet Repository.
     */
    @Autowired
    private ApidRepository apidRepository;

    /**
     * xband Packet Repository.
     */
    @Autowired
    private BandxTransferFrameRepository bandxPacketRepository;

    /**
     * data Source packet Repository.
     */
    @Autowired
    private DataSourceRepository dataSourceRepository;

    /**
     * PacketId Repository.
     */
    @Autowired
    private PacketIdRepository packetIdRepository;
    /**
     * l0c Metadata Repository.
     */
    @Autowired
    private LcMetadataRepository lcMetadataRepository;

    /**
     * @param entityList
     * @return int
     */
    @Transactional
    public int storeFrameArray(BandxTransferFrame[] entityList) {
        int storedframes = 0;
        for (BandxTransferFrame frame : entityList) {
            storeBinarybandxPkt(frame);
            storedframes++;
        }
        return storedframes;
    }

    /**
     * BandxTransferFrame persistence in the database.
     *
     * @param entity BandxTransferFrame.
     */
    @Transactional(Transactional.TxType.REQUIRED)
    public void storeBinarybandxPkt(BandxTransferFrame entity) {
        bandxPacketRepository.save(entity);
        log.debug("frame stored: APID={} PKTORD={} ", entity.getApidPacket().getApidValue(), entity.getPktOrder());
    }

    /*    *//**
     * update post processing attributes as flag.
     * @param entity
     *        x-band packet entity.
     *//*
    public void updateBinarybandxPkt(BandxTransferFrame entity) {
        bandxPacketRepository.save(entity);
        log.info("xband frame updated: {} ", entity);
    }*/

    /**
     * @param lcMetadata L0C xml metadata file.
     * @return LcMetadata
     */
    @Transactional
    public LcMetadata storeLcMetadata(LcMetadata lcMetadata) {
        log.info("Store lcMetadata {} - {}", lcMetadata.getProductId(), lcMetadata.getMetadataId());
        LcMetadata existsmd = lcMetadataRepository.findByTarfileName(lcMetadata.getTarFile().getTarName());
        if (existsmd != null) {
            return existsmd;
        }
        log.debug("Storing {}", lcMetadata);
        return lcMetadataRepository.save(lcMetadata);
    }

    /**
     * @param frame BandxTransferFrame.
     * @return list of packets doublons of the parameter frame
     * doublons = same identifier (Apid + psc + pps).
     */
    public List<BandxTransferFrame> sameIdentifierPktList(BandxTransferFrame frame) {
        return bandxPacketRepository.findPktswithSameIdentifier(
                frame.getApidPacket().getApidValue(), frame.getCcsdsCounter(),
                frame.getPacketTimeSec());
    }
}

