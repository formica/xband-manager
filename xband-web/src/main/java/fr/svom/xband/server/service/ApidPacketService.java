package fr.svom.xband.server.service;

import com.querydsl.core.types.Predicate;
import fr.svom.xband.data.exceptions.AlreadyExistsPojoException;
import fr.svom.xband.data.exceptions.NotExistsPojoException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.PacketId;
import fr.svom.xband.data.repository.ApidRepository;
import fr.svom.xband.data.repository.PacketIdRepository;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author etrigui.
 */
@Service
public class ApidPacketService {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ApidPacketService.class);

    /**
     * apid repository object.
     */
    @Autowired
    private ApidRepository apidRepository;

    /**
     * apid repository object.
     */
    @Autowired
    private PacketIdRepository packetIdRepository;

    /**
     * mapper facade qualifier.
     */
    @Autowired
    private MapperFacade mapper;

    /**
     * Default Ctor.
     */
    public ApidPacketService() {
        log.debug("ApidPacketService constructor");
    }

    /**
     * @param entity packet Apid.
     * @return ApidPacket or null.
     */
    @Transactional
    public ApidPacket registerApid(ApidPacket entity) {
        if (entity == null) {
            log.warn("Cannot register null entity....send back null");
            return null;
        }
        Integer apId = entity.getApidValue();
        /** find apid by Id */
        Optional<ApidPacket> saved = apidRepository.findById(apId);
        if (saved.isPresent()) {
            ApidPacket apidPacket = saved.get();
            log.warn("ApidPacket already registered: {}", apidPacket);
            return apidPacket;
        }
        else {
            /** mapping */
            if (entity.getName() == null) {
                entity.setName(String.format("%04X", entity.getApidValue()));
            }
            if (entity.getDescription() == null) {
                entity.setDescription("none");
            }
            return apidRepository.save(entity);
        }
    }

    /**
     * @param entity packet Apid.
     * @return ApidPacket
     */
    @Transactional
    public ApidPacket updateApid(ApidPacket entity) {
        /** find apid by Id */
        ApidPacket apidPacket = apidRepository.findById(entity.getApidValue()).orElseThrow(
                () -> new NotExistsPojoException("Cannot find apid entity for " + entity.getApidValue())
        );
        apidPacket.setCategory(entity.getCategory());
        apidPacket.setInstrument(entity.getInstrument());
        apidPacket.setPcat(entity.getPcat());
        apidPacket.setPid(entity.getPid());
        apidPacket.setTid(entity.getTid());
        apidRepository.save(apidPacket);
        log.warn("ApidPacket updated: {}", apidPacket);
        return apidPacket;
    }

    /**
     * @param entity packet Apid Dto.
     * @return PacketId
     */
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public PacketId registerPacketId(PacketId entity) {
        /** find apid packet by Id */
        Optional<PacketId> saved = packetIdRepository.findById(entity.getPktId());
        if (saved.isPresent()) {
            log.warn("PacketId {} already registered", entity);
            throw new AlreadyExistsPojoException("PacketId " + entity.getPktId() + " already exists");
        }
        else {
            /** mapping */
            return packetIdRepository.save(entity);
        }
    }

    /**
     * Get the packet ids for an APID.
     *
     * @param apid
     * @return List<PacketId>
     */
    public List<PacketId> findPacketIdByApid(ApidPacket apid) {
        return packetIdRepository.findPacketIdByApid(apid.getApidValue());
    }

    /**
     * @param apid apid decimal value.
     * @return PacketApidDto
     */
    public ApidPacket findApid(int apid) {
        log.debug("Search for Packet Apid {}", apid);
        /** search for apid packet */
        return apidRepository.findById(apid).orElseThrow(
                () -> new NotExistsPojoException("Cannot find ApidPacket for " + apid)
        );
    }

    /**
     * Find all apids.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page<ApidPacket>
     */
    public Page<ApidPacket> findAllApids(Predicate qry, Pageable req) {
        Page<ApidPacket> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = apidRepository.findAll(req);
        }
        else {
            entitylist = apidRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Get a list of apids in the frames selected by pass_id.
     *
     * @param station
     * @param orbit
     * @return List of ApidPacket
     */
    public List<ApidPacket> findByPassId(String station, Integer orbit) {
        return apidRepository.apidsListByPassId(station, orbit);
    }
}
