package fr.svom.xband.server.security;

import org.keycloak.adapters.springsecurity.account.KeycloakRole;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Authentication provider to add realm roles.
 */
public class CustomKeycloakAuthenticationProvider extends KeycloakAuthenticationProvider {
    /**
     * The granted authorities.
     */
    private GrantedAuthoritiesMapper grantedAuthoritiesMapper = null;

    /**
     * @param grantedAuthoritiesMapper
     */
    public void setGrantedAuthoritiesMapper(GrantedAuthoritiesMapper grantedAuthoritiesMapper) {
        this.grantedAuthoritiesMapper = grantedAuthoritiesMapper;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) authentication;
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        // Loop over roles.
        for (String role : token.getAccount().getRoles()) {
            grantedAuthorities.add(new KeycloakRole(role));
        }
        // Get the access token.
        AccessToken.Access fsctoken = token.getAccount().getKeycloakSecurityContext().getToken().getResourceAccess(
                "FSC_VHFMGR");
        if (fsctoken != null) {
            // Check the role.
            for (String role : token.getAccount().getKeycloakSecurityContext().getToken().getResourceAccess(
                    "FSC_VHFMGR").getRoles()) {
                // Add the role to authority list.
                grantedAuthorities.add(new KeycloakRole(role));
            }
        }
        // Get the realm.
        AccessToken.Access realmtoken = token.getAccount().getKeycloakSecurityContext().getToken().getRealmAccess();
        if (realmtoken != null) {
            for (String role : token.getAccount().getKeycloakSecurityContext().getToken().getRealmAccess().getRoles()) {
                // Add the realm role to authority list.
                grantedAuthorities.add(new KeycloakRole(role));
            }
        }
        // Return a token.
        return new KeycloakAuthenticationToken(token.getAccount(), token.isInteractive(),
                mapCustomAuthorities(grantedAuthorities));
    }

    /**
     * @param authorities
     * @return Collection
     */
    private Collection<? extends GrantedAuthority> mapCustomAuthorities(
            Collection<? extends GrantedAuthority> authorities) {
        // return mapped authorities.
        return grantedAuthoritiesMapper != null
                ? grantedAuthoritiesMapper.mapAuthorities(authorities)
                : authorities;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return KeycloakAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
