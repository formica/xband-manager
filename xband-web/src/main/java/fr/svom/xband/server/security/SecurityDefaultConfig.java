package fr.svom.xband.server.security;

import org.keycloak.representations.AccessToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.context.WebApplicationContext;

/**
 * The defaul config for non keycloak profiles.
 *
 * @version %I%, %G%
 * @author formica
 */
@Profile({"!keycloak"})
@Configuration
@EnableWebSecurity
public class SecurityDefaultConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Authorize everything.
        http.authorizeRequests().antMatchers("/**").permitAll();
        // Disable options.
        http.headers().frameOptions().disable();
        // disable csrf.
        http.csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // Ignore every pattern.
        web.ignoring().antMatchers("/**");
    }

    /**
     * Create an access token bean.
     *
     * @return AccessToken
     */
    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AccessToken accessToken() {
        AccessToken accessToken = new AccessToken();
        accessToken.setSubject("abc");
        accessToken.setName("Tester");
        return accessToken;
    }
}
