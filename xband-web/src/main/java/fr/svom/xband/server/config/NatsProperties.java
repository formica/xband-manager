package fr.svom.xband.server.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author etrigui
 * <p>
 * This class recovers nats properties from nats configuration in the application.yml where
 * application.yml get values from applictaion.properties file.
 */
@Component
@ConfigurationProperties("nats")
@Data
public class NatsProperties {

    /**
     * The host.
     */
    private String server = "localhost";
    /**
     * The port.
     */
    private String port = "4222";
    /**
     * The NATs queue for orchestrator, to post alerts and updates on received data.
     */
    private String xbandQueue = NatsQueue.XBAND_DATA.getQueue();
    /**
     * The cluster name for NATs streaming.
     */
    private String cluster = "test-cluster";
    /**
     * The client ID for NATs streaming.
     */
    private String clientId = "xbandmgr";
    /**
     * The user.
     */
    private String user = "none";
    /**
     * The password.
     */
    private String password = "";
    /**
     * Is NATS notification enabled.
     */
    private Boolean enabled = Boolean.TRUE;
    /**
     * option of Nats Listening.
     * By default false : listning option disabled
     * True if you want to active listining function on canal listningChannel
     */
    private Boolean listening = Boolean.FALSE;
    /**
     * Listening CHANNEL name.
     */
    private String listeningChannel = "none";
    /**
     * The service client ID.
     */
    private String serviceClientId = "xbandmgr";

}
