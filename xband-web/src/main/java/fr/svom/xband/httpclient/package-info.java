/**
 * This package contains tools for interaction with external services via http.
 * <p>
 *     The class http client can be used to retrieve information from a dedicates service.
 *     We inserted it initially to possibly retrieve satellite X data from Chinese center via REST.
 *     The method which seems will be used is instead to retrieve via FTP.
 *     Probably this package will not be needed in future.
 * </p>
 * @author etrigui
 *
 */
package fr.svom.xband.httpclient;
