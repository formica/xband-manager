package fr.svom.xband.httpclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author etrigui
 */
@Component
@Slf4j
public class HttpClient {

    /**
     * buffer size.
     */
    private static final int BUFFER_SIZE = 4096;
    /**
     * number of characters in header file .
     */
    private static final int HEADERFILE_NBCHAR = 10;

    /**
     * File for downloading file from HTTP.
     *
     * @param fileURL file URL.
     * @param saveDir directory path to save files.
     * @throws IOException exception.
     */
    public void httpFileDownload(String fileURL, String saveDir) throws IOException {
        // Init URL.
        final URL url = new URL(fileURL);
        // Init connection.
        final HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        final int responseCode = httpConn.getResponseCode();

        /**always check HTTP response code first***/
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            final String disposition = httpConn.getHeaderField("Content-Disposition");
            // Extract file name using header.
            if (disposition != null) {
                /**extracts file name from header field**/
                final int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + HEADERFILE_NBCHAR,
                            disposition.length() - 1);
                }
            }
            else {
                /** extracts file name from URL**/
                fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1, fileURL.length());
            }

            /** opens input stream from the HTTP connection **/
            final InputStream inputStream = httpConn.getInputStream();
            final String saveFilePath = saveDir + File.separator + fileName;

            /** opens an output stream to save into file **/
            try (FileOutputStream outputStream = new FileOutputStream(saveFilePath)) {
                int bytesRead = -1;
                final byte[] buffer = new byte[BUFFER_SIZE];
                // Read bytes.
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }
            log.debug("File downloaded");
        }
        else {
            log.error("No file to download. Server replied HTTP code: {}", responseCode);
        }
        httpConn.disconnect();
    }
}
