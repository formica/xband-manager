
/**
 * This is the base package.
 * It contains the main entry point for the spring-boot application.
 *
 * <p>
 * The package contains:
 * <ul>
 * <li>Server package code.
 * <li>Httpclient tools.
 * <li>Main application.
 * </ul>
 *
 * The server package code hosts the essential of the manager business code.
 * <p>
 * The package <b>fr.svom.xband.server</b> contains:
 * <ul>
 * <li>Annotations to enable special actions following execution of methods.
 * <li>Autoconfiguration for initialization of external variables (db password for example).
 * <li>Configuration classes for spring.
 * <li>Controllers and externals: classes to handle Nats communication and helper methods.
 * <li>Classes for processing methods: decoding of packets, verifications etc.
 * </ul>
 * General description of the packages.<br><br>
 * <pre>
 | package    | description                                 |
 |------------|---------------------------------------------|
 | swagger    | Contains the implementation of the REST     |
 |            | API generated classes                       |
 |------------|---------------------------------------------|
 | service    | Contains the services                       |
 |------------|---------------------------------------------|
 | config     | Classes for spring configuration            |
 |------------|---------------------------------------------|
 | security   | Classes for security configuration          |
 |------------|---------------------------------------------|
 </pre>
 *
 * @see fr.svom.xband.server.service
 * @see fr.svom.xband.server.security
 * @see fr.svom.xband.server.swagger.api.impl
 * @see fr.svom.xband.server.config
 * @see fr.svom.xband.Application
 * @see fr.svom.xband.httpclient
 * @author etrigui, andrea.formica@cern.ch
 *
 */
package fr.svom.xband;
