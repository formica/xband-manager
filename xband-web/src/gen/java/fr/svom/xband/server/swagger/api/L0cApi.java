package fr.svom.xband.server.swagger.api;

import fr.svom.xband.swagger.model.*;
import fr.svom.xband.server.swagger.api.L0cApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import fr.svom.xband.swagger.model.ApiHttpError;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.DatFileSetDto;
import fr.svom.xband.swagger.model.PassIdSetDto;
import fr.svom.xband.swagger.model.TarFileSetDto;

import java.util.Map;
import java.util.List;
import fr.svom.xband.server.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/l0c")


@io.swagger.annotations.Api(description = "the l0c API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class L0cApi  {
   @Autowired
   private L0cApiService delegate;

    @DELETE
    @Path("/dat")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Delete dat file using input filename. It will fail if there are dependent entries.", notes = "delete the specified dat file", response = BaseResponseDto.class, tags={ "l0c", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = BaseResponseDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "Request Error: header needed", response = ApiHttpError.class)
    })
    public Response deleteDatLcFile(@ApiParam(value = "datFileName: the filename to delete.", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("datFileName") @NotNull  String datFileName,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.deleteDatLcFile(datFileName, securityContext, info);
    }
    @DELETE
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Delete tar file using input filename. It will fail if there are dependent entries.", notes = "delete the specified tar file", response = BaseResponseDto.class, tags={ "l0c", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = BaseResponseDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "Request Error: header needed", response = ApiHttpError.class)
    })
    public Response deleteTarLcFile(@ApiParam(value = "tarFileName: the filename to delete.", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("tarFileName") @NotNull  String tarFileName,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.deleteTarLcFile(tarFileName, securityContext, info);
    }
    @GET
    @Path("/dat")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "search the list of received TAR files.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}. The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = DatFileSetDto.class, tags={ "l0c", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = DatFileSetDto.class)
    })
    public Response listDatFiles(@ApiParam(value = "by: the search pattern {none}.", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {lcfileName:ASC}", defaultValue = "lcfileName:ASC") @DefaultValue("lcfileName:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The output list can be json or binary for exporting full list of frames into a file" , defaultValue="json")@HeaderParam("format") String format,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listDatFiles(by, page, size, sort, format, securityContext, info);
    }
    @GET
    @Path("/passids")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "search the list of pass ids.", notes = "This method allows to perform search and sorting. Arguments: orbit={number}, apid={number}, obsid={number}, station={string}, page={ipage}, size={isize}.", response = PassIdSetDto.class, tags={ "l0c", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PassIdSetDto.class)
    })
    public Response listPassIds(@ApiParam(value = "orbit: the orbit number.") @QueryParam("orbit")  Integer orbit,@ApiParam(value = "apid: the apid number.") @QueryParam("apid")  String apid,@ApiParam(value = "station: the station number.", defaultValue = "all") @DefaultValue("all") @QueryParam("station")  String station,@ApiParam(value = "starttime: the beginning of the search.") @QueryParam("starttime")  String starttime,@ApiParam(value = "endtime: the beginning of the search.") @QueryParam("endtime")  String endtime,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listPassIds(orbit, apid, station, starttime, endtime, page, size, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "search the list of received TAR files.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}. The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = TarFileSetDto.class, tags={ "l0c", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = TarFileSetDto.class)
    })
    public Response listTarFiles(@ApiParam(value = "by: the search pattern {none}.", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {tarName:ASC}", defaultValue = "tarName:ASC") @DefaultValue("tarName:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The output list can be json or binary for exporting full list of frames into a file" , defaultValue="json")@HeaderParam("format") String format,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listTarFiles(by, page, size, sort, format, securityContext, info);
    }
}
