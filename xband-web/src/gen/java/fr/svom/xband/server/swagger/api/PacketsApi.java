package fr.svom.xband.server.swagger.api;

import io.swagger.annotations.ApiParam;

import fr.svom.xband.swagger.model.DataSourcePacketDto;
import fr.svom.xband.swagger.model.DataSourceSetDto;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;

@Path("/packets")


@io.swagger.annotations.Api(description = "the packets API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class PacketsApi  {
   @Autowired
   private PacketsApiService delegate;

    @GET
    @Path("/{hashid}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds xband binary packet by HASH_ID.", notes = "This method will search for x-band binary packet.", response = DataSourcePacketDto.class, tags={ "packets", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = DataSourcePacketDto.class)
    })
    public Response findBandxBinaryPacket(@ApiParam(value = "hashid of the binary packet", required = true) @PathParam("hashid") @NotNull  String hashid,@Context SecurityContext securityContext,@Context UriInfo info)
    throws fr.svom.xband.server.swagger.api.NotFoundException {
        return delegate.findBandxBinaryPacket(hashid, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds xband binary packets list.", notes = "This method allows to perform search and sorting.Arguments: by=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]", response = DataSourceSetDto.class, tags={ "packets", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = DataSourceSetDto.class)
    })
    public Response listBinaryPackets(@ApiParam(value = "by: the search pattern {none}", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {hashId:DESC}", defaultValue = "hashId:ASC") @DefaultValue("hashId:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "the output format: {file | dto} default to dto" , defaultValue="dto")@HeaderParam("datatype") String datatype,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listBinaryPackets(by, page, size, sort, datatype, securityContext, info);
    }
}
