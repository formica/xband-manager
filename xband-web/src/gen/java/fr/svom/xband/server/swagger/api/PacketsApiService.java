package fr.svom.xband.server.swagger.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class PacketsApiService {
    public abstract Response findBandxBinaryPacket(String hashid,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listBinaryPackets(String by,Integer page,Integer size,String sort,String datatype,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
