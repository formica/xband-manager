package fr.svom.xband.server.swagger.api;

import fr.svom.xband.server.swagger.api.*;
import fr.svom.xband.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import fr.svom.xband.swagger.model.ApiHttpError;
import fr.svom.xband.swagger.model.BaseResponseDto;
import fr.svom.xband.swagger.model.DatFileSetDto;
import fr.svom.xband.swagger.model.PassIdSetDto;
import fr.svom.xband.swagger.model.TarFileSetDto;

import java.util.List;
import fr.svom.xband.server.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class L0cApiService {
    public abstract Response deleteDatLcFile( @NotNull String datFileName,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response deleteTarLcFile( @NotNull String tarFileName,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listDatFiles(String by,Integer page,Integer size,String sort,String format,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listPassIds(Integer orbit,String apid,String station,String starttime,String endtime,Integer page,Integer size,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listTarFiles(String by,Integer page,Integer size,String sort,String format,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
