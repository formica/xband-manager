package fr.svom.xband.server.swagger.api;

import fr.svom.xband.server.swagger.api.*;
import fr.svom.xband.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import fr.svom.xband.swagger.model.ApiHttpError;
import fr.svom.xband.swagger.model.BandxTransferFrameSetDto;
import fr.svom.xband.swagger.model.BaseResponseDto;
import java.io.File;
import fr.svom.xband.swagger.model.HTTPResponse;
import fr.svom.xband.swagger.model.ObsidSetDto;
import fr.svom.xband.swagger.model.PacketApidIdSetDto;

import java.util.List;
import fr.svom.xband.server.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class FramesApiService {
    public abstract Response deleteFramesOfLcFile( @NotNull String lcFileName,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listApidPacketIdsInFrames( @NotNull String passId,String xNotification,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listBandxTransferFrames(String by,Integer page,Integer size,String sort,String format,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listObsidsInFrames(String passId,Long obsid,Integer apid,Integer packetid,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response saveBandxTransferFrames( @NotNull String dataType,String source,FormDataBodyPart fileBodypart,String lcFileName,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
