package fr.svom.xband.server.swagger.api;

import fr.svom.xband.swagger.model.*;
import fr.svom.xband.server.swagger.api.FramesApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import fr.svom.xband.swagger.model.ApiHttpError;
import fr.svom.xband.swagger.model.BandxTransferFrameSetDto;
import fr.svom.xband.swagger.model.BaseResponseDto;
import java.io.File;
import fr.svom.xband.swagger.model.HTTPResponse;
import fr.svom.xband.swagger.model.ObsidSetDto;
import fr.svom.xband.swagger.model.PacketApidIdSetDto;

import java.util.Map;
import java.util.List;
import fr.svom.xband.server.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/frames")


@io.swagger.annotations.Api(description = "the frames API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class FramesApi  {
   @Autowired
   private FramesApiService delegate;

    @DELETE
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Delete BandxfTransferFrame using input filename", notes = "delete all BandxTransferFrame associated with the specified file", response = BaseResponseDto.class, tags={ "frames", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = BaseResponseDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "Request Error: header needed", response = ApiHttpError.class)
    })
    public Response deleteFramesOfLcFile(@ApiParam(value = "lcFileName: the filename for which we want to delete the loaded frames.", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("lcFileName") @NotNull  String lcFileName,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.deleteFramesOfLcFile(lcFileName, securityContext, info);
    }
    @GET
    @Path("/notifications")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds an apid and packet id lists for a given pass_id.", notes = "This method allows to perform search on obsids in a given pass_id.", response = PacketApidIdSetDto.class, tags={ "frames", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidIdSetDto.class)
    })
    public Response listApidPacketIdsInFrames(@ApiParam(value = "pass_id: the given pass_id [orbit_station].", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("pass_id") @NotNull  String passId,@ApiParam(value = "header parameter containing the requested notification format [generate, \\ \\ none]." , defaultValue="none")@HeaderParam("X-Notification") String xNotification,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listApidPacketIdsInFrames(passId, xNotification, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a bandxTransferFrameDtos lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}. The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = BandxTransferFrameSetDto.class, tags={ "frames", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = BandxTransferFrameSetDto.class)
    })
    public Response listBandxTransferFrames(@ApiParam(value = "by: the search pattern {none}.", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {frameId:ASC}", defaultValue = "frameId:ASC") @DefaultValue("frameId:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The output list can be json or binary for exporting full list of frames into a file" , defaultValue="json")@HeaderParam("format") String format,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listBandxTransferFrames(by, page, size, sort, format, securityContext, info);
    }
    @GET
    @Path("/obsids")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds an obsid lists.", notes = "This method allows to perform search on obsids in a given pass_id.", response = ObsidSetDto.class, tags={ "frames", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = ObsidSetDto.class)
    })
    public Response listObsidsInFrames(@ApiParam(value = "pass_id: the given pass_id [stations_orbit].", defaultValue = "none") @DefaultValue("none") @QueryParam("pass_id")  String passId,@ApiParam(value = "obsid: the given obsid [integer value].") @QueryParam("obsid")  Long obsid,@ApiParam(value = "apid: the given apid [integer value].") @QueryParam("apid")  Integer apid,@ApiParam(value = "packetid: the given packetid [integer value].") @QueryParam("packetid")  Integer packetid,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listObsidsInFrames(passId, obsid, apid, packetid, securityContext, info);
    }
    @POST
    
    @Consumes({ "multipart/form-data" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves BandxfTransferFrame from xband raw data file location", notes = "create a BandxTransferFrame from packet received in a specified file location", response = HTTPResponse.class, tags={ "frames", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = HTTPResponse.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "error in operation", response = HTTPResponse.class)
    })
    public Response saveBandxTransferFrames(@ApiParam(value = "header that specify received datType (.dat or .zip)" ,required=true)@HeaderParam("dataType") String dataType,@ApiParam(value = "The source can be a remote ftp service or local in the body" , defaultValue="local")@HeaderParam("source") String source,
 @FormDataParam("file") FormDataBodyPart fileBodypart ,@ApiParam(value = "")@FormDataParam("lcFileName")  String lcFileName,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.saveBandxTransferFrames(dataType, source, fileBodypart, lcFileName, securityContext, info);
    }
}
