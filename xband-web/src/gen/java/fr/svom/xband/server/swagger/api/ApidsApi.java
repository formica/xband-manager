package fr.svom.xband.server.swagger.api;

import io.swagger.annotations.ApiParam;

import fr.svom.xband.swagger.model.ApiHttpError;
import fr.svom.xband.swagger.model.InlineResponse404;
import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.PacketApidSetDto;
import fr.svom.xband.swagger.model.PacketIdDto;
import fr.svom.xband.swagger.model.PacketIdSetDto;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/apids")


@io.swagger.annotations.Api(description = "the apids API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class ApidsApi  {
   @Autowired
   private ApidsApiService delegate;

    @POST
    
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves PacketApid", notes = "Create a new packet APID description entry in the DB.", response = PacketApidDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = PacketApidDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "internal server error", response = ApiHttpError.class)
    })
    public Response createPacketApid(@ApiParam(value = "A json string that is used to construct a packetapiddto object: { name: xxx, ... }", required = true) @NotNull @Valid  PacketApidDto packetApidDto,@Context SecurityContext securityContext,@Context UriInfo info)
    throws fr.svom.xband.server.swagger.api.NotFoundException {
        return delegate.createPacketApid(packetApidDto, securityContext, info);
    }
    @POST
    @Path("/{apid}/packetid")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves PacketId", notes = "Create a new packet Id entry in the DB associated to the Apid.", response = PacketIdDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = PacketIdDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "internal server error", response = ApiHttpError.class)
    })
    public Response createPacketId(@ApiParam(value = "packet apid", required = true) @PathParam("apid") @NotNull  Integer apid,@ApiParam(value = "A json string that is used to construct a PacketIdDto object: { name: xxx, ... }", required = true) @NotNull @Valid  PacketIdDto packetIdDto,@Context SecurityContext securityContext,@Context UriInfo info)
    throws fr.svom.xband.server.swagger.api.NotFoundException {
        return delegate.createPacketId(apid, packetIdDto, securityContext, info);
    }
    @GET
    @Path("/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds PacketApid by id", notes = "Search for a resource by ID", response = PacketApidSetDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidSetDto.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "resource not found", response = InlineResponse404.class)
    })
    public Response findPacketApids(@ApiParam(value = "packet apid", required = true) @PathParam("id") @NotNull  Integer id,@Context SecurityContext securityContext,@Context UriInfo info)
    throws fr.svom.xband.server.swagger.api.NotFoundException {
        return delegate.findPacketApids(id, securityContext, info);
    }
    @GET
    @Path("/{apid}/packetid")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds PacketId by Apid", notes = "Search for a resource by id", response = PacketIdSetDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketIdSetDto.class)
    })
    public Response findPacketIdsByApid(@ApiParam(value = "packet apid", required = true) @PathParam("apid") @NotNull  Integer apid,@Context SecurityContext securityContext,@Context UriInfo info)
    throws fr.svom.xband.server.swagger.api.NotFoundException {
        return delegate.findPacketIdsByApid(apid, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a packet APID list.", notes = "This method allows to perform search and sorting.Arguments: by=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]", response = PacketApidSetDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidSetDto.class)
    })
    public Response listPacketApids(@ApiParam(value = "by: the search pattern {none}", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {apid:DESC}", defaultValue = "apidValue:ASC") @DefaultValue("apidValue:ASC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listPacketApids(by, page, size, sort, securityContext, info);
    }
}
