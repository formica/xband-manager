package fr.svom.xband.server.swagger.api;

import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.PacketIdDto;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class ApidsApiService {
    public abstract Response createPacketApid(PacketApidDto packetApidDto,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response createPacketId(Integer apid,PacketIdDto packetIdDto,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response findPacketApids(Integer id,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response findPacketIdsByApid(Integer apid,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listPacketApids(String by,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
