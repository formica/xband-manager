package fr.svom.xband.data.model;

import fr.svom.xband.data.repository.XbandNotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("default")
@Slf4j
public class NotificationTest {

    @Autowired
    private XbandNotificationRepository xbandNotificationRepository;

    @Test
    public void testNotification() {
        XbandNotification entity = new XbandNotification();
        entity.setNotifFormat("JSON");
        entity.setNotifMessage("{ \"key\": 1 }");
        entity.setNotifName("LC_DAT_NOTIF");
        entity.setSize(entity.getNotifMessage().length());
        assertThat(entity.toString()).contains("LC_DAT");
        XbandNotification saved = xbandNotificationRepository.save(entity);
        assertThat(saved).isNotNull();
    }

}
