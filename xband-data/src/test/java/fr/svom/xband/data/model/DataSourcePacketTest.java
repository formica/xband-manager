package fr.svom.xband.data.model;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class DataSourcePacketTest {


    @Test
    public void testSetHashId() {
        DataSourcePacket dataSourcePacket = new DataSourcePacket();
        dataSourcePacket.setHashId("X00X");
        assertThat("X00X").isEqualTo(dataSourcePacket.getHashId());
    }

}
