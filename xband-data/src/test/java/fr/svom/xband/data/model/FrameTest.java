package fr.svom.xband.data.model;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class FrameTest {

    BandxTransferFrame frame = new BandxTransferFrame();

    @Test
    public void testSetPacketTimeSec() {
        frame.setPacketTimeSec(3254578L);
        assertThat(3254578L).isEqualTo(frame.getPacketTimeSec());
        frame.setTimeMicroSec(910235);
        assertThat(910235).isEqualTo(frame.getTimeMicroSec());
        ApidPacket apid= new ApidPacket();
        apid.setApidValue(1620);
        PacketId pktid = new PacketId();
        pktid.setPktId(30);
        pktid.setApid(apid);
        frame.setPacketId(pktid.getPktId());
        frame.setApidPacket(apid);
        assertThat(30).isEqualTo(frame.getPacketId());
        assertThat(1620).isEqualTo(frame.getApidPacket().getApidValue());
    }
}
