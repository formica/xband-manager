package fr.svom.xband.data.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.xband.swagger.model.BandxTransferFrameDto;
import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.PacketIdDto;
import ma.glasnost.orika.MapperFacade;


@SpringBootTest
@RunWith(SpringRunner.class)
public class PojoDtoConverterTest {

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Test
    public void testApidPacketConverter() {
        final ApidPacket entity = new ApidPacket();
        entity.setApidValue(1620);
        entity.setInstrument("ECL");
        entity.setClassName("SCIENCE");
        final  PacketApidDto dto = mapper.map(entity, PacketApidDto.class);
        assertThat(entity.getCategory()).isEqualTo(dto.getCategory());
        assertThat(dto.toString().length()).isGreaterThan(0);

        final ApidPacket entitydto = mapper.map(dto, ApidPacket.class);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(entitydto.getCategory()).isEqualTo(entity.getCategory());

    }

    @Test
    public void testPktIdConverter() {
        final PacketId entity =  new PacketId();
        ApidPacket apid = new ApidPacket();
        apid.setApidValue(1620);
        apid.setInstrument("ECL");
        apid.setClassName("SCIENCE");

        entity.setApid(apid);
        entity.setPktId(30);

        final  PacketIdDto dto = mapper.map(entity, PacketIdDto.class);
        assertThat(entity.getPktId()).isEqualTo(dto.getPacketId());
        assertThat(dto.toString().length()).isGreaterThan(0);

        final PacketId entitydto = mapper.map(dto, PacketId.class);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(entitydto.getPktId()).isEqualTo(entity.getPktId());
    }

    @Test
    public void testBandxTransferFrameConverter()  {
        BandxTransferFrame frame = new BandxTransferFrame();
        ApidPacket apidPacket2 = new ApidPacket();
        apidPacket2.setApidValue(1620);
        apidPacket2.setInstrument("ECL");
        apidPacket2.setClassName("SCIENCE");

        apidPacket2.setTid(1);
        apidPacket2.setPid(18);
        apidPacket2.setPcat(20);
        frame.setApidPacket(apidPacket2);

        frame.setCcsdsVersion(0);
        frame.setCcsdsType(0);
        frame.setCcsdsHeadFlag(0);
        frame.setCcsdsGFlag(3);
        frame.setCcsdsCounter(0);
        frame.setCcsdsPlength(1015);

        frame.setPacketTimeSec(3254578L);
        frame.setTimeMicroSec(910235);
        frame.setLocalTimeCounter(999L);
        final PacketId pktid =  new PacketId();
        pktid.setPktId(30);
        pktid.setApid(apidPacket2);
        frame.setPacketId(pktid.getPktId());

        DataSourcePacket dataSourcePacket2 = new DataSourcePacket();
        frame.setDataSourcePacket(dataSourcePacket2);

        final BandxTransferFrameDto dto = mapper.map(frame, BandxTransferFrameDto.class);
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(dto.getApidPacket().getApidValue()).isEqualTo(frame.getApidPacket().getApidValue());

    }
}
