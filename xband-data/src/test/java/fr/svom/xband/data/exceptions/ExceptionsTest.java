package fr.svom.xband.data.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class ExceptionsTest {


    @Test
    public void testExceptions() {
        XbandSQLException sql = new XbandSQLException("Error in sql request");
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        BandxServiceException svc = new BandxServiceException("Error in service");
        assertThat(svc.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(svc.getMessage()).contains("Service");

        XbandDecodingException dec = new XbandDecodingException("Error in service");
        assertThat(dec.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(dec.getMessage()).contains("Decoding");

        XbandRequestFormatException fmt = new XbandRequestFormatException("Error in req format");
        assertThat(fmt.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fmt.getMessage()).contains("request");

        PayloadEncodingException enec = new PayloadEncodingException("Error in encoding");
        assertThat(enec.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(enec.getMessage()).contains("Encoding");

        NotExistsPojoException note = new NotExistsPojoException("Cannot find pojo");
        assertThat(note.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(note.getMessage()).contains("Not Found");

        AlreadyExistsPojoException ex = new AlreadyExistsPojoException("Already exists pojo");
        assertThat(ex.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(ex.getMessage()).contains("Conflict");

        FileDownloadException fd = new FileDownloadException("Download problem");
        assertThat(fd.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fd.getMessage()).contains("download");
    }

    @Test
    public void testExceptionsWithReThrow() throws Exception {
        RuntimeException e = new RuntimeException("some runtime");
        XbandSQLException sql = new XbandSQLException("Error in sql request", e);
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        NotExistsPojoException notfound = new NotExistsPojoException("Entity not found", e);
        assertThat(notfound.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(notfound.getMessage()).contains("Not Found");

        BandxServiceException serv = new BandxServiceException("Some service error", e);
        assertThat(serv.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(serv.getMessage()).contains("Service");

        XbandDecodingException badr = new XbandDecodingException("Some bad request", e);
        assertThat(badr.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(badr.getMessage()).contains("Decoding");

        XbandRequestFormatException fmt = new XbandRequestFormatException("Error in req format", e);
        assertThat(fmt.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fmt.getMessage()).contains("request");

        AlreadyExistsPojoException conf = new AlreadyExistsPojoException("Some conflict", e);
        assertThat(conf.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(conf.getMessage()).contains("Conflict");

        PayloadEncodingException pyld = new PayloadEncodingException("Some error in payload", e);
        assertThat(pyld.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(pyld.getMessage()).contains("Encoding");

        FileDownloadException fd = new FileDownloadException("Download problem", e);
        assertThat(fd.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fd.getMessage()).contains("download");

    }

    @Test
    public void testExceptionsWithReThrowOnly() throws Exception {
        RuntimeException e = new RuntimeException("some runtime");
        XbandSQLException sql = new XbandSQLException(e);
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        NotExistsPojoException notfound = new NotExistsPojoException(e);
        assertThat(notfound.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(notfound.getMessage()).contains("Not Found");

        BandxServiceException serv = new BandxServiceException(e);
        assertThat(serv.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(serv.getMessage()).contains("Service");

        XbandDecodingException badr = new XbandDecodingException(e);
        assertThat(badr.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(badr.getMessage()).contains("Decoding");

        XbandRequestFormatException fmt = new XbandRequestFormatException(e);
        assertThat(fmt.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fmt.getMessage()).contains("request");

        AlreadyExistsPojoException conf = new AlreadyExistsPojoException(e);
        assertThat(conf.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(conf.getMessage()).contains("Conflict");

        PayloadEncodingException pyld = new PayloadEncodingException(e);
        assertThat(pyld.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(pyld.getMessage()).contains("Encoding");

        FileDownloadException fd = new FileDownloadException(e);
        assertThat(fd.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(fd.getMessage()).contains("download");
    }
}
