
package fr.svom.xband.data.helpers;

import java.io.File;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author etrigui
 *
 */

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class LcMetadataParserTest {

    private  ClassLoader classLoader = this.getClass().getClassLoader();

    /**
     * Test method for {@link LcMetadataParser#fillLcMetadata(java.io.File)}.
     */
    @Test
    public void testFillLcMetadata() {
        LcMetadataParser lcMetadataParser = new LcMetadataParser();
        String filename = "SV_L0C-METADATA.xml";
        File fileXml = new File(classLoader.getResource(filename).getFile());
        log.info("Loading file {}",fileXml.getAbsolutePath());
        lcMetadataParser.fillLcMetadata(fileXml);
    }

}
