package fr.svom.xband.data.model;

import fr.svom.frame.data.service.BandXDecoding;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * @author formica
 * TestBaseConfig class.
 */
@TestConfiguration
public class TestBaseConfig {
    /**
     * A Formatter for dates.
     * @return DateTimeFormatter
     */
    @Bean(name = "dateTimeFormatter")
    public DateTimeFormatter getDateTimeFormatter() {
        return new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                // date/time
                .appendPattern("yyyyMMdd'T'HHmmss")
                // optional fraction of seconds (from 0 to 9 digits)
                // create formatter
                .toFormatter();
    }
}
