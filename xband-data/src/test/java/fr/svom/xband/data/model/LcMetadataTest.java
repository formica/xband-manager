package fr.svom.xband.data.model;

import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class LcMetadataTest {

    @Test
    public void testTarFile() {
        LcMetadata lcMeta = new LcMetadata();
        ClassLoader classLoader = getClass().getClassLoader();
        String tarFileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        File localtarfile = new File(classLoader.getResource(tarFileName).getFile());
        TarFile tarfile = new TarFile();
        tarfile.setTarName(tarFileName);
        tarfile.setTarPath(localtarfile.getAbsolutePath());

        lcMeta.setTarFile(tarfile);
        assertEquals(tarfile, lcMeta.getTarFile());
        lcMeta.setIsXmlNameConform(true);
        assertEquals(true, lcMeta.getIsXmlNameConform());
        lcMeta.setSatellite("satellite");
        assertThat("satellite").isEqualTo(lcMeta.getSatellite());
        lcMeta.setProductionDate("pdate");
        assertThat("pdate").isEqualTo(lcMeta.getProductionDate());
        lcMeta.setProducer("productor");
        assertThat("productor").isEqualTo(lcMeta.getProducer());
        lcMeta.setConfidentialLevel("level");
        assertThat("level").isEqualTo(lcMeta.getConfidentialLevel());
        lcMeta.setConfidentialVisibility("visibility");
        assertThat("visibility").isEqualTo(lcMeta.getConfidentialVisibility());
    }

}
