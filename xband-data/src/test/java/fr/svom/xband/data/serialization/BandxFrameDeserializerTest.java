/**
 *
 */
package fr.svom.xband.data.serialization;

import fr.svom.xband.data.exceptions.BandxServiceException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.PacketId;
import fr.svom.frame.utils.ByteOperationServices;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author etrigui
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class BandxFrameDeserializerTest {

    @Autowired
    private BandxFrameDeserializer bandxDeserializer;

    private ClassLoader classLoader = this.getClass().getClassLoader();

    /**
     * logger.
     */
    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Test method for FileNameDecoder.
     */
    @Test
    public void testDeserialize() {
        File fileBx =
                new File(classLoader.getResource("SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.DAT").getFile());
        log.debug("fileBx PATH: {}", fileBx.getAbsolutePath());
        byte[] framedata = ByteOperationServices.readBinaryFile(fileBx);

        BandxTransferFrame frame = new BandxTransferFrame();
        ApidPacket apidPacket = new ApidPacket();
        apidPacket.setApidValue(1620);
        apidPacket.setTid(1);
        apidPacket.setPid(18);
        apidPacket.setPcat(20);
        frame.setApidPacket(apidPacket);

        frame.setCcsdsVersion(0);
        frame.setCcsdsType(0);
        frame.setCcsdsHeadFlag(0);
        frame.setCcsdsGFlag(3);
        frame.setCcsdsCounter(0);
        frame.setCcsdsPlength(1015);

        frame.setPacketTimeSec(3254578L);
        frame.setTimeMicroSec(910235);
        frame.setLocalTimeCounter(999L);

        PacketId pktid = new PacketId();
        pktid.setPktId(30);
        pktid.setApid(apidPacket);
        frame.setPacketId(pktid.getPktId());
        frame.setObsidType(85);
        frame.setObsidNum(16777215);
        frame.setObsid(1442840575L);

        DataSourcePacket dataSourcePacket = new DataSourcePacket();
        frame.setDataSourcePacket(dataSourcePacket);
        log.debug("frame was created {}", frame.toString());
        try {
            BandxTransferFrame bxpkt = bandxDeserializer.deserialize(framedata);
            log.debug("bxpkt deserialized is {}", bxpkt.toString());
            assertThat(frame).isEqualTo(bxpkt);
        }
        catch (BandxServiceException e) {
            log.debug("DESERIALIZE EXCEPTION {}", e);
        }
    }

    /**
     * Test method for FileNameDecoder.
     */
    @Test
    public void testDatNameDecode() {
        String datfileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.dat";
        LcDatFile entity = bandxDeserializer.fileNameDecoderRegexp(datfileName);
        assertThat(entity.getApidHexa()).isEqualTo("0654");
        assertThat(entity.getStation()).isEqualTo("KR000");
        datfileName = "SVOM_L0C_ED_12_0654_20170207T160200_20170207T160222_000001_KR000_001_1.dat";
        entity = bandxDeserializer.fileNameDecoderRegexp(datfileName);
        assertThat(entity.getEndTime()).isEqualTo("20170207T160222");
    }

    /**
     * Test method for FileNameDecoder.
     */
    @Test
    public void testTarNameDecode() {
        String filename = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        String apidhexa = bandxDeserializer.tarNameDecodeApid(filename);
        assertThat(apidhexa).isEqualTo("0654");
        String stTime = bandxDeserializer.tarNameDecodeStartTime(filename);
        assertThat(stTime).isEqualTo("20170207T160200");
    }

    /**
     * Test method for FileNameDecoder.
     */
    @Test
    public void testFileNameDecoder() {
        String filename = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.dat";
        LcDatFile response = bandxDeserializer.fileNameDecoderRegexp(filename);
        assertThat(response.getType()).isEqualTo("ED");
        assertThat(response.getStation()).isEqualTo("KR000");
        assertThat(response.getIndex()).isEqualTo(1);
    }

    /**
     * Test method for IsDatFileNameCoform.
     */
    @Test
    public void testIsDatFileNameCoform() {
        String datfileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.dat";
        assertThat(bandxDeserializer.isDatFileNameCoform(datfileName)).isTrue();
    }

    /**
     * Test method for IsXMLFileNameCoform.
     */
    @Test
    public void testIsXMLFileNameCoform() {
        String xmlfileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.xml";
        assertThat(bandxDeserializer.isXMLFileNameCoform(xmlfileName)).isTrue();
    }

    /**
     * Test method for IsTarFileNameCoform.
     */
    @Test
    public void testIsTarFileNameCoform() {
        String tarfileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        assertThat(bandxDeserializer.isTarFileNameCoform(tarfileName)).isTrue();
    }

}
