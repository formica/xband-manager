package fr.svom.xband.data.model;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class TarFileTest {

    private TarFile tarfile = new TarFile();

    private File localtarfile;

    private String tarFileName;

    @Before
    public void setUp() {
        ClassLoader classLoader = getClass().getClassLoader();
        tarFileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";
        localtarfile = new File(classLoader.getResource(tarFileName).getFile());
    }

    @Test
    public void testSetTarName() {
        tarfile.setTarName(tarFileName);
        assertEquals(tarFileName, tarfile.getTarName());
    }

    @Test
    public void testSetTarPath() {
        tarfile.setTarPath(localtarfile.getAbsolutePath());
        assertEquals(localtarfile.getAbsolutePath(), tarfile.getTarPath());
    }

    @Test
    public void testSetApidHexa() {
        tarfile.setApidHexa("0654");
        String newValue = "0654";
        assertEquals(newValue, tarfile.getApidHexa());
    }

    @Test
    public void testSetStartTime() {
        tarfile.setStartTime("20170207T160200");
        String newValue = "20170207T160200";
        assertEquals(newValue, tarfile.getStartTime());
    }

    @Test
    public void testSetNbDatfile() {
        tarfile.setNbDatfile(4);
        Integer newValue = 4;
        assertEquals(newValue, tarfile.getNbDatfile());
    }

    @Test
    public void testSetIsTarNameConform() {
        tarfile.setIsTarNameConform(true);
        assertEquals(true, tarfile.getIsTarNameConform());
    }

}
