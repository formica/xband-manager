package fr.svom.xband.data.config;

import fr.svom.xband.data.config.converters.DateToOffDateTimeConverter;
import fr.svom.xband.data.config.converters.TimestampToOffDateTimeConverter;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class ConfigTest {

    @Test
    public void propertiesTest() {
        XbandProperties props = new XbandProperties();
        props.setDownload("/tmp");
        props.setSecurity("none");
        props.setUpload("/tmp");
        assertThat(props.getDownload()).isEqualTo("/tmp");
        assertThat(props.getUpload()).isEqualTo("/tmp");
        assertThat(props.getSecurity()).isEqualTo("none");
    }

    @Test
    public void convertersTest() {
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        // Add converters.
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter(new DateToOffDateTimeConverter());
        converterFactory.registerConverter(new TimestampToOffDateTimeConverter());
        MapperFacade facade = mapperFactory.getMapperFacade();
        Date now = new Date();
        OffsetDateTime nowdt = facade.map(now, OffsetDateTime.class);
        long nowms = nowdt.toInstant().toEpochMilli();
        assertThat(nowms).isEqualTo(now.getTime());
        Date fromdt = facade.map(nowdt, Date.class);
        assertThat(fromdt).isEqualTo(now);
    }
}
