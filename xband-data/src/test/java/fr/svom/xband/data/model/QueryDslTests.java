package fr.svom.xband.data.model;


import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.repository.ApidRepository;
import fr.svom.xband.data.repository.BandxTransferFrameRepository;
import fr.svom.xband.data.repository.DataSourceRepository;
import fr.svom.xband.data.repository.LcDatFileRepository;
import fr.svom.xband.data.repository.TarFileRepository;
import fr.svom.xband.data.repository.querydsl.BandxTransferFrameFiltering;
import fr.svom.xband.data.repository.querydsl.DatFileFiltering;
import fr.svom.xband.data.repository.querydsl.DataSourceFiltering;
import fr.svom.xband.data.repository.querydsl.IFilteringCriteria;
import fr.svom.xband.data.repository.querydsl.PacketApidFiltering;
import fr.svom.xband.data.repository.querydsl.SearchCriteria;
import fr.svom.xband.data.repository.querydsl.TarFileFiltering;
import fr.svom.tools.DataGeneratorEntity;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueryDslTests {


    private static final String SORT_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:)([ASC|DESC]+?),";
    private static final String QRY_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:|<|>)([a-zA-Z0-9_\\-\\/\\.\\*\\%]+?),";

    private static final Logger log = LoggerFactory.getLogger(QueryDslTests.class);

    @Autowired
    private ApidRepository apidRepository;

    @Autowired
    private BandxTransferFrameRepository bandxTransferFrameRepository;

    @Autowired
    private DataSourceRepository dataSourceRepository;

    @Autowired
    private LcDatFileRepository lcDatFileRepository;

    @Autowired
    private TarFileRepository tarFileRepository;

    @Autowired
    @Qualifier("dataSource")
    private DataSource mainDataSource;

    DataGeneratorEntity dg = new DataGeneratorEntity();

    @Before
    public void setUp() {
        final Path bpath = Paths.get("/tmp/xband");
        if (!bpath.toFile().exists()) {
            try {
                Files.createDirectories(bpath);
            }
            catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        final Path cpath = Paths.get("/tmp/xband-dump");
        if (!cpath.toFile().exists()) {
            try {
                Files.createDirectories(cpath);
            }
            catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    @Test
    public void testApidRepository() throws Exception {
        final ApidPacket apid = (ApidPacket) dg.createPojo(ApidPacket.class);
        apidRepository.save(apid);
        final IFilteringCriteria filter = new PacketApidFiltering();
        final PageRequest preq = createPageRequest(0, 10, "name:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "instrument:%,category:%,apid:" + apid.getApidValue());
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        BooleanExpression wherepred = createWhere(expressions);

        final Page<ApidPacket> dtolist = apidRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isPositive();

        final Optional<ApidPacket> loaded = apidRepository.findByName(apid.getName());
        assertThat(loaded.get()).isNotNull();
    }

    @Test
    public void testBandxFrameRepository() throws Exception {
        final BandxTransferFrame frame = (BandxTransferFrame) dg.createPojo(BandxTransferFrame.class);
        final ApidPacket apid = (ApidPacket) dg.createPojo(ApidPacket.class);
        final LcDatFile dat = (LcDatFile) dg.createPojo(LcDatFile.class);
        dat.setStation("KR001");
        DataSourcePacket pkt = new DataSourcePacket();
        pkt.setPacket("somebinchain".getBytes(StandardCharsets.UTF_8));
        pkt.setHashId(frame.getBinaryHash());
        DataSourcePacket psaved = dataSourceRepository.save(pkt);
        frame.setDataSourcePacket(psaved);
        ApidPacket apsaved = apidRepository.save(apid);
        LcDatFile ldsaved = lcDatFileRepository.save(dat);
        frame.setApidPacket(apsaved);
        frame.setLcDatFile(ldsaved);
        frame.setFrameValid(Boolean.TRUE);
        frame.setFrameDuplicate(Boolean.FALSE);
        frame.setKnown(Boolean.TRUE);
        frame.setPacketId(9);
        frame.setPacketTimeSec(1000L);
        BandxTransferFrame savedFrame = bandxTransferFrameRepository.save(frame);
        final IFilteringCriteria filter = new BandxTransferFrameFiltering();
        final PageRequest preq = createPageRequest(0, 10, "pktOrder:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "apid:" + apsaved.getApidValue() + ",obsid:" + frame.getObsid() + ",orbit:"
                + frame.getLcDatFile().getOrbit());
        List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        BooleanExpression wherepred = createWhere(expressions);
        final Page<BandxTransferFrame> dtolist = bandxTransferFrameRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isPositive();

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "id:" + savedFrame.getFrameId()
                + ",lcfilename:%,packettime>0,isframevalid:true,isframeduplicate:false"
                + ",station:" + frame.getLcDatFile().getStation() + ",obsidtype:" + frame.getObsidType()
                + ",obsidnum:" + frame.getObsidNum());
        expressions = filter.createFilteringConditions(params1);
        wherepred = createWhere(expressions);
        final Page<BandxTransferFrame> dtolist1 = bandxTransferFrameRepository.findAll(wherepred, preq);
        assertThat(dtolist1.getSize()).isPositive();

        final List<SearchCriteria> params2 = createMatcherCriteria(
                "packetid:" + savedFrame.getPacketId()
                + ",packetid>0,packetid<99"
                + ",tarfilename:%,instrument:%,category:%");
        expressions = filter.createFilteringConditions(params2);
        wherepred = createWhere(expressions);
        final Page<BandxTransferFrame> dtolist2 = bandxTransferFrameRepository.findAll(wherepred, preq);
        assertThat(dtolist2.getSize()).isPositive();

        final List<BandxTransferFrame> loaded =
                bandxTransferFrameRepository.findByApidPacket(apsaved.getApidValue());
        assertThat(loaded).isNotNull();

        // Now proceed with DataSource filtering
        final IFilteringCriteria filter1 = new DataSourceFiltering();
        final PageRequest preq1 = createPageRequest(0, 10, "hashId:ASC");

        final List<SearchCriteria> params3 = createMatcherCriteria(
                "apid:" + apsaved.getApidValue()
                + ",obsid:" + frame.getObsid()
                + ",orbit:" + frame.getLcDatFile().getOrbit());
        expressions = filter1.createFilteringConditions(params3);
        wherepred = createWhere(expressions);
        final Page<DataSourcePacket> dtolist3 = dataSourceRepository.findAll(wherepred, preq1);
        assertThat(dtolist3.getSize()).isPositive();

        final List<SearchCriteria> params4 = createMatcherCriteria(
                "id:" + savedFrame.getFrameId()
                + ",lcfilename:%,packettime>0,packettime<9999999,isframevalid:true,isframeduplicate:false"
                + ",station:" + frame.getLcDatFile().getStation() + ",obsidtype:" + frame.getObsidType()
                + ",obsidnum:" + frame.getObsidNum());
        expressions = filter1.createFilteringConditions(params4);
        wherepred = createWhere(expressions);
        final Page<DataSourcePacket> dtolist4 = dataSourceRepository.findAll(wherepred, preq1);
        assertThat(dtolist4.getSize()).isPositive();

        final List<SearchCriteria> params5 = createMatcherCriteria(
                "packetid:" + savedFrame.getPacketId()
                + ",packetid>0,packetid<99"
                + ",tarfilename:%");
        expressions = filter1.createFilteringConditions(params5);
        wherepred = createWhere(expressions);
        final Page<DataSourcePacket> dtolist5 = dataSourceRepository.findAll(wherepred, preq1);
        assertThat(dtolist5.getSize()).isPositive();

    }

    @Test
    public void testTarFileRepository() throws Exception {
        final TarFile tar = (TarFile) dg.createPojo(TarFile.class);
        tar.setIsTarNameConform(Boolean.TRUE);
        tar.setStartTimeTs(new Timestamp(Instant.now().toEpochMilli()));
        TarFile trsaved = tarFileRepository.save(tar);
        //
        final PageRequest preq = createPageRequest(0, 10, "tarName:ASC");
        final IFilteringCriteria filter = new TarFileFiltering();
        //
        final List<SearchCriteria> params = createMatcherCriteria(
                "name:%,path:%,starttime>0,isconform:true");
        List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        BooleanExpression wherepred = createWhere(expressions);
        final Page<TarFile> dtolist = tarFileRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isPositive();

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "starttime<999999999999,starttime:"+trsaved.getStartTimeTs().getTime());
        expressions = filter.createFilteringConditions(params1);
        wherepred = createWhere(expressions);
        final Page<TarFile> dtolist1 = tarFileRepository.findAll(wherepred, preq);
        assertThat(dtolist1.getSize()).isPositive();

    }

    @Test
    public void testDatFileRepository() throws Exception {
        final LcDatFile dat = (LcDatFile) dg.createPojo(LcDatFile.class);
        dat.setIsFilenameConform(Boolean.TRUE);
        dat.setEndTimeTs(new Timestamp(Instant.now().toEpochMilli()));
        LcDatFile datsaved = lcDatFileRepository.save(dat);
        //
        final PageRequest preq = createPageRequest(0, 10, "lcfileName:ASC");
        final IFilteringCriteria filter = new DatFileFiltering();
        //
        final List<SearchCriteria> params = createMatcherCriteria(
                "name:%,tarname:%,starttime>0,endtime>0");
        List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        BooleanExpression wherepred = createWhere(expressions);
        final Page<LcDatFile> dtolist = lcDatFileRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isPositive();

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "isconform:true,starttime<999999999999,endtime:"+dat.getEndTimeTs().getTime());
        expressions = filter.createFilteringConditions(params1);
        wherepred = createWhere(expressions);
        final Page<LcDatFile> dtolist1 = lcDatFileRepository.findAll(wherepred, preq);
        assertThat(dtolist1.getSize()).isPositive();

    }

    @Test
    public void criteriaTest() {
        SearchCriteria criteria = new SearchCriteria("test", ":", Long.valueOf(1));
        criteria.setKey("name");
        criteria.setOperation(">");
        criteria.setValue(Long.valueOf(99));
        assertThat(criteria.dump().length()).isPositive();
        assertThat(criteria.getKey().length()).isPositive();
        assertThat(criteria.getOperation().length()).isPositive();
        assertThat(criteria.getValue()).isNotNull();
    }

    protected PageRequest createPageRequest(Integer page, Integer size, String sort) {

        final Pattern sortpattern = Pattern.compile(SORT_PATTERN);
        final Matcher sortmatcher = sortpattern.matcher(sort + ",");
        final List<Order> orderlist = new ArrayList<>();
        while (sortmatcher.find()) {
            Direction direc = Direction.ASC;
            if (sortmatcher.group(3).equals("DESC")) {
                direc = Direction.DESC;
            }
            final String field = sortmatcher.group(1);
            log.debug("Creating new order: {} {} ", direc, field);
            orderlist.add(new Order(direc, field));
        }
        log.debug("Created list of sorting orders of size {}", orderlist.size());
        final Order[] orders = new Order[orderlist.size()];
        int i = 0;
        for (final Order order : orderlist) {
            log.debug("Order @ {} = {}", i, order);
            orders[i++] = order;
        }
        final Sort msort = Sort.by(orders);
        return PageRequest.of(page, size, msort);
    }

    protected List<SearchCriteria> createMatcherCriteria(String by) {

        final Pattern pattern = Pattern.compile(QRY_PATTERN);
        final Matcher matcher = pattern.matcher(by + ",");
        log.debug("Pattern is {}", pattern);
        log.debug("Matcher is {}", matcher);
        final List<SearchCriteria> params = new ArrayList<>();
        while (matcher.find()) {
            String val = matcher.group(3);
            val = val.replaceAll("\\*", "\\%");
            params.add(new SearchCriteria(matcher.group(1), matcher.group(2), val));
        }
        log.debug("List of search criteria: {}", params.size());
        return params;
    }

    protected BooleanExpression createWhere(List<BooleanExpression> expressions) {
        BooleanExpression wherepred = null;
        for (final BooleanExpression exp : expressions) {
            if (wherepred == null) {
                wherepred = exp;
            }
            else {
                wherepred = wherepred.and(exp);
            }
        }
        return wherepred;
    }
}
