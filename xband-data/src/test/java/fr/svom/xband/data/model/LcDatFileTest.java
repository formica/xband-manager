package fr.svom.xband.data.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class LcDatFileTest {

    private static final String lcFileName = "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.DAT";

    private static final String tarFileName="SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar";


    @Test
    public void testLcDatFile() {
        LcDatFile lcdatfile = new LcDatFile();
        ClassLoader classLoader = LcDatFileTest.class.getClassLoader();
        File lcFile = new File(classLoader.getResource(lcFileName).getFile());

        TarFile tarfile = new TarFile();
        tarfile.setTarName(tarFileName);
        tarfile.setTarPath(lcFile.getAbsolutePath());

        lcdatfile.setLcfileName(lcFileName);
        assertThat(lcFileName).isEqualTo(lcdatfile.getLcfileName());
        lcdatfile.setTarFile(tarfile);
        assertThat(tarfile).isEqualTo(lcdatfile.getTarFile());
        lcdatfile.setFilePath(lcFile.getAbsolutePath());
        assertThat(lcFile.getAbsolutePath()).isEqualTo(lcdatfile.getFilePath());
        lcdatfile.setType("ED");
        String type = "ED";
        assertThat(type).isEqualTo(lcdatfile.getType());
        lcdatfile.setApidHexa("0654");
        String apid = "0654";
        assertThat(apid).isEqualTo(lcdatfile.getApidHexa());
        lcdatfile.setFileSize(4088L);
        Long size = 4088L;
        assertThat(size).isEqualTo(lcdatfile.getFileSize());
        lcdatfile.setStartTime("20170207T160200");
        String dt = "20170207T160200";
        assertThat(dt).isEqualTo(lcdatfile.getStartTime());
        lcdatfile.setEndTime("");
        String et = "";
        assertThat(et).isEqualTo(lcdatfile.getEndTime());
        lcdatfile.setOrbit(1);
        Integer orbit = 1;
        assertThat(orbit).isEqualTo(lcdatfile.getOrbit());
        lcdatfile.setStation("KR000");
        String newValue = "KR000";
        assertThat(newValue).isEqualTo(lcdatfile.getStation());
        lcdatfile.setVer("001");
        String ver = "001";
        assertThat(ver).isEqualTo(lcdatfile.getVer());
        lcdatfile.setIndex(1);
        Integer index = 1;
        assertThat(index).isEqualTo(lcdatfile.getIndex());
        lcdatfile.setError(0);
        Integer error = 0;
        assertThat(error).isEqualTo(lcdatfile.getError());
        lcdatfile.setIsFilenameConform(true);
        Boolean conform = true;
        assertThat(conform).isEqualTo(lcdatfile.getIsFilenameConform());
        Date date = new Date();
        Timestamp time = new Timestamp(date.getTime());
        lcdatfile.setReceptionTime(time);
        assertThat(time).isEqualTo(lcdatfile.getReceptionTime());
        assertThat(lcdatfile.getPassId()).contains("KR000");
        lcdatfile.setPassId(null);
        assertThat(lcdatfile.getPassId()).contains("1");
    }

}
