package fr.svom.xband.data.model;

import fr.svom.xband.swagger.model.BandxTransferFrameDto;
import fr.svom.xband.swagger.model.DatFileDto;
import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.TarFileDto;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("default")
@Slf4j
public class MapperTest {
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    private final Random rnd = new Random();

    public void fillRandom(Object obj, Class<?> clazz) {
        try {
            Method[] publicMethods = clazz.getMethods();
            for (Method aMethod : publicMethods) {
                if (aMethod.getName().startsWith("set")
                        && aMethod.getParameterCount() == 1) {
                    Class<?> argtype = aMethod.getParameterTypes()[0];
                    if (argtype.equals(Double.class)) {
                        Double val = rnd.nextDouble();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Float.class)) {
                        Float val = rnd.nextFloat();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(BigDecimal.class)) {
                        aMethod.invoke(obj, BigDecimal.valueOf(rnd.nextDouble()));
                    } else if (argtype.equals(Long.class)) {
                        Long val = rnd.nextLong();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Integer.class)) {
                        Integer val = rnd.nextInt();
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(String.class)) {
                        String val = String.valueOf(rnd.nextInt()); // TODO generate better string
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Date.class)) {
                        Date val = Date.from(Instant.ofEpochMilli(rnd.nextLong()));
                        aMethod.invoke(obj, val);
                    } else if (argtype.equals(Timestamp.class)) {
                        Timestamp val = Timestamp.from(Instant.ofEpochMilli(rnd.nextLong()));
                        aMethod.invoke(obj, val);
                    } else {
                        log.warn("fillRandom: not calling setter method {}", aMethod);
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new InternalError(e);
        }
    }

    public void testMapper(Class<?> pojoType, Class<?> dtoType) {
        try {
            Object item = pojoType.getDeclaredConstructor().newInstance();
            fillRandom(item, pojoType);
            Object dto = mapper.map(item, dtoType);
            Object pojo = mapper.map(dto, pojoType);
            log.info("Generated item    = {}", item);
            log.info("Converted to pojo = {}", pojo);
            assertThat(pojo).isEqualTo(item);
        } catch (Exception e) {
            throw new InternalError(e);
        }
    }

    @Test
    public void testApidPacket() {
        testMapper(ApidPacket.class, PacketApidDto.class);
    }
    @Test
    public void testTarFile() {
        testMapper(TarFile.class, TarFileDto.class);
    }
    @Test
    public void testBandxFrame() {
        testMapper(BandxTransferFrame.class, BandxTransferFrameDto.class);
    }
    @Test
    public void testLcDatFile() {
        testMapper(LcDatFile.class, DatFileDto.class);
    }

}
