package fr.svom.tools;

import fr.svom.xband.data.model.TarFile;
import fr.svom.xband.data.serialization.BandxFrameDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

@Slf4j
public class BxDesTest {

    public static void main(String[] argv) {

        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                // date/time
                .appendPattern("yyyyMMdd'T'HHmmss")
                // optional fraction of seconds (from 0 to 9 digits)
                // create formatter
                .toFormatter();
        BandxFrameDeserializer des = new BandxFrameDeserializer();
        //des.setFormatter(dateTimeFormatter);
        TarFile tar = new TarFile();
        String name = "SVOM_L0C_ED_0654_20210130T090600_000493_KR00P.tar";
        //name = "SVOM_L0C_SD_0671_20210130T090600_000493_KR00P.tar";
        tar.setTarName(name);
        Boolean isConform = des.isTarFileNameCoform(name);
        tar.setIsTarNameConform(isConform);
        try {
            TarFile nt = des.tarNameDecoderRegexp(tar);
            log.info("Parsed tar fields from file name: {}", nt);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}