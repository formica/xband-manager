package fr.svom.tools;

import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.TarFile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpMatching {

//    private final static String L0CDAT_REGEXP = "SVOM_L0C_([A-Z]+)?(_\\d{2})?_(\\d{4})_(\\w+)_(\\w+)_(\\d+)_(\\w+)_"
//                                                + "(\\d+)_(\\d+).(dat|DAT)";
    private static final String L0CDAT_REGEXP = "SVOM_L0C_([A-Z]+)?(_\\d{2})?_(\\d{4})_(\\w{15})?(_\\w{15})?_(\\d+)_"
                                                + "(\\w+)_"
                                                + "(\\d+)_(\\d+).(dat|DAT)";
//    private final static String L0CTAR_REGEXP = "SVOM_L0C_([A-Z]+)_(\\d+)_(\\w+)_(\\d+)_(\\w+)_(\\d+)"
//                                                + ".(tar|TAR)";

    private final static String L0CTAR_REGEXP = "SVOM_L0C_([A-Z]+)?_(\\d{4})_(\\w{15})_(\\d+)_(\\w+)(_\\d+)?"
                                                + ".(tar|TAR)";

    public static void main(String[] argv) {

        try {
            // SVOM_L0C_ED_0654_20170207T160200_20170207T160222_000001_KR000_001_1.dat
            String regex = "SVOM_L0C_([A-Z]+)_(\\d+)_(\\w+)_(\\w+)_(\\d+)_(\\w+)_(\\d+)_(\\d+).dat";
            String s = "SVOM_L0C_ED_0654_20170207T160200_20170207T160222_000001_KR000_001_1.dat";
            Pattern pattern1 = Pattern.compile(regex);
            Matcher matcher1 = pattern1.matcher(s);

            StringBuilder result = new StringBuilder();
            while (matcher1.find()) {
                result.append("type=");
                result.append(matcher1.group(1));
                result.append(", apid=");
                result.append(matcher1.group(2));
                result.append(", since=");
                result.append(matcher1.group(3));
                result.append(", until=");
                result.append(matcher1.group(4));
                result.append(", orbit=");
                result.append(matcher1.group(5));
                result.append(", station=");
                result.append(matcher1.group(6));
                result.append(", num=");
                result.append(matcher1.group(7));
                result.append(", ver=");
                result.append(matcher1.group(8));
            }

            System.out.println("Test using regex 1: " + result.toString());
            String s2 = "SVOM_L0C_ED_12_0654_20170207T160200_20170207T160222_000001_KR000_001_1.dat";

            Pattern pattern = Pattern.compile(L0CDAT_REGEXP);
            Matcher matcher = pattern.matcher(s2);
            LcDatFile lcDatFile = new LcDatFile();
            lcDatFile.setLcfileName(s2);
            while (matcher.find()) {
                System.out.println("Found matching of length " + matcher.groupCount());
                int i = 1;
                System.out.println("type =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setType(matcher.group(i));
                i++;
                if (matcher.groupCount() > 8 && matcher.group(i) != null) {
                    System.out.println(" Optional group " + i + " = " + matcher.group(i));
                    String vcid = matcher.group(i).substring(1);
                    lcDatFile.setVcid(Integer.valueOf(vcid));
                    i++; // skip the optional
                }
                System.out.println("apid => group " + i + " = " + matcher.group(i));
                lcDatFile.setApidHexa(matcher.group(i++));
                System.out.println("start =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setStartTime(matcher.group(i++));
                if (matcher.groupCount() > 7 && matcher.group(i) != null) {
                    System.out.println(" end => Optional group " + i + " = " + matcher.group(i));
                    String et = matcher.group(i).substring(1);
                    lcDatFile.setEndTime(et);
                    i++; // skip the optional
                }
                System.out.println("orbit =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setOrbit(Integer.valueOf(matcher.group(i++)));
                System.out.println("station =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setStation(matcher.group(i++));
                System.out.println("ver =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setVer(matcher.group(i++));
                System.out.println("index =>  group " + i + " = " + matcher.group(i));
                lcDatFile.setIndex(Integer.valueOf(matcher.group(i++)));
            }
            System.out.println("MATCHING REGEXP 1: " + lcDatFile.toString());

            Pattern pattern2 = Pattern.compile(L0CDAT_REGEXP);
            Matcher matcher2 = pattern2.matcher(s);
            LcDatFile lcDatFile2 = new LcDatFile();
            lcDatFile2.setLcfileName(s);
            while (matcher2.find()) {
                System.out.println("Found 2 matching of length " + matcher2.groupCount());
                int i = 1;
                System.out.println(" group " + i + " = " + matcher2.group(i));
                lcDatFile2.setType(matcher2.group(i++));
                System.out.println(" group " + i + " = " + matcher2.group(i));
                if (matcher2.groupCount() > 8 && matcher2.group(i) != null) {
                    String vcid = matcher2.group(i++).substring(1);
                    lcDatFile2.setVcid(Integer.valueOf(vcid));
                    System.out.println(" Optional group " + i + " = " + matcher2.group(i));
                }
                else {
                    i++; // skip the optional
                }
                lcDatFile2.setApidHexa(matcher2.group(i++));
                System.out.println(" group " + i + " = " + matcher2.group(i));
                lcDatFile2.setStartTime(matcher2.group(i++));
                lcDatFile2.setEndTime(matcher2.group(i++));
                lcDatFile2.setOrbit(Integer.valueOf(matcher2.group(i++)));
                lcDatFile2.setStation(matcher2.group(i++));
                lcDatFile2.setVer(matcher2.group(i++));
                lcDatFile2.setIndex(Integer.valueOf(matcher2.group(i++)));
            }
            System.out.println("MATCHING REGEXP 2: " + lcDatFile.toString());
            System.out.println(lcDatFile2.toString());
            // TAR File
            String s3 = "SVOM_L0C_SD_0671_20210130T210120_000502_SY00P.tar";
            Pattern pattern3 = Pattern.compile(L0CTAR_REGEXP);
            Matcher matcher3 = pattern3.matcher(s3);
            TarFile tar3 = new TarFile();
            tar3.setTarName(s3);
            while (matcher3.find()) {
                System.out.println("Found 3 matching of length " + matcher3.groupCount());
                for (int i=0; i<matcher3.groupCount();i++) {
                    System.out.println(" group " + i + " = " + matcher3.group(i));
                }
            }
            System.out.println("MATCHING REGEXP 3: " + tar3.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
