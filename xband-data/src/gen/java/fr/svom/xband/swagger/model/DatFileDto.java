/*
 * BAND-X REST API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-oas3
 * Contact: andrea.formica@cea.fr
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package fr.svom.xband.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;

/**
 * DatFileDto
 */
@JsonPropertyOrder({
  DatFileDto.JSON_PROPERTY_LCFILE_NAME,
  DatFileDto.JSON_PROPERTY_TAR_FILE,
  DatFileDto.JSON_PROPERTY_FILE_PATH,
  DatFileDto.JSON_PROPERTY_FILE_SIZE,
  DatFileDto.JSON_PROPERTY_IS_FILENAME_CONFORM,
  DatFileDto.JSON_PROPERTY_TYPE,
  DatFileDto.JSON_PROPERTY_APID_HEXA,
  DatFileDto.JSON_PROPERTY_START_TIME,
  DatFileDto.JSON_PROPERTY_END_TIME,
  DatFileDto.JSON_PROPERTY_START_TIME_TS,
  DatFileDto.JSON_PROPERTY_END_TIME_TS,
  DatFileDto.JSON_PROPERTY_ORBIT,
  DatFileDto.JSON_PROPERTY_STATION,
  DatFileDto.JSON_PROPERTY_PASS_ID,
  DatFileDto.JSON_PROPERTY_VER,
  DatFileDto.JSON_PROPERTY_INDEX,
  DatFileDto.JSON_PROPERTY_RECEPTION_TIME,
  DatFileDto.JSON_PROPERTY_ERROR
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class DatFileDto   {
  public static final String JSON_PROPERTY_LCFILE_NAME = "lcfileName";
  @JsonProperty(JSON_PROPERTY_LCFILE_NAME)
  private String lcfileName;

  public static final String JSON_PROPERTY_TAR_FILE = "tarFile";
  @JsonProperty(JSON_PROPERTY_TAR_FILE)
  private TarFileDto tarFile;

  public static final String JSON_PROPERTY_FILE_PATH = "filePath";
  @JsonProperty(JSON_PROPERTY_FILE_PATH)
  private String filePath;

  public static final String JSON_PROPERTY_FILE_SIZE = "fileSize";
  @JsonProperty(JSON_PROPERTY_FILE_SIZE)
  private Long fileSize;

  public static final String JSON_PROPERTY_IS_FILENAME_CONFORM = "isFilenameConform";
  @JsonProperty(JSON_PROPERTY_IS_FILENAME_CONFORM)
  private Boolean isFilenameConform;

  public static final String JSON_PROPERTY_TYPE = "type";
  @JsonProperty(JSON_PROPERTY_TYPE)
  private String type;

  public static final String JSON_PROPERTY_APID_HEXA = "apidHexa";
  @JsonProperty(JSON_PROPERTY_APID_HEXA)
  private String apidHexa;

  public static final String JSON_PROPERTY_START_TIME = "startTime";
  @JsonProperty(JSON_PROPERTY_START_TIME)
  private String startTime;

  public static final String JSON_PROPERTY_END_TIME = "endTime";
  @JsonProperty(JSON_PROPERTY_END_TIME)
  private String endTime;

  public static final String JSON_PROPERTY_START_TIME_TS = "startTimeTs";
  @JsonProperty(JSON_PROPERTY_START_TIME_TS)
  private Long startTimeTs;

  public static final String JSON_PROPERTY_END_TIME_TS = "endTimeTs";
  @JsonProperty(JSON_PROPERTY_END_TIME_TS)
  private Long endTimeTs;

  public static final String JSON_PROPERTY_ORBIT = "orbit";
  @JsonProperty(JSON_PROPERTY_ORBIT)
  private Integer orbit;

  public static final String JSON_PROPERTY_STATION = "station";
  @JsonProperty(JSON_PROPERTY_STATION)
  private String station;

  public static final String JSON_PROPERTY_PASS_ID = "passId";
  @JsonProperty(JSON_PROPERTY_PASS_ID)
  private String passId;

  public static final String JSON_PROPERTY_VER = "ver";
  @JsonProperty(JSON_PROPERTY_VER)
  private String ver;

  public static final String JSON_PROPERTY_INDEX = "index";
  @JsonProperty(JSON_PROPERTY_INDEX)
  private Integer index;

  public static final String JSON_PROPERTY_RECEPTION_TIME = "receptionTime";
  @JsonProperty(JSON_PROPERTY_RECEPTION_TIME)
  private OffsetDateTime receptionTime;

  public static final String JSON_PROPERTY_ERROR = "error";
  @JsonProperty(JSON_PROPERTY_ERROR)
  private Integer error;

  public DatFileDto lcfileName(String lcfileName) {
    this.lcfileName = lcfileName;
    return this;
  }

  /**
   * Get lcfileName
   * @return lcfileName
   **/
  @JsonProperty("lcfileName")
  @ApiModelProperty(value = "")
  
  public String getLcfileName() {
    return lcfileName;
  }

  public void setLcfileName(String lcfileName) {
    this.lcfileName = lcfileName;
  }

  public DatFileDto tarFile(TarFileDto tarFile) {
    this.tarFile = tarFile;
    return this;
  }

  /**
   * Get tarFile
   * @return tarFile
   **/
  @JsonProperty("tarFile")
  @ApiModelProperty(value = "")
  @Valid 
  public TarFileDto getTarFile() {
    return tarFile;
  }

  public void setTarFile(TarFileDto tarFile) {
    this.tarFile = tarFile;
  }

  public DatFileDto filePath(String filePath) {
    this.filePath = filePath;
    return this;
  }

  /**
   * The file path
   * @return filePath
   **/
  @JsonProperty("filePath")
  @ApiModelProperty(value = "The file path")
  
  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public DatFileDto fileSize(Long fileSize) {
    this.fileSize = fileSize;
    return this;
  }

  /**
   * Get fileSize
   * @return fileSize
   **/
  @JsonProperty("fileSize")
  @ApiModelProperty(value = "")
  
  public Long getFileSize() {
    return fileSize;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public DatFileDto isFilenameConform(Boolean isFilenameConform) {
    this.isFilenameConform = isFilenameConform;
    return this;
  }

  /**
   * Name conformity flag
   * @return isFilenameConform
   **/
  @JsonProperty("isFilenameConform")
  @ApiModelProperty(value = "Name conformity flag")
  
  public Boolean getIsFilenameConform() {
    return isFilenameConform;
  }

  public void setIsFilenameConform(Boolean isFilenameConform) {
    this.isFilenameConform = isFilenameConform;
  }

  public DatFileDto type(String type) {
    this.type = type;
    return this;
  }

  /**
   * file type
   * @return type
   **/
  @JsonProperty("type")
  @ApiModelProperty(value = "file type")
  
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public DatFileDto apidHexa(String apidHexa) {
    this.apidHexa = apidHexa;
    return this;
  }

  /**
   * The apid in HEX format
   * @return apidHexa
   **/
  @JsonProperty("apidHexa")
  @ApiModelProperty(value = "The apid in HEX format")
  
  public String getApidHexa() {
    return apidHexa;
  }

  public void setApidHexa(String apidHexa) {
    this.apidHexa = apidHexa;
  }

  public DatFileDto startTime(String startTime) {
    this.startTime = startTime;
    return this;
  }

  /**
   * The file start time
   * @return startTime
   **/
  @JsonProperty("startTime")
  @ApiModelProperty(value = "The file start time")
  
  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public DatFileDto endTime(String endTime) {
    this.endTime = endTime;
    return this;
  }

  /**
   * The file end time
   * @return endTime
   **/
  @JsonProperty("endTime")
  @ApiModelProperty(value = "The file end time")
  
  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public DatFileDto startTimeTs(Long startTimeTs) {
    this.startTimeTs = startTimeTs;
    return this;
  }

  /**
   * Get startTimeTs
   * @return startTimeTs
   **/
  @JsonProperty("startTimeTs")
  @ApiModelProperty(value = "")
  
  public Long getStartTimeTs() {
    return startTimeTs;
  }

  public void setStartTimeTs(Long startTimeTs) {
    this.startTimeTs = startTimeTs;
  }

  public DatFileDto endTimeTs(Long endTimeTs) {
    this.endTimeTs = endTimeTs;
    return this;
  }

  /**
   * Get endTimeTs
   * @return endTimeTs
   **/
  @JsonProperty("endTimeTs")
  @ApiModelProperty(value = "")
  
  public Long getEndTimeTs() {
    return endTimeTs;
  }

  public void setEndTimeTs(Long endTimeTs) {
    this.endTimeTs = endTimeTs;
  }

  public DatFileDto orbit(Integer orbit) {
    this.orbit = orbit;
    return this;
  }

  /**
   * orbit
   * @return orbit
   **/
  @JsonProperty("orbit")
  @ApiModelProperty(value = "orbit")
  
  public Integer getOrbit() {
    return orbit;
  }

  public void setOrbit(Integer orbit) {
    this.orbit = orbit;
  }

  public DatFileDto station(String station) {
    this.station = station;
    return this;
  }

  /**
   * station
   * @return station
   **/
  @JsonProperty("station")
  @ApiModelProperty(value = "station")
  
  public String getStation() {
    return station;
  }

  public void setStation(String station) {
    this.station = station;
  }

  public DatFileDto passId(String passId) {
    this.passId = passId;
    return this;
  }

  /**
   * pass id
   * @return passId
   **/
  @JsonProperty("passId")
  @ApiModelProperty(value = "pass id")
  
  public String getPassId() {
    return passId;
  }

  public void setPassId(String passId) {
    this.passId = passId;
  }

  public DatFileDto ver(String ver) {
    this.ver = ver;
    return this;
  }

  /**
   * version
   * @return ver
   **/
  @JsonProperty("ver")
  @ApiModelProperty(value = "version")
  
  public String getVer() {
    return ver;
  }

  public void setVer(String ver) {
    this.ver = ver;
  }

  public DatFileDto index(Integer index) {
    this.index = index;
    return this;
  }

  /**
   * index
   * @return index
   **/
  @JsonProperty("index")
  @ApiModelProperty(value = "index")
  
  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public DatFileDto receptionTime(OffsetDateTime receptionTime) {
    this.receptionTime = receptionTime;
    return this;
  }

  /**
   * Get receptionTime
   * @return receptionTime
   **/
  @JsonProperty("receptionTime")
  @ApiModelProperty(value = "")
  
  public OffsetDateTime getReceptionTime() {
    return receptionTime;
  }

  public void setReceptionTime(OffsetDateTime receptionTime) {
    this.receptionTime = receptionTime;
  }

  public DatFileDto error(Integer error) {
    this.error = error;
    return this;
  }

  /**
   * error
   * @return error
   **/
  @JsonProperty("error")
  @ApiModelProperty(value = "error")
  
  public Integer getError() {
    return error;
  }

  public void setError(Integer error) {
    this.error = error;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DatFileDto datFileDto = (DatFileDto) o;
    return Objects.equals(this.lcfileName, datFileDto.lcfileName) &&
        Objects.equals(this.tarFile, datFileDto.tarFile) &&
        Objects.equals(this.filePath, datFileDto.filePath) &&
        Objects.equals(this.fileSize, datFileDto.fileSize) &&
        Objects.equals(this.isFilenameConform, datFileDto.isFilenameConform) &&
        Objects.equals(this.type, datFileDto.type) &&
        Objects.equals(this.apidHexa, datFileDto.apidHexa) &&
        Objects.equals(this.startTime, datFileDto.startTime) &&
        Objects.equals(this.endTime, datFileDto.endTime) &&
        Objects.equals(this.startTimeTs, datFileDto.startTimeTs) &&
        Objects.equals(this.endTimeTs, datFileDto.endTimeTs) &&
        Objects.equals(this.orbit, datFileDto.orbit) &&
        Objects.equals(this.station, datFileDto.station) &&
        Objects.equals(this.passId, datFileDto.passId) &&
        Objects.equals(this.ver, datFileDto.ver) &&
        Objects.equals(this.index, datFileDto.index) &&
        Objects.equals(this.receptionTime, datFileDto.receptionTime) &&
        Objects.equals(this.error, datFileDto.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lcfileName, tarFile, filePath, fileSize, isFilenameConform, type, apidHexa, startTime, endTime, startTimeTs, endTimeTs, orbit, station, passId, ver, index, receptionTime, error);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DatFileDto {\n");
    
    sb.append("    lcfileName: ").append(toIndentedString(lcfileName)).append("\n");
    sb.append("    tarFile: ").append(toIndentedString(tarFile)).append("\n");
    sb.append("    filePath: ").append(toIndentedString(filePath)).append("\n");
    sb.append("    fileSize: ").append(toIndentedString(fileSize)).append("\n");
    sb.append("    isFilenameConform: ").append(toIndentedString(isFilenameConform)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    apidHexa: ").append(toIndentedString(apidHexa)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("    startTimeTs: ").append(toIndentedString(startTimeTs)).append("\n");
    sb.append("    endTimeTs: ").append(toIndentedString(endTimeTs)).append("\n");
    sb.append("    orbit: ").append(toIndentedString(orbit)).append("\n");
    sb.append("    station: ").append(toIndentedString(station)).append("\n");
    sb.append("    passId: ").append(toIndentedString(passId)).append("\n");
    sb.append("    ver: ").append(toIndentedString(ver)).append("\n");
    sb.append("    index: ").append(toIndentedString(index)).append("\n");
    sb.append("    receptionTime: ").append(toIndentedString(receptionTime)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

