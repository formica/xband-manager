package fr.svom.xband.swagger.messages;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets DataSource
 */
/**
 * @author etrigui
 *
 */
public enum DataSource {

    /**
     * VHF.
     */
    VHF("VHF"),

    /**
     * XBAND.
     */
    XDB("XDB"),

    /**
     * FPOC.
     */
    FPOC("FPOC");

    /**
     * value.
     */
    private String value;

    /**
     * @param value
     *        value of data source.
     */
    DataSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * @param text
     *        text of data source.
     * @return DataSource
     */
    @JsonCreator
    public static DataSource fromValue(String text) {
        for (DataSource b : DataSource.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
