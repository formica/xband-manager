package fr.svom.xband.swagger.messages;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * DataNotification
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2020-03-23T21:55:21.919+01:00")
public class DataNotification   {
    /**
     * Gets or Sets messageClass
     */
    public enum MessageClassEnum {
        DATANOTIFICATION("DataNotification");

        private String value;

        MessageClassEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static MessageClassEnum fromValue(String text) {
            for (MessageClassEnum b : MessageClassEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    @JsonProperty("message_class")
    private MessageClassEnum messageClass = MessageClassEnum.DATANOTIFICATION;

    @JsonProperty("message_date")
    private String messageDate = null;

    @JsonProperty("content")
    private DataNotificationContent content = null;

    public DataNotification messageClass(MessageClassEnum messageClass) {
        this.messageClass = messageClass;
        return this;
    }

    /**
     * Get messageClass
     * @return messageClass
     **/
    @JsonProperty("message_class")
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public MessageClassEnum getMessageClass() {
        return messageClass;
    }

    public void setMessageClass(MessageClassEnum messageClass) {
        this.messageClass = messageClass;
    }

    public DataNotification messageDate(String messageDate) {
        this.messageDate = messageDate;
        return this;
    }

    /**
     * Starting Date of the service
     * @return messageDate
     **/
    @JsonProperty("message_date")
    @ApiModelProperty(required = true, value = "Starting Date of the service")
    @NotNull
    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public DataNotification content(DataNotificationContent content) {
        this.content = content;
        return this;
    }

    /**
     * Get content
     * @return content
     **/
    @JsonProperty("content")
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public DataNotificationContent getContent() {
        return content;
    }

    public void setContent(DataNotificationContent content) {
        this.content = content;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataNotification dataNotification = (DataNotification) o;
        return Objects.equals(this.messageClass, dataNotification.messageClass) &&
                Objects.equals(this.messageDate, dataNotification.messageDate) &&
                Objects.equals(this.content, dataNotification.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageClass, messageDate, content);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DataNotification {\n");

        sb.append("    messageClass: ").append(toIndentedString(messageClass)).append("\n");
        sb.append("    messageDate: ").append(toIndentedString(messageDate)).append("\n");
        sb.append("    content: ").append(toIndentedString(content)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
