package fr.svom.xband.data.repository;

import fr.svom.xband.swagger.model.PacketApidIdDto;

import java.util.List;

/**
 * Interface for special jdbc methods.
 *
 * @author formica
 */
public interface IJdbcRepository {

    /**
     * Using the pass_id retrieves a list of APIDs and PacketIds which are found over
     * the all set of tar/dat files that have been ingested.
     *
     * @param station
     * @param orbit
     * @return List of PacketApidIdDto
     */
    List<PacketApidIdDto> findApidPacketByPassId(String station, Integer orbit);
}
