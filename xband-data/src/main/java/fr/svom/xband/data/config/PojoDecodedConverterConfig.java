package fr.svom.xband.data.config;

import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.PacketId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * @author formica
 * PojoDecodedConverterConfig class
 */
@Configuration
public class PojoDecodedConverterConfig {

    /**
     * @return MapperFactory
     */
    @Bean(name = "mapperDecodedFactory")
    public MapperFactory createOrikaMapperFactory() {
        MapperFactory mapperFact = new DefaultMapperFactory.Builder().build();
        this.initApidPacketMap(mapperFact);
        this.initPacketIdMap(mapperFact);
        this.initDataSourcePacketMap(mapperFact);
        return mapperFact;

    }

    /**
     * @param mapperFactory
     *        orika mapper factory.
     */
    protected void initApidPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(ApidPacket.class,
                ApidPacket.class)
                .exclude("instrument")
                .exclude("className")
                .exclude("packetDescription")
                .exclude("packetName")
        .exclude("category").byDefault()
        .register();
    }

    /**
     * @param mapperFactory
     *        orika mapper factory.
     */
    protected void initPacketIdMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(PacketId.class,
                PacketId.class)
        .byDefault().register();
    }

    /**
     * @param mapperFactory
     *        orika mapper factory.
     */
    protected void initDataSourcePacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(DataSourcePacket.class,
                DataSourcePacket.class)
        .exclude("hashId").byDefault().register();
    }

    /**
     * @param mapperFactory
     *        orika mapper factory.
     * @return mapperFactory
     */
    @Bean(name = "mapperDecoded")
    @Autowired
    public MapperFacade createMapperFacade(@Qualifier("mapperDecodedFactory"
            + "") MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }
}
