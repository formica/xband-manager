/**
 * 
 */
package fr.svom.xband.data.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author formica
 *
 */
public class XbandSQLException extends AbstractCdbServiceException {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -8552538724531679765L;

    /**
     * @param message
     *            the String
     */
    public XbandSQLException(String message) {
        super(message);
    }

    /**
     * @param message
     *            the String
     * @param err
     *            the Throwable
     */
    public XbandSQLException(String message, Throwable err) {
        super(message, err);
    }

    /**
     * Create using a throwable.
     *
     * @param cause
     */
    public XbandSQLException(Throwable cause) {
        super(cause);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "SQL " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.NOT_MODIFIED;
    }
}
