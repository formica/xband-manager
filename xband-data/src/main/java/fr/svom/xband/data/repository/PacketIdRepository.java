package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.PacketId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author etrigui
 *
 */
@Repository
public interface PacketIdRepository extends CrudRepository<PacketId, Integer> {

    /**
     * Get the packets ids from the apid value.
     * @param apid
     * @return List<PacketId>
     */
    @Query("SELECT p FROM PacketId p WHERE "
           + "p.apid.apidValue = (:apid)")
    List<PacketId> findPacketIdByApid(@Param("apid") Integer apid);
}
