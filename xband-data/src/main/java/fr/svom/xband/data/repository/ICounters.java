package fr.svom.xband.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ICounters {
    /**
     * @param lcfilename lc filename.
     * @return number of packets with bad header
     */
    @Query("SELECT COUNT(p.frameId) FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.lcfileName = (:lcfilename) "
           + "AND p.isFrameValid = false")
    Integer countBadHeader(@Param("lcfilename") String lcfilename);

    /**
     * @param lcfilename lc filename.
     * @return number of packets with unexpected Apid
     */
    @Query("SELECT COUNT(p.frameId) FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.lcfileName = (:lcfilename) "
           + "AND p.isFrameValid = false "
           + "AND p.isKnown = True")
    Integer countUnexpectedApid(@Param("lcfilename") String lcfilename);

    /**
     * @param lcfilename lc filename.
     * @return nb of packets with unexpeted pkt ID.
     */
    @Query("SELECT COUNT(p.frameId) FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.lcfileName = (:lcfilename) "
           + "AND p.isFrameValid = True "
           + "AND p.isKnown = False")
    Integer countUnexpectedPktId(@Param("lcfilename") String lcfilename);

    /**
     * @param lcfilename lc filename.
     * @return nb of identical packets.
     */
    @Query("SELECT COUNT(p.frameId) FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.lcfileName = (:lcfilename) "
           + "AND p.isDuplicate = True")
    Integer countIdenticalPkts(@Param("lcfilename") String lcfilename);

    /**
     * SecHeader Counter :(flag & 0x60) > 0; mask = 0x60 = 96.
     *
     * @param lcfilename lc filename.
     * @return nb packets with invalid secondary Header
     */
    @Query(value = "SELECT sum(case when (flag & 96)>0 then 1 else 0 end) as secHead "
                   + "FROM xband_packet p, l0c_datfile c "
                   + "WHERE c.lcfilename = :lcfilename "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countSecHeader(@Param("lcfilename") String lcfilename);

    /**
     * Decreasing PPS counter: (flag & 0x09 == 0x09).
     *
     * @param lcfilename lc filename.
     * @return nb of packets with decreasing pps.
     */
    @Query(value = "SELECT sum(case when (flag & 9)=9 then 1 else 0 end) as decPPS "
                   + "FROM xband_packet p, l0c_datfile c "
                   + "WHERE c.lcfilename = :lcfilename "
                   + "AND p.LCFILE_ID = c.LCFILE_ID "
                   + "AND p.pkt_order > 0",
            nativeQuery = true)
    Integer countDecPPS(@Param("lcfilename") String lcfilename);

    /**
     * Bad Duplicate pkt counter: (flag & 0x80) > 0); mask = 0x80=128.
     *
     * @param lcfilename lc filename.
     * @return nb of Bad duplicate packets.
     */
    @Query(value = "SELECT sum(case when (flag & 128)>0 then 1 else 0 end) as badDuplicate "
                   + "FROM xband_packet p, l0c_datfile c "
                   + "WHERE c.lcfilename = :lcfilename "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countBadDuplicate(@Param("lcfilename") String lcfilename);

    /**
     * TM gaps counters :  (flag & 0x03) == 0x03).
     *
     * @param lcfilename lc filename.
     * @return nb of packets with gaps.
     */
    @Query(value = "SELECT sum(case when (flag & 3)=3 then 1 else 0 end) as tmGap "
                   + "FROM xband_packet p, l0c_datfile c "
                   + "WHERE c.lcfilename = :lcfilename "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countTMGaps(@Param("lcfilename") String lcfilename);

    /**
     * @param tarfilename tar file name.
     * @return counters values
     */
    @Query(value = "SELECT sum(case when is_valid=false then 1 else 0 end) as badHeader "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countBadHeaderInTar(@Param("tarfilename") String tarfilename);

    /**
     * @param tarfilename tar file name.
     * @return counters values
     */
    @Query(value = "SELECT sum(case when is_valid=false and is_known=true then 1 else 0 end) "
                   + "as badApid "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countUexpectedApidFromTar(@Param("tarfilename") String tarfilename);

    /**
     * @param tarfilename tar file name.
     * @return counters values
     */
    @Query(value = "SELECT sum(case when is_valid=true and is_known=false then 1 else 0 end) "
                   + "as badPktId "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countUnexpecrPktIdFromTar(@Param("tarfilename") String tarfilename);

    /**
     * @param tarfilename tar file name.
     * @return counters values
     */
    @Query(value = "SELECT sum(case when is_duplicate=true then 1 else 0 end) as identicalPkt "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countIdenticalPktsFromTar(@Param("tarfilename") String tarfilename);

    /**
     * SecHeader Counter :(flag & 0x60) > 0; mask = 0x60 = 96.
     *
     * @param tarfilename tar file name.
     * @return nb packets with invalid secondary Header
     */
    @Query(value = "SELECT sum(case when (flag & 96)>0 then 1 else 0 end) as secHead "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countSecHeaderFromTar(@Param("tarfilename") String tarfilename);

    /**
     * Decreasing PPS counter: (flag & 0x09 == 0x09).
     *
     * @param tarfilename tar file name.
     * @return nb of packets with decreasing pps.
     */
    @Query(value = "SELECT sum(case when (flag & 9)=9 then 1 else 0 end) as decPPS "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID "
                   + "AND p.pkt_order > 0",
            nativeQuery = true)
    Integer countDecPPSFromTar(@Param("tarfilename") String tarfilename);

    /**
     * Bad Duplicate pkt counter: (flag & 0x80) > 0); mask = 0x80=128.
     *
     * @param tarfilename tar file name.
     * @return nb of Bad duplicate packets.
     */
    @Query(value = "SELECT sum(case when (flag & 128)>0 then 1 else 0 end) as badDuplicate "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countBadDuplicateFromTar(@Param("tarfilename") String tarfilename);

    /**
     * TM gaps counters :  (flag & 0x03) == 0x03).
     *
     * @param tarfilename tar file name.
     * @return nb of packets with gaps.
     */
    @Query(value = "SELECT sum(case when (flag & 3)=3 then 1 else 0 end) as tmGap "
                   + "FROM xband_packet p, l0c_datfile c, tar_file t "
                   + "WHERE t.tarname = :tarfilename "
                   + "AND c.tarname = t.tarname "
                   + "AND p.LCFILE_ID = c.LCFILE_ID ",
            nativeQuery = true)
    Integer countTMGapsFromTar(@Param("tarfilename") String tarfilename);

    // Example of query to implement.
    // select distinct xp.apid, xp.PACKET_ID
    // from xband_packet xp, l0c_datfile l0c, tar_file tf
    // where xp.lcfile_id=l0c.lcfile_id and tf.tarname=l0c.tarname and tf.tarname like '%';

}
