package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author etrigui
 * <p>
 * | APID | PACKET_ID |      PACKET_NAME      |   DATA_NOTIFICATION   |
 * |------|-----------|-----------------------|-----------------------|
 * | 1588 |   1,2     | TmXbPdpuAavPvt        |                       |
 * | 1620 |    30     | TmXbEclairsAavPvt     | TmXbAavPvt            |
 * | 1652 |    159    | TmXbMxtAavPvt         |                       |
 * |------|-----------|-----------------------|-----------------------|
 * | 1617 |    96     | TmXbEclairsSCRawData  | TmXbEclairsSCRawData  |
 * |------|-----------|-----------------------|-----------------------|
 * | 1649 |    225    | TmXbMxtScEvtOpen      | TmXbMxtScEvtOpen      |
 * | 1649 |    226    | TmXbMxtScEvtFilter    | TmXbMxtScEvtFilter    |
 * | 1649 |    227    | TmXbMxtScEvtSrcE      | TmXbMxtScEvtSrcE      |
 * | 1649 |    228    | TmXbMxtScEvtClsd      | TmXbMxtScEvtClsd      |
 * | 1649 |    229    | TmXbMxtScEvtUNKWN     | TmXbMxtScEvtUNKWN     |
 * |------|-----------|-----------------------|-----------------------|
 * | 1649 |    230    | TmXbMxtScFFOpen       |                       |
 * | 1649 |    231    | TmXbMxtScFFFilter     |                       |
 * | 1649 |    232    | TmXbMxtScFFSrcE       | TmXbMxtScEvtFullFrame |
 * | 1649 |    233    | TmXbMxtScFFClsd       |                       |
 * | 1649 |    234    | TmXbMxtScFFUNKWN      |                       |
 */
@Entity
@Table(name = "XBAND_APID", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class ApidPacket implements Serializable {

    /**
     * Primary key : the APID.
     */
    @Id
    @Column(name = "APID")
    private Integer apidValue = null; // decimal value

    /**
     * Packet name.
     */
    @Column(name = "NAME", unique = true, nullable = false, length = 100)
    private String name = null;

    /**
     * Packet description.
     */
    @Column(name = "DESCR", length = 100)
    private String description = null;

    /**
     * Class type to be used for parsing the byte stream.
     */
    @Column(name = "CLASS", length = 200)
    private String className = null;

    /**
     * Class type to be used for parsing the byte stream.
     */
    @Column(name = "DESTINATION", length = 200)
    private String destination = null;

    /**
     * The tid.
     */
    @Column(name = "TID")
    @EqualsAndHashCode.Exclude
    private Integer tid = -1; // 1 bit

    /**
     * The pid.
     */
    @Column(name = "PID")
    @EqualsAndHashCode.Exclude
    private Integer pid = -1; // 5 bits

    /**
     * The pcat.
     */
    @Column(name = "PCAT")
    @EqualsAndHashCode.Exclude
    private Integer pcat = -1; // 5 bits

    /**
     * The instrument.
     */
    @Column(name = "INSTRUMENT", length = 100)
    private String instrument = null;

    /**
     * The category.
     */
    @Column(name = "CATEGORY", length = 50)
    private String category = null;

    /**
     * The details. Comma separated list mentioning details on relevant content.
     * It can be for example: Attitude,Position,
     */
    @Column(name = "DETAILS", length = 4000)
    private String details = null;

    /**
     * Alternative name found in some documentation.
     * Ex: for apid name TmVhfVtFindChartR1 the alias is VT_VHF_FINDCHART_R_1
     */
    @Column(name = "ALIAS", length = 100)
    private String alias = null;

    /**
     * The number of expected packets.
     */
    @Column(name = "EXPECTED_PACKETS")
    private Integer expectedPackets = 0;

    /**
     * The priority.
     */
    @Column(name = "PRIORITY")
    private Integer priority = 0;

}
