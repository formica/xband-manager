package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.LcMetadata;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * A repository for LcMetadata.
 *
 * @author etrigui
 */
public interface LcMetadataRepository extends CrudRepository<LcMetadata, Long> {
    /**
     * Find tar file using the name.
     *
     * @param filename
     * @return LcMetadata.
     */
    @Query("SELECT p FROM LcMetadata p WHERE p.tarFile.tarName = (:filename)")
    LcMetadata findByTarfileName(@Param("filename") String filename);
}
