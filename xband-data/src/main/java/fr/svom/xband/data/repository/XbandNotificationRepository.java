package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.XbandNotification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Bookkeeping of notitications.
 * @author formica
 *
 */
@Repository
public interface XbandNotificationRepository extends CrudRepository<XbandNotification, Long> {
    // Empty.
}
