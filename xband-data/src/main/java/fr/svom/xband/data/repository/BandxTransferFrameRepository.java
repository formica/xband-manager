package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.BandxTransferFrame;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author etrigui
 */
@Repository
public interface BandxTransferFrameRepository extends PagingAndSortingRepository<BandxTransferFrame, Long>,
                                                      QuerydslPredicateExecutor<BandxTransferFrame> {

    @Override
    void delete(BandxTransferFrame arg0);

    @SuppressWarnings("unchecked")
    @Override
    BandxTransferFrame save(BandxTransferFrame arg0);

    /* (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#findById
     */
    @Override
    Optional<BandxTransferFrame> findById(Long id);

    @Modifying
    @Query("delete from BandxTransferFrame b where b.lcDatFile.lcfileId=(:lcfileid)")
    int deleteByLcDatFileId(@Param("lcfileid") Long lcfileid);

    /**
     * //TO-DO//: this method should also take a time window to limit the range.
     * @param apid apid value.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid)")
    List<BandxTransferFrame> findByApidPacket(@Param("apid") Integer apid);

    /**
     * @param obsid observation Identifier
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.obsidNum = (:obsid)")
    List<BandxTransferFrame> findByObsid(@Param("obsid") Integer obsid);

    /**
     * @param apid  apid value.
     * @param obsid observation Identifier
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + "AND p.obsidNum = (:obsid)")
    List<BandxTransferFrame> findByApidandObsid(@Param("apid") Integer apid,
                                                @Param("obsid") Integer obsid);

    /**
     * @param packetId packet identifier.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.packetId = (:packetId)")
    List<BandxTransferFrame> findByPacketId(@Param("packetId") Integer packetId);

    /**
     * @param apid     apid value.
     * @param packetId packet identifier.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + "AND p.packetId = (:packetId)")
    List<BandxTransferFrame> findByApidAndPacketId(@Param("apid") Integer apid,
                                                   @Param("packetId") Integer packetId);

    /**
     * @param filename lcfile Name.
     * @param packetId packet Identifier
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.lcDatFile.lcfileName = (:filename) "
           + "AND p.packetId = (:packetId) "
           + "ORDER BY p.pktOrder ASC")
    List<BandxTransferFrame> findByLcfilenameAndPacketId(@Param("filename") String filename,
                                                         @Param("packetId") Integer packetId);

    /**
     * @param tarfilename tar file Name.
     * @param packetId    packet Identifier
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.lcDatFile.tarFile.tarName= (:tarfilename) "
           + "AND p.packetId = (:packetId) "
           + "ORDER BY p.pktOrder ASC")
    List<BandxTransferFrame> findByTarNameAndPacketId(@Param("tarfilename") String tarfilename,
                                                      @Param("packetId") Integer packetId);

    /**
     * @param elapsedt1 the interval of time born 1.
     * @param elapsedt2 the interval of time born 2.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.packetTimeSec >= (:elapsedt1) "
           + "AND p.packetTimeSec <= (:elapsedt2) ")
    List<BandxTransferFrame> findByTime(@Param("elapsedt1") Long elapsedt1,
                                        @Param("elapsedt2") Long elapsedt2);

    /**
     * @param apid      the apid value.
     * @param elapsedt1 the interval of time born 1.
     * @param elapsedt2 the interval of time born 2.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + "AND p.packetTimeSec >= (:elapsedt1) "
           + "AND p.packetTimeSec <= (:elapsedt2) ")
    List<BandxTransferFrame> findByApidAndTime(@Param("apid") Integer apid, @Param("elapsedt1")
            Long elapsedt1, @Param("elapsedt2") Long elapsedt2);

    /**
     * @param apid      the apid value.
     * @param obsid     the Observation Identifier.
     * @param elapsedt1 the interval of time born 1.
     * @param elapsedt2 the interval of time born 2.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + "AND p.obsidNum = (:obsid) "
           + "AND p.packetTimeSec > (:elapsedt1) "
           + "AND p.packetTimeSec < (:elapsedt2) ")
    List<BandxTransferFrame> findByApidObsidAndTime(@Param("apid") Integer apid,
                                                    @Param("obsid") Integer obsid,
                                                    @Param("elapsedt1") Integer elapsedt1,
                                                    @Param("elapsedt2") Integer elapsedt2);

    /**
     * @param lcFilename the L0C file name.
     * @return List of BandxTransferFrame
     */
    @Query("SELECT p FROM BandxTransferFrame p JOIN FETCH p.dataSourcePacket ds "
            + " WHERE p.lcDatFile.lcfileName = (:lcFilename) ")
    List<BandxTransferFrame> findByL0cFilename(@Param("lcFilename") String lcFilename);

    /**
     * @param apid    Apid packet.
     * @param psc     Ccsds packet sequence count
     * @param pps     packet Time seconds.
     * @param timeMs  packet time microseconds.
     * @param pktid   packet id.
     * @param obsType obsid type.
     * @param obsNum  obsid num.
     * @param hashId  hash id.
     * @return List<BandxTransferFrame>.
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + "AND p.ccsdsCounter = (:psc) "
           + "AND p.packetTimeSec = (:pps) "
           + "AND p.timeMicroSec = (:timeMs) "
           + "AND p.packetId = (:pktid) "
           + "AND p.obsidType = (:obsType) AND p.obsidNum = (:obsNum) "
           + "AND p.dataSourcePacket.hashId = (:hashId)"
           + " ")
    List<BandxTransferFrame> findDuplicatePacket(@Param("apid") Integer apid,
                                                 @Param("psc") Integer psc, @Param("pps") Long pps,
                                                 @Param("timeMs") Integer timeMs, @Param("pktid") Integer pktid,
                                                 @Param("obsType") Integer obsType, @Param("obsNum") Integer obsNum,
                                                 @Param("hashId") String hashId
    );

    /**
     * @param apid apid.
     * @param psc  packet ccsds packet count.
     * @param pps  packet time second.
     * @return List<BandxTransferFrame> >
     */
    @Query("SELECT p FROM BandxTransferFrame p WHERE "
           + "p.apidPacket.apidValue = (:apid) "
           + " AND p.ccsdsCounter = (:psc)"
           + " AND p.packetTimeSec = (:pps) ")
    List<BandxTransferFrame> findPktswithSameIdentifier(@Param("apid") Integer apid,
                                                        @Param("psc") Integer psc, @Param("pps") Long pps);

    /**
     * @param lcfilename lc filename.
     * @return list of PacketId.
     */
    @Query("SELECT distinct p.packetId FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.lcfileName  = (:lcfilename)")
    List<Integer> pktIDList(@Param("lcfilename") String lcfilename);

    /**
     * @param tarfilename tar filename.
     * @return list of PacketId.
     */
    @Query("SELECT distinct p.packetId FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.tarFile.tarName= (:tarfilename)")
    List<Integer> pktIDListBytarFile(@Param("tarfilename") String tarfilename);

    /**
     * @param station the station name.
     * @param orbit the orbit name.
     * @return list of Apids.
     */
    @Query("SELECT distinct p.apidPacket.apidValue FROM BandxTransferFrame p "
           + "WHERE p.lcDatFile.station = (:station) and p.lcDatFile.orbit = (:orbit)")
    List<Integer> apidsListByPassId(@Param("station") String station, @Param("orbit") Integer orbit);

}
