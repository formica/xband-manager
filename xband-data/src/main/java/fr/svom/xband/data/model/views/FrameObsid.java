package fr.svom.xband.data.model.views;

import lombok.Data;
import org.hibernate.annotations.Subselect;
import org.springframework.data.annotation.Immutable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author formica
 */
@Entity
@Immutable
// This represents a view: the SELECT statement correspond to the following.
@Subselect("select distinct p.obsid, p.obsidnum as obsid_num, p.obsidtype as obsid_type,"
           + " p.apid, p.packet_id as packetid, l.lcfilename as lc_filename, "
           + " l.start_time_ts as start_time_ts,"
           + " l.station, l.orbit "
           + " from xband_packet p, l0c_datfile l "
           + " where p.lcfile_id=l.lcfile_id")
@Data
public class FrameObsid implements Serializable {

    /**
     * pk.
     */
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "obsid", column = @Column(name = "obsid")),
            @AttributeOverride(name = "lcFilename", column = @Column(name = "lc_filename"))
    })
    private FrameObsidPk pk;

    /**
     * The obsidnum.
     */
    @Column
    private Integer obsidNum = null;

    /**
     * The obsidtype.
     */
    @Column
    private Integer obsidType = null;

    /**
     * The apid.
     */
    @Column
    private Integer apid = null;

    /**
     * The packetId.
     */
    @Column
    private Integer packetid = null;

    /**
     * The station.
     */
    @Column
    private String station = null;

    /**
     * Start Time as Timestamp.
     */
    @Column
    private Timestamp startTimeTs = null;

    /**
     * The orbit.
     */
    @Column
    private Integer orbit = null;

    /**
     * Get the obsid from pk.
     * @return Long
     */
    public Long getObsid() {
        return pk.getObsid();
    }

    /**
     * Get the file name.
     * @return String
     */
    public String getLcFilename() {
        return pk.getLcFilename();
    }
}
