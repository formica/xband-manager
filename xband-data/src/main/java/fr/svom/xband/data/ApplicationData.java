package fr.svom.xband.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

/**
 * @author etrigui
 */
@SpringBootApplication
public class ApplicationData {

    /**
     * logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ApplicationData.class);


    /**
     * @param args arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(ApplicationData.class, args);
    }

    /**
     * Utility bean to dump spring beans.
     *
     * @param ctx Application Context.
     * @return command line runner
     */
    @Bean
    public CommandLineRunner cliRunner(ApplicationContext ctx) {
        return args -> {
            /** inspect beans */
            log.debug("Let's inspect the beans provided by Spring Boot:");
            final String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            // Print out the beans.
            for (final String beanName : beanNames) {
                log.debug(beanName);
            }

        };
    }
}
