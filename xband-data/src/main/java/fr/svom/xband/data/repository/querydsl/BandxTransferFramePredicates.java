/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.QBandxTransferFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class BandxTransferFramePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(BandxTransferFramePredicates.class);

    /**
     * Default Ctor.
     */
    private BandxTransferFramePredicates() {

    }

    /**
     * Where condition on frame ID.
     *
     * @param id
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Long id) {
        log.debug("hasId: argument {}", id);
        return QBandxTransferFrame.bandxTransferFrame.frameId.eq(id);
    }

    /**
     * Where condition on frame valid.
     *
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameValid(Boolean status) {
        log.debug("isFrameValid: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QBandxTransferFrame.bandxTransferFrame.isFrameValid.isTrue();
        }
        else {
            pred = QBandxTransferFrame.bandxTransferFrame.isFrameValid.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on apid.
     *
     * @param apid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(Integer apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QBandxTransferFrame.bandxTransferFrame.apidPacket.apidValue.eq(apid);
    }

    /**
     * Where condition on lcFilename.
     *
     * @param lcfilename
     *            the lc file
     * @return BooleanExpression
     */
    public static BooleanExpression hasLcfileName(String lcfilename) {
        log.debug("hasLcfileName: argument {}", lcfilename);
        return QBandxTransferFrame.bandxTransferFrame.lcDatFile.lcfileName.like("%" + lcfilename + "%");
    }

    /**
     * Where condition on category.
     *
     * @param category
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasCategoryLike(String category) {
        log.debug("hasCategoryLike: argument {}", category);
        return QBandxTransferFrame.bandxTransferFrame.apidPacket.category.like(category);
    }

    /**
     * Where condition on class.
     *
     * @param instrument
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasInstrumentLike(String instrument) {
        log.debug("hasInstrumentLike: argument {}", instrument);
        return QBandxTransferFrame.bandxTransferFrame.apidPacket.instrument.like(instrument);
    }


    /**
     * Where condition on obsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsid(Long obsid) {
        log.debug("hasObsid: argument {}", obsid);
        return QBandxTransferFrame.bandxTransferFrame.obsid.eq(obsid);
    }

    /**
     * Where condition on obsid type.
     *
     * @param obsidtype
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidType(Integer obsidtype) {
        log.debug("hasObsidType: argument {}", obsidtype);
        return QBandxTransferFrame.bandxTransferFrame.obsidType.eq(obsidtype);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsidnum
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidNum(Integer obsidnum) {
        log.debug("hasObsidNum: argument {}", obsidnum);
        return QBandxTransferFrame.bandxTransferFrame.obsidNum.eq(obsidnum);
    }

    /**
     * Where condition on tarfile.
     *
     * @param tarfile
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasTarFileNameLike(String tarfile) {
        log.debug("hasTarFileNameLike: argument {}", tarfile);
        return QBandxTransferFrame.bandxTransferFrame.lcDatFile.tarFile.tarName.like(tarfile);
    }

    /**
     * Where condition on orbitid.
     *
     * @param orbitid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasOrbitId(Integer orbitid) {
        log.debug("hasOrbitId: argument {}", orbitid);
        return QBandxTransferFrame.bandxTransferFrame.lcDatFile.orbit.eq(orbitid);
    }

    /**
     * Where condition on stationid.
     *
     * @param stationid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(String stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QBandxTransferFrame.bandxTransferFrame.lcDatFile.station.eq(stationid);
    }

    /**
     * Where condition on packet id and range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketIdXThan(String oper, String num) {
        log.debug("isPacketIdXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Integer pktid = Integer.valueOf(num);

        if ("<".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetId.loe(pktid);
        }
        else if (">".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetId.goe(pktid);
        }
        else if (":".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetId.eq(pktid);
        }
        return pred;
    }

    /**
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameDuplicate(Boolean status) {
        log.debug("isFrameDuplicate: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QBandxTransferFrame.bandxTransferFrame.isFrameDuplicate.isTrue();
        }
        else {
            pred = QBandxTransferFrame.bandxTransferFrame.isFrameDuplicate.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = Long.valueOf(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }

        if ("<".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetTimeSec.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetTimeSec.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QBandxTransferFrame.bandxTransferFrame.packetTimeSec.eq(tsec);
        }
        return pred;
    }

}
