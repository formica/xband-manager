/**
 * 
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.BandxServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("packetapidFiltering")
@Slf4j
public class PacketApidFiltering implements IFilteringCriteria {
    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws BandxServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                    + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = PacketApidPredicates
                        .hasId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("instrument".equals(key)) {
                // Filter based on instrument.
                final BooleanExpression islike = PacketApidPredicates
                        .isInstrumentLike(searchCriteria.getValue().toString());
                expressions.add(islike);
            }
            else if ("category".equals(key)) {
                // Filter based on category.
                final BooleanExpression islike = PacketApidPredicates
                        .isCategoryLike(searchCriteria.getValue().toString());
                expressions.add(islike);
            }
        }
        return expressions;
    }
}
