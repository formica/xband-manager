package fr.svom.xband.data.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import fr.svom.xband.data.exceptions.PayloadEncodingException;

/**
 * @author formica
 *
 */
public final class HashGenerator {

    /**
     *SHA_TYPE.
     */
    private static final String SHA_TYPE = "SHA-256";

    /**
     *FORMAT.
     */
    private static final Integer FORMAT = 0xff;

    /**
     * default ctor.
     */
    private HashGenerator() {
    }

    /**
     * @param message
     *            The message byte[] from which to generate md5.
     * @return The MD5 representation of the message string.
     * @throws PayloadEncodingException
     *             Exception encoding the message.
     */
    public static String shaJava(byte[] message)
            throws PayloadEncodingException {
        String digest = null;
        try {
            final MessageDigest md = MessageDigest.getInstance(SHA_TYPE);
            final byte[] hash = md.digest(message);
            // converting byte array to Hexadecimal String
            final StringBuilder sb = new StringBuilder(2 * hash.length);
            for (final byte b : hash) {
                sb.append(String.format("%02x", b & FORMAT));
            }
            digest = sb.toString();
        }
        catch (final NoSuchAlgorithmException ex) {
            throw new PayloadEncodingException(ex);
        }
        return digest;
    }

}
