package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * This entity contains the binary packet.
 * DataSource packet 1022 octets.
 *
 * @author formica
 */
@Entity
@Table(name = "XBAND_BINARY_PACKET", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
// Use lombok.
@Data
@Accessors(fluent = false)
public class DataSourcePacket {

    /**
     * The hash ID.
     */
    @Id
    @Column(name = "HASH_ID")
    private String hashId = null;

    /**
     * The lcfile ID.
     */
    @Column(name = "LCFILE_ID")
    // Exclude the field.
    @EqualsAndHashCode.Exclude
    private Long lcfileId = null;

    /**
     * It is represented as a LOB in the database, but stored in the table.
     * The binary packet (1022 bytes).
     */
    @Column(name = "PACKET", length = 1022)
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Size(max = 1022)
    // Exclude the field.
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private byte[] packet = null;

    /**
     * The binary hash is used for mapping associated frames.
     * List of associated frames.
     */
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "binaryHash")
    // Exclude the field.
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<BandxTransferFrame> frames = new HashSet<>(0);
}
