package fr.svom.xband.data.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author etrigui
 *
 */
public class FileDownloadException extends AbstractCdbServiceException {

    /**
     * @param string
     *            the String
     */
    public FileDownloadException(String string) {
        super(string);
    }

    /**
     * Add context information.
     * @param message
     * @param err
     */
    public FileDownloadException(String message, Throwable err) {
        super(message, err);
    }

    /**
     * Add context information.
     * @param err
     */
    public FileDownloadException(Throwable err) {
        super(err);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "File download error " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.BAD_REQUEST;
    }
}
