package fr.svom.xband.data.config;

/**
 * Configuration properties.
 * @author formica
 *
 */
public final class DatabasePropertyConfigurator {

    /**
     * Constant SCHEMA_NAME..
     */
    public static final String SCHEMA_NAME = "";

    /**
     * Default Ctor.
     */
    private DatabasePropertyConfigurator() {
        super();
    }
}
