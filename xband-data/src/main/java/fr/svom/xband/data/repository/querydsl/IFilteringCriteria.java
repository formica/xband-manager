/**
 * 
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.List;

/**
 * @author aformic
 *
 */
public interface IFilteringCriteria {

    /**
     * @param criteria
     *            the List<SearchCriteria>
     * @return List<BooleanExpression>
     */
    List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria);
}
