/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.QDataSourcePacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class DataSourcePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DataSourcePredicates.class);

    /**
     * Default Ctor.
     */
    private DataSourcePredicates() {

    }

    /**
     * Where condition on frame ID.
     *
     * @param id
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Long id) {
        log.debug("hasId: argument {}", id);
        return QDataSourcePacket.dataSourcePacket.frames.any().frameId.eq(id);
    }

    /**
     * Where condition on frame valid.
     *
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameValid(Boolean status) {
        log.debug("isFrameValid: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().isFrameValid.isTrue();
        }
        else {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().isFrameValid.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on apid.
     *
     * @param apid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(Integer apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QDataSourcePacket.dataSourcePacket.frames.any().apidPacket.apidValue.eq(apid);
    }

    /**
     * Where condition on category.
     *
     * @param category
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasCategoryLike(String category) {
        log.debug("hasCategoryLike: argument {}", category);
        return QDataSourcePacket.dataSourcePacket.frames.any().apidPacket.category.like(category);
    }

    /**
     * Where condition on class.
     *
     * @param instrument
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasInstrumentLike(String instrument) {
        log.debug("hasInstrumentLike: argument {}", instrument);
        return QDataSourcePacket.dataSourcePacket.frames.any().apidPacket.instrument.like(instrument);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsid(Long obsid) {
        log.debug("hasObsid: argument {}", obsid);
        return QDataSourcePacket.dataSourcePacket.frames.any().obsid.eq(obsid);
    }

    /**
     * Where condition on obsid type.
     *
     * @param obsidtype
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidType(Integer obsidtype) {
        log.debug("hasObsidType: argument {}", obsidtype);
        return QDataSourcePacket.dataSourcePacket.frames.any().obsidType.eq(obsidtype);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsidnum
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidNum(Integer obsidnum) {
        log.debug("hasObsidNum: argument {}", obsidnum);
        return QDataSourcePacket.dataSourcePacket.frames.any().obsidNum.eq(obsidnum);
    }

    /**
     * Where condition on tarfile.
     *
     * @param tarfile
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasTarFileNameLike(String tarfile) {
        log.debug("hasTarFileNameLike: argument {}", tarfile);
        return QDataSourcePacket.dataSourcePacket.frames.any().lcDatFile.tarFile.tarName.like(tarfile);
    }

    /**
     * Where condition on orbitid.
     *
     * @param orbitid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasOrbitId(Integer orbitid) {
        log.debug("hasOrbitId: argument {}", orbitid);
        return QDataSourcePacket.dataSourcePacket.frames.any().lcDatFile.orbit.eq(orbitid);
    }

    /**
     * Where condition on stationid.
     *
     * @param stationid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(String stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QDataSourcePacket.dataSourcePacket.frames.any().lcDatFile.station.eq(stationid);
    }

    /**
     * Where condition on packet id and range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketIdXThan(String oper, String num) {
        log.debug("isPacketIdXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Integer pktid = Integer.valueOf(num);

        if ("<".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetId.loe(pktid);
        }
        else if (">".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetId.goe(pktid);
        }
        else if (":".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetId.eq(pktid);
        }
        return pred;
    }

    /**
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameDuplicate(Boolean status) {
        log.debug("isFrameDuplicate: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().isFrameDuplicate.isTrue();
        }
        else {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().isFrameDuplicate.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = Long.valueOf(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }

        if ("<".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetTimeSec.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetTimeSec.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QDataSourcePacket.dataSourcePacket.frames.any().packetTimeSec.eq(tsec);
        }
        return pred;
    }

}
