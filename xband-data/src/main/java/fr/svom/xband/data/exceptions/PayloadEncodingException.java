package fr.svom.xband.data.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author formica
 *
 */

public class PayloadEncodingException extends AbstractCdbServiceException {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 1851049463533897275L;

    /**
     * @param string
     *            the String
     */
    public PayloadEncodingException(String string) {
        super(string);
    }

    /**
     * Add context information.
     * @param message
     * @param err
     */
    public PayloadEncodingException(String message, Throwable err) {
        super(message, err);
    }

    /**
     * Add context information.
     * @param err
     */
    public PayloadEncodingException(Throwable err) {
        super(err);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "Encoding error " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.BAD_REQUEST;
    }

}
