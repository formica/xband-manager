package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author etrigui
 */

@Entity
@Table(name = "L0C_DATFILE", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class LcDatFile implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The L0c file name
     * format: SVOM_L0C_%2c_%04X_%s_%06d_%5s_%02d_%1d.dat.
     */
    @Id
    @Column(name = "LCFILE_ID")
    @SequenceGenerator(name = "lc_seq", sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".LC_SEQUENCE")
    @GeneratedValue(generator = "lc_seq")
    @EqualsAndHashCode.Exclude
    private Long lcfileId = null;

    /**
     * The L0c file name
     * format: SVOM_L0C_%2c_%04X_%s_%06d_%5s_%02d_%1d.dat.
     */
    @Column(name = "LCFILENAME", unique = true)
    private String lcfileName = null;

    /**
     * l0C tar File.
     */
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "TARNAME")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private TarFile tarFile = null;

    /**
     * The L0c file path.
     */
    @Column(name = "LCFILEPATH")
    private String filePath = null;

    /**
     * The l0c Dat file size; maximum 2GB (2 * 1024 * 1024 packets)
     * xband packet size : 1022 bytes.
     */
    @Column(name = "FILESIZE")
    private Long fileSize = null;

    /**
     * boolean to describe if the Filename is conform.
     */
    @Column(name = "IS_NAME_CONFORM")
    @EqualsAndHashCode.Exclude
    private Boolean isFilenameConform = null;

    /**
     * Type of the data
     * ED (Engine Data) or SD (science data).
     */
    @Column(name = "TYPE")
    private String type = null;

    /**
     * hexadecimal integer value of the apid.
     */
    @Column(name = "APID_HEXA")
    private String apidHexa = null;

    /**
     * Start Time : Time of the first packet received in the L0c file
     * UTC format at 2nd precision format YYYYMMDDThhmmss.
     */
    @Column(name = "START_TIME")
    @EqualsAndHashCode.Exclude
    private String startTime = null;

    /**
     * End Time (optional field): Time of the last packet received in L0c file
     * UTC format at 2nd precision format YYYYMMDDThhmmss.
     */
    @Column(name = "END_TIME")
    @EqualsAndHashCode.Exclude
    private String endTime = null;

    /**
     * Start Time as Timestamp.
     */
    @Column(name = "START_TIME_TS")
    @EqualsAndHashCode.Exclude
    private Timestamp startTimeTs = null;

    /**
     * Start Time as Timestamp.
     */
    @Column(name = "END_TIME_TS")
    @EqualsAndHashCode.Exclude
    private Timestamp endTimeTs = null;

    /**
     * The Orbit: 6 digits integer.
     */
    @Column(name = "ORBIT")
    @EqualsAndHashCode.Exclude
    private Integer orbit = null;

    /**
     * The station: 5 letters string.
     */
    @Column(name = "STATION")
    @EqualsAndHashCode.Exclude
    private String station = null;

    /**
     * FIXME: define pass ID
     * The pass_id: orbit_station.
     */
    @Transient
    @EqualsAndHashCode.Exclude
    private String passId = null;

    /**
     * The version
     * Format: 2 digits + 1 char from a to z (00O-99z).
     */
    @Column(name = "VERSION")
    @EqualsAndHashCode.Exclude
    private String ver = null;

    /**
     * Index
     * format: 1 digit (1-&gt; 9).
     */
    @Column(name = "INDEX")
    @EqualsAndHashCode.Exclude
    private Integer index = null;

    /**
     * VCID: the priority associated to this file.
     * format: 2 digit.
     */
    @Column(name = "VCID")
    @EqualsAndHashCode.Exclude
    private Integer vcid = null;

    /**
     * The receptionTime.
     * time of the packet reception by the fsc. Units are milliseconds since epoch.
     */
    @Column(name = "RECEPTION_TIME")
    @EqualsAndHashCode.Exclude
    private Timestamp receptionTime = null;

    /**
     * Error : field (to be filled in post processing for ld generation step).
     * ERROR FLAG is initially equal to à 0 (1 byte = 0000000 )
     * ERRRO: 0_0_0_NBERRH1 _NBBADDUPLI_NBERRH2 _NBPPSDECR _NBPACKID
     * NBERRH1 = 1 if the nb of packets with invalid primary header is &gt;  0
     * NBBADDUPLI  if the nb of packets with bad duplicated is &gt;  0
     * NBERRH2     if the nb of packets with invalid secondary is &gt;  0
     * NBPPSDECR   if the nb of packets with pps decreasing is &gt;  0
     * NBPACKID    if the nb of packets with unknown packetid is &gt;  0.
     */
    @Column(name = "ERROR")
    @EqualsAndHashCode.Exclude
    private Integer error = null;

    /**
     * @return the passId
     */
    public String getPassId() {
        if (this.passId == null) {
            this.passId = String.format("%05d_%s", orbit, station);
        }
        return passId;
    }

    /**
     * Check time before persistence.
     *
     * @return
     */
    @PrePersist
    public void checktime() {
        if (receptionTime == null) {
            final Instant now = Instant.now();
            receptionTime = new Timestamp(now.toEpochMilli());
        }
    }
}
