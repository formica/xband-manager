package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.DataSourcePacket;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author etrigui
 */
@Repository
public interface DataSourceRepository
        extends PagingAndSortingRepository<DataSourcePacket, String>,
                QuerydslPredicateExecutor<DataSourcePacket> {

    /* (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(S)
     */
    @SuppressWarnings("unchecked")
    @Override
    DataSourcePacket save(DataSourcePacket arg0);

    /**
     *
     * @param lcfileid
     * @return number of deleted items
     */
    long deleteByLcfileId(Long lcfileid);
}
