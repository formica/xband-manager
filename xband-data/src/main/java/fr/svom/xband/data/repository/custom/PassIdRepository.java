package fr.svom.xband.data.repository.custom;

import fr.svom.xband.swagger.model.PassIdDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PassIdRepository {

    Page<PassIdDto> listPassIdInDatFiles(
            String apid, String station, Integer orbit, String start, String end, Pageable preq);
}
