/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.QTarFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class TarFilePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TarFilePredicates.class);

    /**
     * Default Ctor.
     */
    private TarFilePredicates() {

    }

    /**
     * Where condition on tar name.
     *
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasName(String name) {
        log.debug("hasName: argument {}", name);
        return QTarFile.tarFile.tarName.like("% " + name + "%");
    }

    /**
     * Where condition on tar name.
     *
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPath(String name) {
        log.debug("hasPath: argument {}", name);
        return QTarFile.tarFile.tarPath.like("%" + name + "%");
    }

    /**
     * Where condition on tar name valid.
     *
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isConform(Boolean status) {
        log.debug("isConform: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QTarFile.tarFile.isTarNameConform.isTrue();
        }
        else {
            pred = QTarFile.tarFile.isTarNameConform.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isStartTimeXThan(String oper, Timestamp num) {
        log.debug("isStartTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        //Timestamp tsec = num; // Coded as timestamp
        if ("<".equals(oper)) {
            pred = QTarFile.tarFile.startTimeTs.lt(num);
        }
        else if (">".equals(oper)) {
            pred = QTarFile.tarFile.startTimeTs.gt(num);
        }
        else if (":".equals(oper)) {
            pred = QTarFile.tarFile.startTimeTs.eq(num);
        }
        return pred;
    }

    /**
     * Where condition on apid.
     *
     * @param apid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(String apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QTarFile.tarFile.apidHexa.eq(apid);
    }

    /**
     * Where condition on orbitid.
     *
     * @param orbitid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasOrbitId(Integer orbitid) {
        log.debug("hasOrbitId: argument {}", orbitid);
        return QTarFile.tarFile.orbit.eq(orbitid);
    }

    /**
     * Where condition on stationid.
     *
     * @param stationid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(String stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QTarFile.tarFile.station.eq(stationid);
    }

}
