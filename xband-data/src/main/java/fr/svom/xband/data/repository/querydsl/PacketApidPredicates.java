/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.QApidPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class PacketApidPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PacketApidPredicates.class);

    /**
     * Default Ctor.
     */
    private PacketApidPredicates() {

    }

    /**
     * Where condition on APID.
     *
     * @param id
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Integer id) {
        log.debug("hasId: argument {}", id);
        return QApidPacket.apidPacket.apidValue.eq(id);
    }

    /**
     * Where condition on instrument.
     *
     * @param instrument
     *            the instrument
     * @return BooleanExpression
     */
    public static BooleanExpression isInstrumentLike(String instrument) {
        log.debug("isInstrumentLike: argument {}", instrument);
        return QApidPacket.apidPacket.instrument.like("%" + instrument + "%");
    }

    /**
     * Where condition on category.
     *
     * @param category
     *            the category
     * @return BooleanExpression
     */
    public static BooleanExpression isCategoryLike(String category) {
        log.debug("isCategoryLike: argument {}", category);
        return QApidPacket.apidPacket.category.like("%" + category + "%");
    }

}
