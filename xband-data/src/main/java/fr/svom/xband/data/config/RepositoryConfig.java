package fr.svom.xband.data.config;

import fr.svom.xband.data.repository.IJdbcRepository;
import fr.svom.xband.data.repository.JdbcRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Configuration for repositories.
 *
 * @author formica
 *
 */
@Configuration
@ComponentScan("fr.svom.xband.data.repository")
public class RepositoryConfig {

    /**
     * Configuration properties.
     */
    @Autowired
    private XbandProperties cprops;

    /**
     * @param mainDataSource
     *            the DataSource
     * @return IJdbcRepository
     */
    @Bean(name = "jdbcrepo")
    public IJdbcRepository payloadDefaultRepository(
            @Qualifier("dataSource") DataSource mainDataSource) {
        return new JdbcRepositoryImpl(mainDataSource);
    }
}
