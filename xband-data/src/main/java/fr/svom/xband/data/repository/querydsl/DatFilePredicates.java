/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.model.QLcDatFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class DatFilePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatFilePredicates.class);

    /**
     * Default Ctor.
     */
    private DatFilePredicates() {

    }

    /**
     * Where condition on tar name.
     *
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasName(String name) {
        log.debug("hasName: argument {}", name);
        return QLcDatFile.lcDatFile.lcfileName.like("% " + name + "%");
    }

    /**
     * Where condition on tar name.
     *
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasTarName(String name) {
        log.debug("hasTarName: argument {}", name);
        return QLcDatFile.lcDatFile.tarFile.tarName.like("%" + name + "%");
    }

    /**
     * Where condition on tar name valid.
     *
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isConform(Boolean status) {
        log.debug("isConform: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QLcDatFile.lcDatFile.isFilenameConform.isTrue();
        }
        else {
            pred = QLcDatFile.lcDatFile.isFilenameConform.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isStartTimeXThan(String oper, Timestamp num) {
        log.debug("isStartTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        //Timestamp tsec = num; // Coded as timestamp
        if ("<".equals(oper)) {
            pred = QLcDatFile.lcDatFile.startTimeTs.lt(num);
        }
        else if (">".equals(oper)) {
            pred = QLcDatFile.lcDatFile.startTimeTs.gt(num);
        }
        else if (":".equals(oper)) {
            pred = QLcDatFile.lcDatFile.startTimeTs.eq(num);
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isEndTimeXThan(String oper, Timestamp num) {
        log.debug("isEndTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        //Timestamp tsec = num; // Coded as timestamp
        if ("<".equals(oper)) {
            pred = QLcDatFile.lcDatFile.endTimeTs.lt(num);
        }
        else if (">".equals(oper)) {
            pred = QLcDatFile.lcDatFile.endTimeTs.gt(num);
        }
        else if (":".equals(oper)) {
            pred = QLcDatFile.lcDatFile.endTimeTs.eq(num);
        }
        return pred;
    }

    /**
     * Where condition on apid.
     *
     * @param apid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(String apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QLcDatFile.lcDatFile.apidHexa.eq(apid);
    }

    /**
     * Where condition on orbitid.
     *
     * @param orbitid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasOrbitId(Integer orbitid) {
        log.debug("hasOrbitId: argument {}", orbitid);
        return QLcDatFile.lcDatFile.orbit.eq(orbitid);
    }

    /**
     * Where condition on stationid.
     *
     * @param stationid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(String stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QLcDatFile.lcDatFile.station.eq(stationid);
    }

}
