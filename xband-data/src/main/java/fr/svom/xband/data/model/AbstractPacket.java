package fr.svom.xband.data.model;

import fr.svom.frame.data.model.BandxDataHeaderPacket;

/**
 * An abstract packet providing only the method for the mandatory insertion of an associated frame.
 * @see fr.svom.frame.data.model.BandxDataHeaderPacket
 * @author etrigui
 *
 */
public interface AbstractPacket {
    /**
     * @param frame the Bandx Data Header packet.
     */
    void setFrame(BandxDataHeaderPacket frame);
}
