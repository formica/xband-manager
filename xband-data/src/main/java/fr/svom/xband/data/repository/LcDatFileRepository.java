package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.LcDatFile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author etrigui
 */
@Repository
public interface LcDatFileRepository extends PagingAndSortingRepository<LcDatFile, Long>,
                                             QuerydslPredicateExecutor<LcDatFile> {

    /**
     * @param apid    apidHexa.
     * @param orbit   orbit.
     * @param station station.
     * @param index   index.
     * @return lcdatfile if found.
     */
    @Query("SELECT p FROM LcDatFile p WHERE "
           + "p.apidHexa = (:apid) "
           + "AND p.orbit = (:orbit) "
           + "AND p.station = (:station) "
           + "AND p.index = (:index)")
    List<LcDatFile> findByApidOrbitStationIndex(@Param("apid") String apid,
                                                @Param("orbit") Integer orbit, @Param("station") String station,
                                                @Param("index") Integer index);

    /**
     * @param lcfileName
     * @return lcdatfile.
     */
    Optional<LcDatFile> findByLcfileName(@Param("lcfileName") String lcfileName);

}
