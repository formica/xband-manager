package fr.svom.xband.data.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Properties for crest.
 *
 * @author formica
 *
 */
@Component
@ConfigurationProperties("xband")
public class XbandProperties {

    /**
     * The security.
     */
    private String security = "none";

    /**
     * The upload dir.
     */
    private String upload = "/tmp";

    /**
     * The upload dir.
     */
    private String download = "/tmp";

    /**
     * @return the security
     */
    public String getSecurity() {
        return security;
    }

    /**
     * @param security
     *            the security to set
     */
    public void setSecurity(String security) {
        this.security = security;
    }

    /**
     * Get the upload directory.
     * @return String
     */
    public String getUpload() {
        return upload;
    }

    /**
     * Set the upload directory.
     * @param upload
     */
    public void setUpload(String upload) {
        this.upload = upload;
    }

    /**
     * Get the download directory.
     * @return String
     */
    public String getDownload() {
        return download;
    }

    /**
     * Set the download directory.
     * @param download
     */
    public void setDownload(String download) {
        this.download = download;
    }
}
