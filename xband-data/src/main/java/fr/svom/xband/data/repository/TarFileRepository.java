package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.TarFile;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * A Repository for TarFile.
 *
 * @author etrigui
 */
@Repository
public interface TarFileRepository extends PagingAndSortingRepository<TarFile, String>,
                                           QuerydslPredicateExecutor<TarFile> {
    // No special methods.
}
