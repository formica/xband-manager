package fr.svom.xband.data.repository.custom;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import fr.svom.xband.data.model.QTarFile;
import fr.svom.xband.swagger.model.PassIdDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class PassIdRepositoryImpl implements PassIdRepository {

    /**
     * The entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * The datetime formatter.
     */
    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public Page<PassIdDto> listPassIdInDatFiles(String apid, String station, Integer orbit,
                                                String start, String end,
                                                Pageable preq) {

        JPQLQuery<Tuple> query = new JPAQuery<>(entityManager);

        //final JPAQuery<LcDatFile> query = new JPAQuery<LcDatFile>(entityManager).from(QLcDatFile.lcDatFile);
        long offset = preq.getOffset();
        long limit = preq.getPageSize();

        final BooleanBuilder where = new BooleanBuilder();
        if (apid != null) {
            where.and(QTarFile.tarFile.apidHexa.eq(apid));
            log.debug("Add where condition on apid hexa: {}", apid);
        }
        if (station != null) {
            where.and(QTarFile.tarFile.station.eq(station));
            log.debug("Add where condition on station: {}", station);
        }
        if (orbit != null) {
            where.and(QTarFile.tarFile.orbit.eq(orbit));
            log.debug("Add where condition on orbit: {}", orbit);
        }
        if (start != null) {
            LocalDateTime ldt = LocalDateTime.parse(start, dateTimeFormatter);
            Instant sinst = ldt.toInstant(ZoneOffset.UTC);
            Timestamp st = new Timestamp(sinst.toEpochMilli());
            where.and(QTarFile.tarFile.startTimeTs.goe(st));
        }
        if (end != null) {
            LocalDateTime ldt = LocalDateTime.parse(start, dateTimeFormatter);
            Instant sinst = ldt.toInstant(ZoneOffset.UTC);
            Timestamp et = new Timestamp(sinst.toEpochMilli());
            where.and(QTarFile.tarFile.startTimeTs.loe(et));
        }
        // The tar file name should be conform.
        where.and(QTarFile.tarFile.isTarNameConform.isTrue());
        // Execute the query.
        QTarFile tarFile = QTarFile.tarFile;
        query = query.select(tarFile.orbit, tarFile.station, tarFile.startTimeTs).from(tarFile)
                .where(where)
                .orderBy(QTarFile.tarFile.startTimeTs.asc())
                .distinct();
        // Set limits and offset.
        query.limit(limit);
        query.offset(offset);
        QueryResults<Tuple> result = query.fetchResults();
        long total = result.getTotal();
        List<Tuple> rows = result.getResults();

        List<PassIdDto> dtoList = rows.stream()
                .map(t -> new PassIdDto()
                        .orbit(t.get(0, Integer.class))
                        .station(t.get(1, String.class))
                        .startTimeTs(t.get(2, Timestamp.class).getTime()))
                .collect(Collectors.toList());

        return new PageImpl<>(dtoList, preq, total);
    }
}
