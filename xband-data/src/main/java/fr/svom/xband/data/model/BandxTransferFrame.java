package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * BandxTransferFrame class. Mapping the table XBAND_PACKET.
 *
 * @author etrigui
 */
@Entity
@Table(name = "XBAND_PACKET", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class BandxTransferFrame {

    /**
     * frameId. A generated field used as PK.
     */
    @Id
    @Column(name = "FRAME_ID", nullable = false)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "frame_seq"
    )
    @GenericGenerator(
            name = "frame_seq",
            strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence_name",
                            value = DatabasePropertyConfigurator.SCHEMA_NAME + ".FRAME_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "500"),
                    @Parameter(name = "optimizer", value = "hilo")
            }
    )
    @EqualsAndHashCode.Exclude
    private Long frameId = null;

    /**
     * lcDatFile. The FK to the data file.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LCFILE_ID", nullable = false)
    @EqualsAndHashCode.Exclude
    private LcDatFile lcDatFile = null;

    /**
     * ccsdsVersionNum (3 bits).
     */
    @Column(name = "CCSDS_VERSION")
    private Integer ccsdsVersion = null;

    /**
     * ccsdsType (1 bit).
     */
    @Column(name = "CCSDS_TYPE")
    private Integer ccsdsType = null;

    /**
     * ccsdsSecHeadFlag (1 bit).
     */
    @Column(name = "CCSDS_HEADFLAG")
    private Integer ccsdsHeadFlag = null;

    /**
     * apidPacket (11 bits).
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APID", nullable = false)
    private ApidPacket apidPacket = null;

    /**
     * ccsdsGFlag (2 bits).
     */
    @Column(name = "CCSDS_GFLAG")
    private Integer ccsdsGFlag = null;

    /**
     * ccsdsCounter (14 bits).
     */
    @Column(name = "CCSDS_COUNT")
    private Integer ccsdsCounter = 0;

    /**
     * ccsdsPlength (2 bytes).
     */
    @Column(name = "CCSDS_PLENGTH")
    private Integer ccsdsPlength = 0;

    /**
     * The packet Time in second : 4 bytes.
     */
    @Column(name = "TIMESECOND")
    private Long packetTimeSec = 0L;

    /**
     * The packet Time in microsecond : 3 bytes.
     */
    @Column(name = "TIMEMICRO")
    @EqualsAndHashCode.Exclude
    private Integer timeMicroSec = 0;

    /**
     * The packet Identifier : 1 byte.
     */
    @Column(name = "PACKET_ID", nullable = false)
    private Integer packetId = null;

    /**
     * The Obsid : 4 bytes.
     */
    @Column(name = "OBSID")
    private Long obsid = -1L;

    /**
     * The Oobsid - Type : 1 byte.
     */
    @Column(name = "OBSIDTYPE")
    private Integer obsidType = -1;

    /**
     * The Obsid - Number  : 3 byte.
     */
    @Column(name = "OBSIDNUM")
    private Integer obsidNum = -1;

    /**
     * The local Time Counter : 4 bytes.
     */
    @Column(name = "TIME_COUNTER")
    @EqualsAndHashCode.Exclude
    private Long localTimeCounter = 0L;

    /**
     * dataSourcePacket. The FK to the DataSourcePacket.
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "HASH_ID", referencedColumnName = "HASH_ID", insertable = false, updatable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private DataSourcePacket dataSourcePacket = null;

    /**
     * The packet hash.
     */
    @Column(name = "HASH_ID", length = 512)
    private String binaryHash = "none";

    /**
     * packet Order.
     * The order of reception of packets in the L0C file.
     */
    @Column(name = "PKT_ORDER")
    @EqualsAndHashCode.Exclude
    private Integer pktOrder = 0;

    /**
     * isFrameValid.
     * the packet is valid if primaryHeader is valid and apid is conform.
     * apid is conform if:
     * apid (hexa) in filename is egal to apid value (decimal) inside of the binary packet.
     * primary header is valid if: (1) 5 firsts bits =0 && (2) 11 bits of apid are known
     * && (3) 2 bits of Gflag à 11 && (4) packet data length =1015.
     * apid is known : see BX_APID_LIST in class StaticLists
     */
    @Column(name = "IS_VALID")
    @EqualsAndHashCode.Exclude
    private boolean isFrameValid = Boolean.FALSE;

    /**
     * isKnown.
     * APID known but not conform (to apid hexa in the l0c filename)
     */
    @Column(name = "IS_KNOWN")
    @EqualsAndHashCode.Exclude
    private boolean isKnown = Boolean.FALSE;

    /**
     * isDuplicate.
     * the packet is duplicated if exist another packet
     * with same header and same binary data.
     */
    @Column(name = "IS_DUPLICATE")
    @EqualsAndHashCode.Exclude
    private boolean isFrameDuplicate = Boolean.FALSE;

    /**
     * nbre of packets in the database having same identifier (APID+PSC+PPS).
     **/
//    @Column(name = "D_VERS")
//    private Integer               dVers;

    /**
     * flag.
     * will be filled in post processing
     * contains a byte indicator for packet quality (0 to 3 if all OK):
     * # 1st bit is 1 if packet has previous packet in DB (3 next bits are meaningful),
     * # 2nd bit is 1 for PSC gap, 3rd bit is 1 for PSC roll over in same second,
     * # 4th bit is 1 for PPS decreasing, 5th bit is always Ok=0 (known packetID),
     * # 6th bit is 0 for valid PPS microsecond, 7th bit is 0 for valid ObsID
     * # 7th bit : 0 if the packet is not a bad duplicate.
     */
    @Column(name = "FLAG")
    @EqualsAndHashCode.Exclude
    private Integer flag = null;

}
