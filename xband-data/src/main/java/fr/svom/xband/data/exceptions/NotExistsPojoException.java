/**
 * 
 */
package fr.svom.xband.data.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author formica
 *
 */
public class NotExistsPojoException extends AbstractCdbServiceException {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -8552538724531679765L;

    /**
     * @param string
     *            the String
     */
    public NotExistsPojoException(String string) {
        super(string);
    }

    /**
     * @param string
     *            the String
     * @param err
     *            the Throwable
     */
    public NotExistsPojoException(String string, Throwable err) {
        super(string, err);
    }

    /**
     * Create using a throwable.
     *
     * @param cause
     */
    public NotExistsPojoException(Throwable cause) {
        super(cause);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "Not Found " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.NOT_FOUND;
    }

}
