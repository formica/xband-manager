package fr.svom.xband.data.repository.custom;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import fr.svom.xband.data.model.views.FrameObsid;
import fr.svom.xband.data.model.views.QFrameObsid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author formica
 */
@Repository
@Slf4j
public class FrameObsidRepositoryImpl implements FrameObsidRepository {
    /**
     * The entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<FrameObsid> listObsidInDatFiles(Long obsid,
            Integer apid, String station, Integer orbit, Integer packetid, Pageable preq) {

        JPQLQuery<FrameObsid> query = new JPAQuery<>(entityManager);

        final BooleanBuilder where = new BooleanBuilder();
        if (obsid != null) {
            where.and(QFrameObsid.frameObsid.pk.obsid.eq(obsid));
            log.debug("Add where condition on obsid: {}", obsid);
        }
        if (apid != null) {
            where.and(QFrameObsid.frameObsid.apid.eq(apid));
            log.debug("Add where condition on apid: {}", apid);
        }
        if (station != null) {
            where.and(QFrameObsid.frameObsid.station.eq(station));
            log.debug("Add where condition on station: {}", station);
        }
        if (orbit != null) {
            where.and(QFrameObsid.frameObsid.orbit.eq(orbit));
            log.debug("Add where condition on orbit: {}", orbit);
        }
        if (packetid != null) {
            where.and(QFrameObsid.frameObsid.packetid.eq(packetid));
            log.debug("Add where condition on packetid: {}", packetid);
        }

        QFrameObsid frameobsid = QFrameObsid.frameObsid;
        query = query.select(frameobsid).from(frameobsid)
                .where(where)
                .distinct();
        query.limit(preq.getPageSize());
        query.offset(preq.getOffset());
        query.orderBy(frameobsid.pk.obsid.asc());
        QueryResults<FrameObsid> result = query.fetchResults();
        long total = result.getTotal();
        List<FrameObsid> rows = result.getResults();
        return new PageImpl<>(rows, preq, total);
    }
}
