package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author etrigui
 */
@Entity
@Table(name = "TAR_FILE", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class TarFile implements Serializable {

    /**
     * Primary key :
     * The L0c tar filename
     * format :SVOM_L0C_%2c_%04X_%s_%06d_%5s_%02d_%1d.tar.
     */
    @Id
    @Column(name = "TARNAME")
    private String tarName = null;
    /**
     * The L0c tar file path.
     */
    @Column(name = "TARPATH")
    private String tarPath = null;

    /**
     * The number of datFile in the l0c tar.
     */
    @Column(name = "NBDATFILE")
    private Integer nbDatfile = null;
    /**
     * The apid in Hexa value.
     */
    @Column(name = "APIDHEXA")
    private String apidHexa = null;

    /**
     * Start Time.
     */
    @Column(name = "START_TIME")
    private String startTime = null;

    /**
     * Start Time as Timestamp.
     */
    @Column(name = "START_TIME_TS")
    private Timestamp startTimeTs = null;

    /**
     * The Orbit: 6 digits integer.
     */
    @Column(name = "ORBIT")
    @EqualsAndHashCode.Exclude
    private Integer orbit = null;

    /**
     * The station: 5 letters string.
     */
    @Column(name = "STATION")
    @EqualsAndHashCode.Exclude
    private String station = null;

    /**
     * FIXME: define pass ID
     * The pass_id: orbit_station.
     */
    @Transient
    @EqualsAndHashCode.Exclude
    private String passId = null;

    /**
     * The receptionTime.
     * time of the packet reception by the fsc. Units are milliseconds since epoch.
     */
    @Column(name = "RECEPTION_TIME")
    @EqualsAndHashCode.Exclude
    private Timestamp receptionTime = null;

    /**
     * boolean to describe if the tar Filename is conform.
     */
    @Column(name = "IS_CONFORM")
    private Boolean isTarNameConform = null;

    /**
     * @return the passId
     */
    public String getPassId() {
        if (this.passId == null) {
            this.passId = String.format("%05d_%s", orbit, station);
        }
        return passId;
    }

    /**
     * Check time before persistence.
     *
     * @return
     */
    @PrePersist
    public void checktime() {
        if (receptionTime == null) {
            final Instant now = Instant.now();
            receptionTime = new Timestamp(now.toEpochMilli());
        }
    }

}
