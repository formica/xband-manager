package fr.svom.xband.data.repository;

import fr.svom.xband.data.model.ApidPacket;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author etrigui
 *
 */
@Repository
public interface ApidRepository extends PagingAndSortingRepository<ApidPacket, Integer>,
                                        QuerydslPredicateExecutor<ApidPacket> {

    /**
     * Find by name.
     *
     * @param name the string.
     * @return Optional<ApidPacket>
     */
    Optional<ApidPacket> findByName(@Param("name") String name);

    /**
     * @param station the station name.
     * @param orbit the orbit name.
     * @return list of Apids.
     */
    @Query("SELECT distinct a FROM ApidPacket a, BandxTransferFrame p "
           + "WHERE p.lcDatFile.station = (:station) and p.lcDatFile.orbit = (:orbit) "
           + " and a.apidValue=p.apidPacket.apidValue")
    List<ApidPacket> apidsListByPassId(@Param("station") String station, @Param("orbit") Integer orbit);
}
