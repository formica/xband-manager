package fr.svom.xband.data.model;


import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * XBAND Notification POJO. Used to log notifications that are sent via NATs.
 *
 * @author formica
 * 
 */
@Entity
@Table(name = "XBAND_NOTIFICATION", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
public class XbandNotification {

    /**
     * The ID.
     */
    @Id
    @SequenceGenerator(name = "notif_seq",
            sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".NOTIF_SEQUENCE")
    @GeneratedValue(generator = "notif_seq")
    @Column(name = "NOTIF_ID")
    private Long notifId = null;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime = null;

    /**
     * The size.
     */
    @Column(name = "NOTIF_SIZE")
    private Integer size = 0;

    /**
     * The message.
     */
    @Column(name = "NOTIF_MESSAGE", length = 1024)
    private String notifMessage = null;

    /**
     * The message.
     */
    @Column(name = "NOTIF_NAME", length = 30)
    private String notifName = null;

    /**
     * The format.
     */
    @Column(name = "NOTIF_FORMAT", length = 30)
    private String notifFormat = "ASCII";


    /**
     * The related frame id.
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LCFILE_ID")
    private LcDatFile l0cdatfile = null;

    /**
     * Set time when inserting.
     *
     * @return
     */
    @PrePersist
    public void prePersist() {
        // Init insertion time before storage.
        if (this.insertionTime == null) {
            // Set to now.
            final Instant now = Instant.now();
            final Timestamp nowts = new Timestamp(now.toEpochMilli());
            this.insertionTime = nowts;
        }
    }
}
