package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * This class should map the content of the XML file.
 *
 * @author etrigui
 */
@Entity
@Table(name = "L0C_METADATA", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class LcMetadata implements Serializable {

    /**
     * Primary key : generated.
     */
    @Id
    @Column(name = "METADATA_ID")
    @SequenceGenerator(name = "lcmeta_seq",
            sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".LCMETA_SEQUENCE")
    @GeneratedValue(generator = "lcmeta_seq")
    private Long metadataId = null;

    /**
     * l0c tar File.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TARNAME", nullable = false)
    @ToString.Exclude
    private TarFile tarFile = null;

    /**
     * boolean to describe if the xml Filename is conform.
     */
    @Column(name = "IS_CONFORM")
    private Boolean isXmlNameConform = null;
    /**
     * statelite.
     */
    @Column(name = "SATELLITE")
    private String satellite = null;

    /**
     * The production Date.
     */
    @Column(name = "PRODUCTION_DATE")
    private String productionDate = null;

    /**
     * The productor.
     */
    @Column(name = "PRODUCER")
    private String producer = null;

    /**
     * confidential Level.
     */
    @Column(name = "CONF_LEVEL")
    private String confidentialLevel = null;

    /**
     * confidential Visibility.
     */
    @Column(name = "CONF_VISIBILITY")
    private String confidentialVisibility = null;

    /**
     * instrument.
     */
    @Column(name = "INSTRUMENT")
    private String instrument = null;

    /**
     * vcid Number.
     */
    @Column(name = "VCID_NUM")
    private String vcidNumber = null;

    /**
     * vcid Name.
     */
    @Column(name = "VCID_NAME")
    private String vcidName = null;

    /**
     * apid value.
     */
    @Column(name = "APID")
    private String apid = null;

    /**
     * Time system.
     */
    @Column(name = "SYS_TIME")
    private String timeSystem = null;

    /**
     * origin Time.
     */
    @Column(name = "ORIGIN_TIME")
    private String originTime = null;

    /**
     * station Code.
     */
    @Column(name = "STATION_CODE")
    private String stationCode = null;

    /**
     * antenne Code.
     */
    @Column(name = "ANTENNA_CODE")
    private String antennaCode = null;

    /**
     * product Identifier.
     */
    @Column(name = "PRODUCT_ID")
    private String productId = null;

    /**
     * Comments.
     */
    @Column(name = "COMMENT")
    private String comment = null;

    /**
     * Level.
     */
    @Column(name = "LEVEL")
    private String level = null;

    /**
     * Type.
     */
    @Column(name = "TYPE")
    private String type = null;

    /**
     * product Version.
     */
    @Column(name = "PRODUCT_VERSION")
    private String productVersion = null;

    /**
     * software Version.
     */
    @Column(name = "SOFTWARE_VERSION")
    private String softwareVersion = null;

    /**
     * product MD5.
     */
    @Column(name = "PRODUCT_MD5")
    private String productMD5 = null;

    /**
     * total packet.
     */
    @Column(name = "TOTAL_PACKETS")
    private String totalPackets = null;

    /**
     * valid Packet.
     */
    @Column(name = "VALID_PACKETS")
    private String validPackets = null;

    /**
     * packet Length.
     */
    @Column(name = "PACKET_LENGTH")
    private String packetLength = null;

    /**
     * has Gaps.
     */
    @Column(name = "HAS_GAPS")
    private String hasGaps = null;

    /**
     * gaps Percent.
     */
    @Column(name = "GAPS_PERCENT")
    private String gapsPercent = null;

    /**
     * quality Level.
     */
    @Column(name = "QUALITY_LEVEL")
    private String qualityLevel = null;

    /**
     * organization.
     */
    @Column(name = "ORGANIZATION")
    private String organization = null;

    /**
     * email.
     */
    @Column(name = "EMAIL")
    private String email = null;

    /**
     * telephone.
     */
    @Column(name = "TEL")
    private String tel = null;

    /**
     * fax.
     */
    @Column(name = "FAX")
    private String fax = null;

    /**
     * adress.
     */
    @Column(name = "ADDRESS")
    private String address = null;

}
