/**
 * 
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.BandxServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("dataSourceFiltering")
public class DataSourceFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DataSourceFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws BandxServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                    + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("id".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = DataSourcePredicates
                        .hasId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("packettime".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression packetTimexthan = DataSourcePredicates
                        .isPacketTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(packetTimexthan);
            }
            else if ("isframevalid".equals(key)) {
                // Filter based on frame validity.
                final BooleanExpression isFrameValid = DataSourcePredicates
                        .isFrameValid(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameValid);
            }
            else if ("isframeduplicate".equals(key)) {
                // Filter based on frame is duplicate.
                final BooleanExpression isFrameDuplicate = DataSourcePredicates
                        .isFrameDuplicate(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameDuplicate);
            }
            else if ("station".equals(key)) {
                // Filter based on station.
                final BooleanExpression exp = DataSourcePredicates
                        .hasStationId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("orbit".equals(key)) {
                // Filter based on Orbit.
                final BooleanExpression exp = DataSourcePredicates
                        .hasOrbitId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = DataSourcePredicates
                        .hasPacketApid(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsid".equals(key)) {
                // Filter based on obsid Type.
                final BooleanExpression exp = DataSourcePredicates
                        .hasObsid(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidtype".equals(key)) {
                // Filter based on obsid Type.
                final BooleanExpression exp = DataSourcePredicates
                        .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidnum".equals(key)) {
                // Filter based on obsid Num.
                final BooleanExpression exp = DataSourcePredicates
                        .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("packetid".equals(key)) {
                // Filter based on obsid Num.
                final BooleanExpression exp = DataSourcePredicates
                        .isPacketIdXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("tarfilename".equals(key)) {
                // Filter based on tar file name.
                final BooleanExpression exp = DataSourcePredicates
                        .hasTarFileNameLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("category".equals(key)) {
                // Filter based on Category of the APID.
                final BooleanExpression exp = DataSourcePredicates
                        .hasCategoryLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("instrument".equals(key)) {
                // Filter based on Instrument of the APID.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasInstrumentLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        return expressions;
    }
}
