/**
 *
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.BandxServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("datFileFiltering")
public class DatFileFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatFileFiltering.class);

    /**
     * The datetime formatter.
     */
    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws BandxServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                      + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("name".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = DatFilePredicates
                        .hasName(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("tarname".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression path = DatFilePredicates
                        .hasTarName(searchCriteria.getValue().toString());
                expressions.add(path);
            }
            else if ("starttime".equals(key)) {
                // Filter based on packet time.
//                LocalDateTime ldt = LocalDateTime.parse(searchCriteria.getValue().toString(), dateTimeFormatter);
//                Instant sinst = ldt.toInstant(ZoneOffset.UTC);
                final BooleanExpression startTimexthan = DatFilePredicates
                        .isStartTimeXThan(searchCriteria.getOperation(),
                                new Timestamp(Long.parseLong(searchCriteria.getValue().toString())));
                expressions.add(startTimexthan);
            }
            else if ("endtime".equals(key)) {
                // Filter based on packet time.
//                LocalDateTime ldt = LocalDateTime.parse(searchCriteria.getValue().toString(), dateTimeFormatter);
//                Instant sinst = ldt.toInstant(ZoneOffset.UTC);
                final BooleanExpression endTimexthan = DatFilePredicates
                        .isEndTimeXThan(searchCriteria.getOperation(),
                                new Timestamp(Long.parseLong(searchCriteria.getValue().toString())));
                expressions.add(endTimexthan);
            }
            else if ("isconform".equals(key)) {
                // Filter based on frame validity.
                final BooleanExpression isConform = DatFilePredicates
                        .isConform(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isConform);
            }
            else if ("station".equals(key)) {
                // Filter based on station.
                final BooleanExpression exp = DatFilePredicates
                        .hasStationId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("orbit".equals(key)) {
                // Filter based on Orbit.
                final BooleanExpression exp = DatFilePredicates
                        .hasOrbitId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = DatFilePredicates
                        .hasPacketApid(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        return expressions;
    }
}
