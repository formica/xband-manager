package fr.svom.xband.data.config;

import fr.svom.xband.data.config.converters.DateToOffDateTimeConverter;
import fr.svom.xband.data.config.converters.TimestampToOffDateTimeConverter;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.DataSourcePacket;
import fr.svom.xband.data.model.PacketId;
import fr.svom.xband.swagger.model.BandxTransferFrameDto;
import fr.svom.xband.swagger.model.DataSourcePacketDto;
import fr.svom.xband.swagger.model.PacketApidDto;
import fr.svom.xband.swagger.model.PacketIdDto;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.bind.DatatypeConverter;

/**
 * Configuration for mapping.
 *
 * @author formica
 */
@Configuration
public class PojoDtoConverterConfig {


    /**
     * Create the mapper factory to generate DTOs from POJOs and vice versa.
     *
     * @return the mapper factory.
     */
    @Bean(name = "mapperFactory")
    public MapperFactory createOrikaMapperFactory() {
        // Create factory.
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        // Add converters.
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter(new DateToOffDateTimeConverter());
        converterFactory.registerConverter(new TimestampToOffDateTimeConverter());
        // Init specific mapping methods.
        this.initPacketIdMap(mapperFactory);
        this.initFrameMap(mapperFactory);
        this.initBinaryPacketMap(mapperFactory);
        this.initPacketApidMap(mapperFactory);
        return mapperFactory;
    }

    /**
     * Datasource packet converter.
     * @param mapperFactory
     */
    private void initBinaryPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(DataSourcePacket.class, DataSourcePacketDto.class)
                .exclude("frames")
                .byDefault()
                .customize(new CustomMapper<DataSourcePacket, DataSourcePacketDto>() {
                    @Override
                    // Map the entity to dto.
                    public void mapAtoB(DataSourcePacket a, DataSourcePacketDto b, MappingContext context) {
                        if (a.getPacket() != null) {
                            b.setPacket(DatatypeConverter.printHexBinary(a.getPacket()));
                        }
                    }
                    @Override
                    // Map the dto to entity.
                    public void mapBtoA(DataSourcePacketDto a, DataSourcePacket b, MappingContext context) {
                        // ignore the millisec fields.
                    }
                }).register();
    }

    /**
     * Entity to Dto for PacketId.
     *
     * @param mapperFactory the MapperFactory
     */
    protected void initPacketIdMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(PacketId.class, PacketIdDto.class).byDefault().register();
    }

    /**
     * Entity to Dto for BandxTransferFrame.
     *
     * @param mapperFactory the MapperFactory
     */
    protected void initFrameMap(MapperFactory mapperFactory) {
        // Define mapping for specific fields: orbit and station.
        mapperFactory.classMap(BandxTransferFrame.class, BandxTransferFrameDto.class)
                .field("lcDatFile.orbit",
                        "orbit")
                .field("lcDatFile.station",
                        "station")
                .exclude("dataSourcePacket")
                .exclude("lcDatFile")
                .byDefault().register();
    }

    /**
     * Entity to Dto for ApidPacket.
     *
     * @param mapperFactory the MapperFactory
     */
    protected void initPacketApidMap(MapperFactory mapperFactory) {
        // Define mapping for ApidPacket.
        mapperFactory.classMap(ApidPacket.class, PacketApidDto.class)
                .byDefault().register();
    }

    /**
     * The Mapper facade that will be used by the client classes.
     *
     * @param mapperFactory the MapperFactory
     * @return MapperFacade
     */
    @Bean(name = "mapper")
    @Autowired
    public MapperFacade createMapperFacade(@Qualifier("mapperFactory") MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }
}
