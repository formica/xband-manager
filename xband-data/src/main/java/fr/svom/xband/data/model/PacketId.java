package fr.svom.xband.data.model;

import fr.svom.xband.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Packet Identifier Class.
 *
 * @author etrigui
 */
@Entity
@Table(name = "XBAND_PACKET_ID", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class PacketId implements Serializable {

    /**
     * Primary key : the packet_id.
     */
    @Id
    @Column(name = "PACKET_ID")
    private Integer pktId = null; //1 byte

    /**
     * apidValue.
     */
    @ManyToOne
    @JoinColumn(name = "APID", nullable = false)
    private ApidPacket apid = null;

    /**
     * The packet name.
     */
    @Column(name = "PACKET_NAME")
    private String packetName = null;

    /**
     * The packet notification.
     */
    @Column(name = "PACKET_NOTIFICATION")
    private String packetNotification = null;

}
