package fr.svom.xband.data.serialization;

import fr.svom.frame.data.model.BandxDataHeaderPacket;
import fr.svom.frame.data.model.BandxPacket;
import fr.svom.frame.data.model.CcsdsPacket;
import fr.svom.frame.data.service.BandXDecoding;
import fr.svom.xband.data.exceptions.BandxServiceException;
import fr.svom.xband.data.model.ApidPacket;
import fr.svom.xband.data.model.BandxTransferFrame;
import fr.svom.xband.data.model.LcDatFile;
import fr.svom.xband.data.model.TarFile;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author etrigui
 */
@Service
@Slf4j
public class BandxFrameDeserializer {
    /**
     * BandXDecoding (header-packet-decoding library).
     */
    @Autowired
    private BandXDecoding bxDecoderBean;
    /**
     * MapperFacade.
     */
    @Autowired
    @Qualifier("mapperDecoded")
    private MapperFacade mapper;
    /**
     * The datetime formatter.
     */
    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    /**
     * The pattern of L0C DAT file.
     */
    private static final String L0CDAT_REGEXP =
            "SVOM_L0C_([A-Z]+)?(_\\d{2})?_(\\d{4})_(\\w{15})?(_\\w{15})?_(\\d+)_(\\w+)_"
            + "(\\d+)_(\\d+).(dat|DAT)";
    /**
     * The pattern of L0C TAR file: ED_APID_STARTDATE_ORB_STATION_VER.tar .
     */
    private static final String L0CTAR_REGEXP = "SVOM_L0C_([A-Z]+)_(\\d{4})_(\\w{15})_(\\d+)_(\\w+)(_\\d+)?"
                                                + ".(tar|TAR)";

    /**
     * The pattern of L0C XML file: ED_APID_STARTDATE_ORB_STATION_VER.xml .
     * SVOM_L0C_ED_0654_20210131T084346_000511_HK00M.xml
     */
    private static final String XML_REGEXP = "SVOM_([A-Z0-9]+)_([A-Z]+)_(\\d+)_(\\w+)_(\\d+)_(\\w+)"
                                                + ".(xml|XML)";


    /**
     * Default Ctor.
     */
    public BandxFrameDeserializer() {
        super();
    }

    /**
     * @param framedata raw binary data.
     * @return BandxTransferFrame
     * @throws BandxServiceException exception.
     */
    public BandxTransferFrame deserialize(byte[] framedata)
            throws BandxServiceException {

        BandxTransferFrame frame = new BandxTransferFrame();

        log.debug("Deserialize byte array of length {} ", framedata.length);

        BandxPacket pkt = bxDecoderBean.binaryDataDecoder(framedata);
        log.debug("decoded bandx packet:{} ", pkt);

//        CcsdsPacket ccsds = mapper.map(pkt.getCcsdsPacket(), CcsdsPacket.class);
        CcsdsPacket ccsds = pkt.getCcsdsPacket();
        log.debug("ccsds extracted: {}", ccsds);
        frame.setCcsdsVersion(ccsds.getCcsdsVersionNum());
        frame.setCcsdsType(ccsds.getCcsdsType());
        frame.setCcsdsHeadFlag(ccsds.getCcsdsSecHeadFlag());
        frame.setCcsdsGFlag(ccsds.getCcsdsGFlag());
        frame.setCcsdsCounter(ccsds.getCcsdsCounter());
        frame.setCcsdsPlength(ccsds.getCcsdsPlength());

        ApidPacket apidpkt = mapper.map(pkt.getApidPacket(), ApidPacket.class);
        log.debug("apid extracted: {}", apidpkt);
        frame.setApidPacket(apidpkt);

        BandxDataHeaderPacket dataheader = mapper.map(pkt.getBandxDataHeaderPacket(),
                BandxDataHeaderPacket.class);
        log.debug("data header packet extracted:{} ", dataheader);
        frame.setPacketTimeSec(dataheader.getPacketTimeSec());
        frame.setTimeMicroSec(dataheader.getTimeMicroSec());
        frame.setLocalTimeCounter(dataheader.getLocalTimeCounter());

        // Set the packet ID.
        frame.setPacketId(dataheader.getPacketID());

        Long obsid = dataheader.getPacketObsid();
        log.debug("obsid (4 bytes):{} ", obsid);
        frame.setObsidType(dataheader.getObsidType());
        frame.setObsidNum(dataheader.getObsidNum());
        frame.setObsid(obsid);
//        DataSourcePacket bxds = mapper.map(pkt.getDataSourcePacket(),
//                DataSourcePacket.class);
//        log.debug("data source packet extracted:{} ", bxds);
//        frame.setDataSourcePacket(bxds);

        return frame;
    }

    /**
     * @param filename L0C file name.
     * @return LcDatFile
     */
    public LcDatFile fileNameDecoderRegexp(String filename) {
        Pattern pattern = Pattern.compile(L0CDAT_REGEXP);
        Matcher matcher = pattern.matcher(filename);
        LcDatFile lcDatFile = new LcDatFile();
        lcDatFile.setLcfileName(filename);
        while (matcher.find()) {
            int i = 1;
            lcDatFile.setType(matcher.group(i));
            i++;
            if (matcher.groupCount() > 8 && matcher.group(i) != null) {
                String vcid = matcher.group(i).substring(1);
                lcDatFile.setVcid(Integer.valueOf(vcid));
            }
            i++;
            lcDatFile.setApidHexa(matcher.group(i));
            i++;
            lcDatFile.setStartTime(matcher.group(i));
            i++;
            if (matcher.groupCount() > 7 && matcher.group(i) != null) {
                String endtime = matcher.group(i).substring(1);
                lcDatFile.setEndTime(endtime);
            }
            i++;
            lcDatFile.setOrbit(Integer.valueOf(matcher.group(i)));
            i++;
            lcDatFile.setStation(matcher.group(i));
            i++;
            lcDatFile.setVer(matcher.group(i));
            i++;
            lcDatFile.setIndex(Integer.valueOf(matcher.group(i)));
        }
        return lcDatFile;
    }

    /**
     * @param tarfile the input Tarfile.
     * @return Tarfile
     */
    public TarFile tarNameDecoderRegexp(TarFile tarfile) {
        Pattern pattern = Pattern.compile(L0CTAR_REGEXP);
        Matcher matcher = pattern.matcher(tarfile.getTarName());
        while (matcher.find()) {
            tarfile.setApidHexa(matcher.group(2));
            String st = matcher.group(3);
            tarfile.setStartTime(st);
            LocalDateTime ldt = LocalDateTime.parse(st, dateTimeFormatter);
            Instant sinst = ldt.toInstant(ZoneOffset.UTC);
            tarfile.setStartTimeTs(Timestamp.from(sinst));
            Integer orbit = Integer.valueOf(matcher.group(4));
            String station = matcher.group(5);
            tarfile.setOrbit(orbit);
            tarfile.setStation(station);
        }
        return tarfile;
    }

    /**
     * @param tarfileName Tar file name. format: "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar".
     * @return String
     * The apid hexa value.
     */
    public String tarNameDecodeApid(String tarfileName) {
        final String[] fields = tarfileName.split("_");
        return fields[3];
    }

    /**
     * @param tarfileName Tar file name. format: "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001.tar".
     * @return String
     * The StartTime value.
     */
    public String tarNameDecodeStartTime(String tarfileName) {
        final String[] fields = tarfileName.split("_");
        return fields[4];
    }

    /**
     * @param filename L0c file name.
     * @return boolean
     * true if the L0c (.dat) filename is conform.
     */
    public boolean isDatFileNameCoform(String filename) {
        /**
         * example of dat filename (sans End Time):
         * "SVOM_L0C_ED_0654_20170207T160200_000001_KR000_001_1.dat"
         **/
//        final String rexp = "SVOM_([0-9a-zA-Z]{3})_(ED|SD)_(\\w{4})_"
//                            + "([0-9]{8}T[0-9]{6})(_?)"
//                            + "(([0-9]{8}T[0-9]{6})?)_([0-9]{6})_([0-9a-zA-Z]{5})_([0-9][0-9][0-9a-z])_([1-9])"
//                            + ".(dat|DAT)";
        final Pattern pattern = Pattern.compile(L0CDAT_REGEXP);
        final Matcher matcher = pattern.matcher(filename);
        return matcher.matches();
    }

    /**
     * @param filename XML filename
     * @return boolean
     * true if the xml filename is conform.
     */
    public boolean isXMLFileNameCoform(String filename) {
        /**
         * example of xml filename (sans End Time):
         * "SVOM_LOC_ED_0654_20170207T160200_000001_KR000_001.xml"
         **/
//        final String rexp = "SVOM_([0-9a-zA-Z]{3})_(ED|SD)_[0-9abcdefABCDEF]{4}_[0-9]{8}T[0-9]{6}"
//                            + "(_[0-9]{8}T[0-9]{6})*_[0-9]{6}_[0-9a-zA-Z]{5}_"
//                            + "[0-9][0-9][0-9a-z].(xml|XML)";
        final Pattern pattern = Pattern.compile(XML_REGEXP);
        final Matcher matcher = pattern.matcher(filename);
        return matcher.matches();

    }

    /**
     * @param filename the TAR file name.
     * @return boolean
     * true if the tar archive name is conform.
     */
    public boolean isTarFileNameCoform(String filename) {
        /**
         * example of tar filename (without End Time):
         * "SVOM_LOC_ED_0654_20170207T160200_000001_KR000_001.tar"
         **/
//        final String rexp = "SVOM_([0-9a-zA-Z]{3})_(ED|SD)_[0-9abcdefABCDEF]{4}_[0-9]{8}T[0-9]{6}"
//                            + "(_[0-9]{8}T[0-9]{6})*_[0-9]{6}_[0-9a-zA-Z]{5}_"
//                            + "[0-9][0-9][0-9a-z].(tar|TAR)";

        final Pattern pattern = Pattern.compile(L0CTAR_REGEXP);
        final Matcher matcher = pattern.matcher(filename);
        return matcher.matches();
    }

}
