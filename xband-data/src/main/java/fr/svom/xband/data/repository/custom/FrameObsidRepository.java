package fr.svom.xband.data.repository.custom;

import fr.svom.xband.data.model.views.FrameObsid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author formica
 */
public interface FrameObsidRepository {
    /**
     * @param obsid
     * @param apid
     * @param station
     * @param orbit
     * @param packetid
     * @param preq
     * @return Page of FrameObsid
     */
    Page<FrameObsid> listObsidInDatFiles(Long obsid,
            Integer apid, String station, Integer orbit, Integer packetid, Pageable preq);

}
