package fr.svom.xband.data.model.views;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class FrameObsidPk implements Serializable {
    /**
     * The obsid.
     */
    @Column
    private Long obsid = null;

    /**
     * The lcfilename.
     */
    @Column
    private String lcFilename = null;
}
