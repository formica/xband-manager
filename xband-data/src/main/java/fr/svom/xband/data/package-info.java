
/**
 * This is the base package for all classes handling the data model.
 * <p>
 * The package contains:
 * <ul>
 * <li>Entities mapped into DB tables.
 * <li>Repositories for communication with the DB and dealing with entities (save, select or update).
 * <li>Configuration classes for spring.
 * <li>Helper classes for parsing and hash generation.
 * <li>Classes for deserialization.
 * </ul>
 * General description of the packages.<br><br>
 * <pre>
 | package    | description                                 |
 |------------|---------------------------------------------|
 | model      | Contains entities, which are typically      |
 |            | transferred in the REST API after a mapping |
 |            | into a DTO                                  |
 |------------|---------------------------------------------|
 | repository | Contains the repositories, where the sql    |
 |            | methods are defined                         |
 |------------|---------------------------------------------|
 | config     | Classes for spring configuration            |
 |------------|---------------------------------------------|
 | exceptions | Classes for exceptions                      |
 |------------|---------------------------------------------|
 </pre>
 *
 * @see fr.svom.xband.data.model
 * @see fr.svom.xband.data.repository
 * @see fr.svom.xband.data.config
 * @see fr.svom.xband.data.exceptions
 * @author etrigui, andrea.formica@cern.ch
 *
 */
package fr.svom.xband.data;

