package fr.svom.xband.data.config;

import fr.svom.frame.data.service.BandXDecoding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * @author formica
 * BandxConverterConfig class.
 */
@Configuration
public class BandxConverterConfig {

    /**
     * @return BandXDecoding
     */
    @Bean(name = "bandxDecoderBean")
    public BandXDecoding createDecoder() {
        return new BandXDecoding();
    }

    /**
     * A Formatter for dates.
     * @return DateTimeFormatter
     */
    @Bean(name = "dateTimeFormatter")
    public DateTimeFormatter getDateTimeFormatter() {
        return new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                // date/time
                .appendPattern("yyyyMMdd'T'HHmmss")
                // optional fraction of seconds (from 0 to 9 digits)
                // create formatter
                .toFormatter();
    }
}
