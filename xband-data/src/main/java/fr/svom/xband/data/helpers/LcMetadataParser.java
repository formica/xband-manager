package fr.svom.xband.data.helpers;

import fr.svom.xband.data.model.LcMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author etrigui
 * LC meta-Data parser class.
 */

@Component
@Slf4j
public class LcMetadataParser {
    
    /**
     * Default ctor.
     */
    public LcMetadataParser() {
        super();
    }

    /**
     * Ctor with input file.
     *
     * @param file XML file containing l0c meta-data.
     */
    public LcMetadataParser(File file) {
    }

    /**
     * Read the list of dat files from the xml.
     * @param file the XML file
     * @return List<String>
     */
    public List<String> getDatfileList(File file) {
        List<String> datnamelist = new ArrayList<>();
        try {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder dBuilder = dbf.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            log.debug("Get datfile list: {}", doc.getDocumentElement().getNodeName());
            if (doc.getElementsByTagName("DatFile").getLength() > 0) {
                NodeList list = doc.getElementsByTagName("DatFile");
                for (int i = 0; i < list.getLength(); i++) {
                    Node node = list.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        String fname = element.getTextContent();
                        log.debug("Found dat file: {}", fname);
                        datnamelist.add(fname);
                    }
                }
            }
        }
        catch (SAXException | IOException | ParserConfigurationException e) {
            log.error("exception {}", e);
        }
        return datnamelist;
    }

    /**
     * @param file XML file containing l0c meta-data to parse.
     * @return LcMetadata.
     */
    public LcMetadata fillLcMetadata(File file) {
        LcMetadata lcMetadata = new LcMetadata();

        try {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder dBuilder = dbf.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            log.debug("Root: {}", doc.getDocumentElement().getNodeName());
            lcMetadata.setSatellite("SVOM");
            lcMetadata.setProductionDate("20211216T100000");
            lcMetadata.setProducer("FSC");
            if (doc.getElementsByTagName("Type").getLength() > 0) {
                NodeList typelist = doc.getElementsByTagName("Type");
                lcMetadata.setType(typelist.item(0).getTextContent());
            }

            if (doc.getElementsByTagName("DatFile").getLength() > 0) {
                NodeList list = doc.getElementsByTagName("DatFile");
                for (int i = 0; i < list.getLength(); i++) {
                    Node node = list.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        String fname = element.getTextContent();
                        log.info("Found dat file: {}", fname);
                    }
                }
                lcMetadata.setComment("NDAT=" + list.getLength());
            }

            /** get satellite meta-data */
//            if (doc.getElementsByTagName("SATELLITE").getLength() > 0) {
//                String satellite = doc.getElementsByTagName("SATELLITE").item(0).getNodeName();
//                lcMetadata.setSatellite(satellite);
//            }

            /*            *//** get production Date meta-data *//*
            final String pDate = doc.getElementsByTagName("PRODUCTION_DATE").item(0).getNodeName();
            lcMetadata.setProductionDate(pDate);

             *//** get productor meta-data *//*
            final String productor = doc.getElementsByTagName("PRODUCER").item(0).getNodeName();
            lcMetadata.setProductor(productor);

              *//** get confidential Level meta-data *//*
            final String confLevel = doc.getElementsByTagName("LEVEL").item(0).getNodeName();
            lcMetadata.setConfidentialyLevel(confLevel);

               *//** get confidential Visibility meta-data *//*
            final String confVis = doc.getElementsByTagName("VISIBILITY").item(0).getNodeName();
            lcMetadata.setConfidentialyVisibility(confVis);

                *//** get instrument meta-data *//*
            final String instrument = doc.getElementsByTagName("Instrument").item(0).getNodeName();
            lcMetadata.setInstrument(instrument);

                 *//** get VCID Number meta-data *//*
            final String vcidNumber = doc.getElementsByTagName("VCID_Number").item(0).getNodeName();
            lcMetadata.setVcidNumber(vcidNumber);

                  *//** get VCID Name meta-data *//*
            final String vcidName = doc.getElementsByTagName("VCID_Name").item(0).getNodeName();
            lcMetadata.setVcidName(vcidName);

                   *//** get apid meta-data *//*
            final String apid = doc.getElementsByTagName("APID").item(0).getNodeName();
            lcMetadata.setApid(apid);

                    *//** get time System meta-data *//*
            final String timeSystem = doc.getElementsByTagName("TimeSystem").item(0).getNodeName();
            lcMetadata.setTimeSystem(timeSystem);

                     *//** get origin Time meta-data  *//*
            final String originTime = doc.getElementsByTagName("OriginTime").item(0).getNodeName();
            lcMetadata.setOriginTime(originTime);

                      *//** get station Code meta-data  *//*
            final String stCode = doc.getElementsByTagName("StationCode").item(0).getNodeName();
            lcMetadata.setStationCode(stCode);

                       *//** get antenne Code meta-data  *//*
            final String antCode = doc.getElementsByTagName("AntennaCode").item(0).getNodeName();
            lcMetadata.setAntenneCode(antCode);

                        *//** get product Id meta-data  *//*
            final String productId = doc.getElementsByTagName("ProductID").item(0).getNodeName();
            lcMetadata.setProductId(productId);

                         *//** get comment meta-data  *//*
            final String comment = doc.getElementsByTagName("Comment").item(0).getNodeName();
            lcMetadata.setComment(comment);

                          *//** get level meta-data  *//*
            final String level = doc.getElementsByTagName("Level").item(0).getNodeName();
            lcMetadata.setLevel(level);

                           *//** get type meta-data  *//*
            final String type = doc.getElementsByTagName("Type").item(0).getNodeName();
            lcMetadata.setType(type);

                            *//** get product Version meta-data  *//*
            final String prodVer = doc.getElementsByTagName("ProductVersion").item(0).getNodeName();
            lcMetadata.setProductVersion(prodVer);

                             *//** get satellite meta-data  *//*
            final String soVer = doc.getElementsByTagName("SoftwareVersion").item(0).getNodeName();
            lcMetadata.setSoftwareVersion(soVer);

                              *//** get product MD5 meta-data  *//*
            final String productMD5 = doc.getElementsByTagName("ProductMD5").item(0).getNodeName();
            lcMetadata.setProductMD5(productMD5);

            final NodeList nList = doc.getElementsByTagName("File");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                final Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    final Element eElement = (Element) nNode;
                    log.debug("index:{} ", eElement.getAttribute("index"));
                }
            }

                               *//** get total Packet meta-data  *//*
            final String totalpkt = doc.getElementsByTagName("TotalPackets").item(0).getNodeName();
            lcMetadata.setTotalPAcket(totalpkt);

                                *//** get valid Packet meta-data  *//*
            final String validPkt = doc.getElementsByTagName("ValidPackets").item(0).getNodeName();
            lcMetadata.setValidPacket(validPkt);

                                 *//** get packet Length meta-data  *//*
            final String pktLength = doc.getElementsByTagName("PacketLength").item(0).getNodeName();
            lcMetadata.setPacketLength(pktLength);

                                  *//** get Has Gaps meta-data  *//*
            final String hasGaps = doc.getElementsByTagName("HasGaps").item(0).getNodeName();
            lcMetadata.setHasGaps(hasGaps);

                                   *//** get Gaps Percent meta-data  *//*
            final String gapsPer = doc.getElementsByTagName("GapsPercent").item(0).getNodeName();
            lcMetadata.setGapsPercent(gapsPer);

                                    *//** get quality Level meta-data  *//*
            final String qualLevel = doc.getElementsByTagName("QualityLevel").item(0).getNodeName();
            lcMetadata.setQualityLevel(qualLevel);

                                     *//** get organization meta-data  *//*
            final String org = doc.getElementsByTagName("Organization").item(0).getNodeName();
            lcMetadata.setOrganization(org);

                                      *//** get email meta-data  *//*
            final String email = doc.getElementsByTagName("Email").item(0).getNodeName();
            lcMetadata.setEmail(email);

                                       *//** get tel meta-data  *//*
            final String tel = doc.getElementsByTagName("Tel").item(0).getNodeName();
            lcMetadata.setTel(tel);

                                        *//** get fax meta-data  *//*
            final String fax = doc.getElementsByTagName("Fax").item(0).getNodeName();
            lcMetadata.setFax(fax);

                                         *//** get address meta-data  *//*
            final String address = doc.getElementsByTagName("Address").item(0).getNodeName();
            lcMetadata.setAddress(address);*/
        }
        catch (SAXException | IOException | ParserConfigurationException e) {
            log.error("exception {}", e);
        }
        return lcMetadata;
    }

}
