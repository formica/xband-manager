/**
 * 
 */
package fr.svom.xband.data.repository.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.xband.data.exceptions.BandxServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("bandxTransferFrameFiltering")
public class BandxTransferFrameFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(BandxTransferFrameFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws BandxServiceException {
        // Init expression.
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                    + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("id".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("lcfilename".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression lcfilename = BandxTransferFramePredicates
                        .hasLcfileName(searchCriteria.getValue().toString());
                expressions.add(lcfilename);
            }
            else if ("packettime".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression packetTimexthan = BandxTransferFramePredicates
                        .isPacketTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(packetTimexthan);
            }
            else if ("isframevalid".equals(key)) {
                // Filter based on frame validity.
                final BooleanExpression isFrameValid = BandxTransferFramePredicates
                        .isFrameValid(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameValid);
            }
            else if ("isframeduplicate".equals(key)) {
                // Filter based on frame is duplicate.
                final BooleanExpression isFrameDuplicate = BandxTransferFramePredicates
                        .isFrameDuplicate(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameDuplicate);
            }
            else if ("station".equals(key)) {
                // Filter based on station.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasStationId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("orbit".equals(key)) {
                // Filter based on Orbit.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasOrbitId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasPacketApid(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsid".equals(key)) {
                // Filter based on obsid Type.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasObsid(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidtype".equals(key)) {
                // Filter based on obsid Type.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidnum".equals(key)) {
                // Filter based on obsid Num.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("packetid".equals(key)) {
                // Filter based on packet id.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .isPacketIdXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("tarfilename".equals(key)) {
                // Filter based on tar file name.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasTarFileNameLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("category".equals(key)) {
                // Filter based on Category of the APID.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasCategoryLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("instrument".equals(key)) {
                // Filter based on Instrument of the APID.
                final BooleanExpression exp = BandxTransferFramePredicates
                        .hasInstrumentLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else {
                log.warn("Unkown criteria key : {}", key);
            }
        }
        return expressions;
    }
}
