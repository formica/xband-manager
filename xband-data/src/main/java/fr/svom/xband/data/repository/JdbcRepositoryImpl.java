/**
 * This file is part of PhysCondDB.
 * <p>
 * PhysCondDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * PhysCondDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with PhysCondDB.  If not, see <http://www.gnu.org/licenses/>.
 **/
package fr.svom.xband.data.repository;

import fr.svom.xband.swagger.model.PacketApidIdDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * A repository for JDBC queries.
 *
 * @author formica
 */
@Slf4j
public class JdbcRepositoryImpl implements IJdbcRepository {

    /**
     * Datasource.
     */
    private final DataSource ds;

    /**
     * Default Ctor.
     *
     * @param ds the DataSource
     */
    public JdbcRepositoryImpl(final DataSource ds) {
        super();
        this.ds = ds;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getCountObsIdSummaryInfo(java.
     * lang.String, java.lang.Long, java.lang.Long, java.lang.Long)
     */
    @Override
    public List<PacketApidIdDto> findApidPacketByPassId(String station, Integer orbit) {
        // Create the jdbc template from the Datasource ds.
        log.info("Select apids and packetids by pass_id using JDBCTEMPLATE");
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select distinct xp.apid, xp.PACKET_ID, apid.name, "
                           + "pid.packet_name, pid.packet_notification "
                           + "from xband_packet xp, l0c_datfile l0c, "
                           + "xband_packet_id pid, xband_apid apid  "
                           + "where xp.lcfile_id=l0c.lcfile_id and "
                           + "xp.apid=apid.apid and xp.packet_id=pid.packet_id "
                           + "and l0c.station=? and l0c.orbit=?";

        // Add group by expression and order by.
        final String orderby = " order by xp.apid ASC, xp.PACKET_ID ASC";
        log.info("Using arguments station={} orbit={}", station, orbit);
        /**
         * Tested request: select distinct xp.apid, xp.PACKET_ID, apid.name,
         * pid.packet_name, pid.packet_notification
         * from xband_packet xp, l0c_datfile l0c, xband_packet_id pid, xband_apid apid
         * where xp.lcfile_id=l0c.lcfile_id and xp.apid=apid.apid and
         * xp.packet_id=pid.packet_id and l0c.station='KR00P' and l0c.orbit=493;
         */
        return jdbcTemplate.query(sql + orderby,
                (rs, num) -> {
                    final PacketApidIdDto entity = new PacketApidIdDto();
                    entity.setApidValue(rs.getInt("apid"));
                    entity.setPacketId(rs.getInt("packet_id"));
                    entity.setPacketName(rs.getString("packet_name"));
                    entity.setName(rs.getString("name"));
                    entity.setPacketNotification(rs.getString("packet_notification"));
                    return entity;
                }, station, orbit);
    }
}
