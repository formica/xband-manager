#### Author: E.Trigui
##### Date of last development period: 2018/05/16

## Description
This subproject consists on a x-band packets data manager which allows to create a band-x database.

## dependance
The project use the [`svom-packet-decoding library`](https://drf-gitlab.cea.fr/svom/svom-packet-decoding) To decode packets.

## Overview
This project uses [Spring](https://spring.io/projects/spring-boot) and [hibernate](http://hibernate.org/) frameworks and [Orika](https://github.com/orika-mapper/orika) for the mapping.

## Database
The database connections are defined in the file `./BandxMgrData/src/main/resources/application.yml`. 
This file present different set of properties which are chosen by selecting a specific spring profile when running the server. The file should be edited if you are administering the conditions database in order to provide an appropriate set of parameters.

For more details about the xbandMgr Database, see the [DB scheme](https://drf-gitlab.cea.fr/svom/xband/xband-manager/wikis/xbandmgrDB)