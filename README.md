#       Vhf Manager

#### Author: A.Formica, E.Trigui
##### Date of last development period: 2021/12/20
```
   Copyright (C) 2015  A.Formica, E.Trigui

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

# Table of Contents
1. [Description](#description)
1. [OpenApi](#openapi)
2. [Installation](#installation)
3. [Build instructions](#build-instructions)
4. [Run the server](#run-the-server)
4. [Server fonctionalities](#server-fonctionalities)
4. [Deployment](#deployment)
6. [General information](#general-information)

## Description
This is a project for x-band pre-processing system using swagger generated server stubs.

The project contains two mains subprojects: 
* [xband-data](https://drf-gitlab.cea.fr/svom/xband/xband-manager/tree/master/xband-data/README.md): an x-band packets data manager which allows to create, update and select information from
  a dedicated database schema. A set of sql tools to manage the tables is available in the `sql` directory.
* [xband-web](https://drf-gitlab.cea.fr/svom/xband/xband-manager/tree/master/xband-web/Readme.md) a REST API that expose web services for x-band data processing.

Note that the project uses the [jar file](https://drf-gitlab.cea.fr/svom/common/svom-ccsds-decoder) which is loaded as a submodule.

In both sub-projects the directory structure is the same:
```
src/gen/java
src/main/java
src/main/resources
src/test/java
src/test/resources
```
The *java* directories contain java code. The *resources* directory contain relevant configuration files.
The essential configuration is in the *xband-web/src/main/resources* : here all *application-xxx.yml* files
are present, and allow to start the server with the desired set of configuration parameters, concerning security, database and other things.

The external dependencies of the project when it is deployed at FSC are:
```
postgres database service
nats messaging service
```
For testing the system you do not need postgres, because the default spring profile will use *h2* database.
NATS is also not strictly necessary.

## Openapi
These instructions are ment to be used by developers of the vhf manager application.
The file which contains the full API definition is:
```
./swagger_schemas/swagger/yaml/xband_full.yaml
```
This file can also be used by applications like the swagger-ui for visualization of the API.
### Openapi code generation
In order to regenerate the API we use the schemas and templates which are stored in the directories:

```
./swagger_schemas
     - templates
```

The server stub generation is implemented as a gradle task:

```
./gradlew openApiGenerate
```
The generated classes are in `api-generated/src/gen/java` and in case you want to change the server code these classes
will need to be copied into `xband-data/src/gen/java` or `xband-web/src/gen/java`.
You can do this using the following instructions:
```
scp ./api-generated/src/gen/java/fr/svom/xband/swagger/model/*java xband-data/src/gen/java/fr/svom/xband/swagger/model/
scp ./api-generated/src/gen/java/fr/svom/xband/server/swagger/api/*java xband-web/src/gen/java/fr/svom/xband/server/swagger/api/
```
You do not need to do this if you just want to use the server.

For generating a client (e.g. typescript) :
```
openapi-generator-cli generate -g typescript-axios -i ./swagger_schemas/swagger/yaml/xband_full.yaml -o out
```
This can then be installed via npm with command like:
```
npm install @openapitools/openapi-generator-cli -g
```

## Installation
Download the project from gitlab (example below is using _https_):
```
git clone git@drf-gitlab.cea.fr:svom/xband/xband-manager.git
```
This will create a directory _xband-manager_ in the location where you run the git command.

## Build instructions
You need to have **java >= 8** installed on your machine. 
This command generate a war (java web archive) file in  : `xband-web/build/libs/xband-manager.war` 

```
gradle clean build
```
In case gradle is not installed on your machine, you can run the wrapper delivered with the project:
```
./gradlew clean build
```
### checkStyle
This plugin is active by default. If something is not conform to the style chosen (a configuration is present in `./config/checkstyle` directory) then an error will appear and the project is not build.
Output html files allow to follow up on spotted bugs.
### jacoco
In order to check the test coverage you can do:
```
gradle build jacocoTestReport
```
Output html files will be stored in :
```
./xband-data/build/reports/jacoco/test/html/index.html
./xband-web/build/reports/jacoco/test/html/index.html
```

## Run the server
This section is under maintenance.

The server will use by default an embedded `undertow` web server.

The server need by definition to have a database connection in order to store the conditions data. The database connections are defined in the files `./xband-web/src/main/resources/application-<profile>.yml`. This file present different set of properties which are chosen by selecting a specific spring profile when running the server. The file should be edited if you are administering the xband database in order to provide an appropriate set of parameters.

If you do not have any remote database available you should use the default spring profile.

The set of default properties to run the server is defined in `config/application.properties` which will be read by spring when starting the server. The file there will use the `default` spring profile and a local database instance `h2database` where to store the data (it is a sort of `sqlite` file).
It is essential to set appropriate parameters in this file. An example
is provided to start up a default profile with local h2 database.

### Run on docker

* Create image:

To run the server on docker, you have first to create the xbandmgr image from this [Dokerfile](https://drf-gitlab.cea.fr/svom/xband/xband-manager/blob/master/Dockerfile) using this command:

```
docker build -t xbandmgr:1.0 .
```
* run docker container

Then you can run a container using this command

```
docker run -p 8080:8080  -d xbandmgr:1.0 
```

To view log files outside the container, you have to bind the local logs folder with `/home/svom/data/logs` in the container as shown in this example

```
docker run -p 8080:8080 \
           -v ${PWD}/logs:/home/svom/data/logs \
	       -d xbandmgr:1.0 
```
* Run on docker stack

you can run server on a docker stack: create the config through the create.config.sh [script](https://drf-gitlab.cea.fr/svom/xband/xband-manager/blob/master/create.config.sh), define secrets and then
execute this command to deploy stack through the docker-compose [file](https://drf-gitlab.cea.fr/svom/xband/xband-manager/blob/master/docker-compose.yml) 
````
docker stack deploy --with-registry-auth -c Docker-compose.yml xbandManager
````
## Server fonctionalities 
This [scheme](https://drf-gitlab.cea.fr/svom/xband/swagger-xband-mgr/wikis/xbandmgr-scheme) summarizes input/output and principal components of xbandmanager system.

For more detail on the differents web services proposed by the xbandmgr server, you can see [here](https://drf-gitlab.cea.fr/svom/xband/swagger-xband-mgr/tree/master/BandxMgrWeb#web-services-api).

### Input/Output

As `inpout` to the xband-manager program, the system expects to have .tar having the following format:

SVOM_L0C_<Type>_<APID>_<StartTime>_<EndTime>_<OrbitID>_<Station>_<Ver>.tar where:

* Type = data type acronyms, the value should be ED or SD, ED represents Engine Data, SD represents Science Dataand Science HK Data.

* APID = identifier of the filtered APID (replaced by “Packet Identifier” on 16 bits),4 HEX representation, extracted from packet header.

* StartTime = first TM packet date with the format YYYYMMDDThhmmss, UTC time.

* EndTime = last TM packet date with the format YYYYMMDDThhmmss, UTC time.

* OrbitID = satellite pass number acquired fromX-pass-plan, computed from the descending node with the format XXXXXX, starting from 1.

* Station = station acronyms, with the format KR000, HK000, SY4G1(station acronyms||antenna num||record system||channel num).

* Ver = the number of production (with the format YY) and the algorithm version (with the format 0-9, a-z), value range from YYX=000 to YYX=99z.

* Index= the file number, with the format X, value range from 1 to 9.It will be only 1 if the file is less than 2 GBytes.

It's also possible to have as input .dat file havinf the following format:

SVOM_L0C_<Type>_<APID>_<StartTime>_<EndTime>_<OrbitID>_<Station>_<Ver>_<Index>.dat

As `output`, the program generates .bin files and L0d .fits files :

**Output1: 

Each .bin  file corresponds to one xband-packet with size of 1022 bytes and having as name: 

.bin filename = the datfilename Without Extension + "_pkt_" + the number of the packet in the dat file + ".bin"

bin files are referenced at the xband Database as data url for each xband-Packet.

The .bin files directory is named as follow: apidHexa_StartTime.

Exemple of .bin file path: /BIN/0654_20210201T071559/SVOM_L0C_ED_0654_20210201T071559_20210201T124900_000525_KR00P_000_1_pkt_99.bin

### Notification

The xband pre-processing system interacts with `NATS` [messaging](https://drf-gitlab.cea.fr/svom/common-services/messaging) system in the following way: 

* It sends a notification message via a specified canal through **Nats streaming** to alert subscribers that a new L0C tar file has been processed.

The canal name is defined in the application.properties file as nats.listening paramater. Actually this canal is **xband.data**.

{
         "message_class": "DataNotification",
         "message_date": "???",
             "content":  {   
                     "source":"XDB",   
                     "data_type":"L0D",   
                     "date":"",   
                     "message": "a new l0d fits file is available via API REST",   
                     "obsid":""
                     "resource_locator":"tarfilename"   
                }
}   

## Deployment

Example of xband-manager docker stack deployment at the _FSC_ network:

The `xbandmgr` service is a web server which exposes endpoints to manage _XBAND data_.

The xbandmgr [docker image](https://drf-gitlab.cea.fr/svom/xband/xband-manager#run-on-docker) 
is depoyed in svomtest Registry (for more detail see this [doc](https://drf-gitlab.cea.fr/svom/common/Tools/-/wikis/Svom-docker-registry));

To start the docker stack we must define this yml file:
* **docker-compose.yml** : starts the servie _xbandmgr-http_ on _http_ and port **8080**

It depends on on **postgres** and **nats** and the following configurations: 

* **xband-conf** : application.properties file

### Deployment instructions
This section is work in progress. Probably not needed.

1. create xbandmgr docker image.
2. change configuration in application.properties file
3. create xband-conf by executing create-config.sh 
```
./create.config.sh
```
4. create secrets 'nats_password' and 'postgres_password'
```
echo "nats password" | docker secret create nats_password -
echo "postgres password" | docker secret create postgres_password -
```
5. start [postgres service](https://drf-gitlab.cea.fr/svom/xband/xband-manager/-/blob/master/docker-compose-postgres.yml) 
6. start the stack

```
docker stack deploy --with-registry-auth -c docker-compose.yml xbandmgr
```

For more details about xbandmgr `deployment` see [here](https://drf-gitlab.cea.fr/svom/common/svom-deployment/tree/master/swarm/xband-system)

For more details about xbandmgr `test deploiement` see [here](https://drf-gitlab.cea.fr/svom/data-challenge/-/tree/master/dc2_tests/xband_tests)

## General information
Some general instructions or information on the project.

### CCSDS decoding library
The jar file used is in common with vhf-manager and hosted in a submodule of this project.

### Check application 

To verify that xbandmgr server is running, check if we obtain response from this url : 
`http://yourServerAdress:port/api/frames`

For more detail on the differents web services proposed by the xbandmgr server, you can see [here](https://drf-gitlab.cea.fr/svom/xband/swagger-xband-mgr/tree/master/BandxMgrWeb#web-services-api).

### Simulation Tests

To execute some simulation Tests, you can see [here](https://drf-gitlab.cea.fr/svom/common/Tools/-/wikis/Xband-manager-application#simulation-tests)

### Useful commands
Select a bytea column as an hexa string in Postgres: 
```dtd
select encode(packet::bytea, 'hex') as pkt from xband_binary_packet;
```
For most of the actions against the REST API it is better to use the messaging project `xbanddbio.py`.