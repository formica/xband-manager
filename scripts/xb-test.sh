function json_data() {
  cat <<EOF
{ "" }
EOF
}

generate_apid1617_data()
{
  cat <<EOF
{
  "apidValue":  1617, "category": "Science", "instrument":  "ECL", "pcat": 20, "pid": 18, "tid": 1
}
EOF
}
generate_apid1620_data()
{
  cat <<EOF
{
  "apidValue":  1620, "category": "AAVPVT", "instrument":  "ECL", "pcat": 20, "pid": 18, "tid": 1
}
EOF
}
generate_apid1588_data()
{
  cat <<EOF
{
  "apidValue":  1588, "category": "AAVPVT", "instrument":  "PDPU", "pcat": 20, "pid": 17, "tid": 1
}
EOF
}
generate_apid1649_data()
{
  cat <<EOF
{
"apidValue":  1649, "category": "Science", "instrument":  "MXT", "pcat": 17, "pid": 19, "tid": 1
}
EOF
}

generate_packet_id_data()
{
  cat <<EOF
{
  "packetId": 30,
  "apid": $(generate_apid1620_data)
}
EOF
}

function get_data() {
  echo "Execute $1 : get data of type $2 from server using search $3"

  resp=`curl -X GET -H "Accept: application/json" -H "Content-Type: application/json" "${host}/${apiname}/$2?by=$3"`
  echo "Received response "
  echo $resp | json_pp
}

function post_data() {
  pdata=$2
  echo "Execute: curl -X POST -H \"Accept: application/json\" -H \"Content-Type: application/json\" \"${host}/${apiname}/$1\" --data \"${pdata}\""
  resp=`curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" "${host}/${apiname}/$1" --data "${pdata}"`
  echo "Received response "
  echo $resp | json_pp
}

function post_tar_data() {
  dtype=$1
  source=$2
  fname=$3

  resp=`curl -H "Content-Type: multipart/form-data" -H "dataType: ${dtype}" -H "source: ${source}" --form lcFileName="${fname}"  --form "file=@${fname}" "${host}/${apiname}/frames"`

  echo "Received response $resp"
  echo $resp | json_pp
}


function create_apids() {
  echo "Execute $1 : create apids..."
  tdata="$(generate_apid1620_data)"
  echo "Upload ${tdata}"
  echo ${tdata} > .apid
  post_data "apids" "@.apid"
  tdata="$(generate_apid1588_data)"
  echo "Upload ${tdata}"
  echo ${tdata} > .apid
  post_data "apids" "@.apid"
  tdata="$(generate_apid1649_data)"
  echo "Upload ${tdata}"
  echo ${tdata} > .apid
  post_data "apids" "@.apid"
  tdata="$(generate_apid1617_data)"
  echo "Upload ${tdata}"
  echo ${tdata} > .apid
  post_data "apids" "@.apid"
  tdata="$(generate_packet_id_data)"
  echo "Upload ${tdata} for apid 1620"
  echo ${tdata} > .pktid
  post_data "apids/1620/packetid" "@.pktid"
}

function upload_tar() {
  echo "Execute $1 : upload data from tar $2 "
  tarfile=$2
  post_tar_data "archive" "local" $2
}

###
# Main script
host=$1
apiname=$2
echo "Use host = $host apiname $apiname"
echo "Execute $3"
if [ "$host" == "help" ]; then
  echo "$0 <host> <apiname> <command>"
  echo "Use commands: create_apids, upload_tar.. "
  echo "get_data: <type> <search pattern>"
elif [[ "x$3" == "x" ]]; then
  echo "use arg help to get help."
else
  $3 "${@:3}"
fi
