#_______________________________________________________________________________
#
# Xbandmgr service
# Version xbandmgr-1.1
# Emna Trigui - November 2018
# Usage :
# docker build --build-arg svom_gid=${SVOM_GID} --build-arg svom_uid=${SVOM_UID}\
#             -t xbandmgr:2.1 .
# docker tag xbandmgr:2.1 svomtest.svom.fr:5543/xbandmgr:2.1
#_______________________________________________________________________________

# use an alpine image from docker-hub.
#FROM anapsix/alpine-java
FROM adoptopenjdk/openjdk11:alpine-jre

MAINTAINER Emna TRIGUI

ENV USR svom
ENV SVOM_UID 208
ENV SVOM_GROUPID 1090

ENV xbandmgr_version 1.1-SNAPSHOT
ENV xbandmgr_dir /home/${USR}/xbandmgr
ENV data_dir /home/${USR}/data
ENV gradle_version 5.2.1
ENV TZ GMT

RUN addgroup -g $SVOM_GROUPID $USR \
    && adduser -S -u $SVOM_UID -G $USR -h /home/$USR $USR

RUN mkdir -p ${xbandmgr_dir} \
  && mkdir -p ${data_dir}/logs \
  && mkdir -p ${data_dir}/packets \
  && mkdir -p ${data_dir}/upload

## This works if using an externally generated war, in the local directory
ADD ./xband-web/build/libs/xbandmgr.war ${xbandmgr_dir}/xbandmgr.war

ADD logback.xml /home/${USR}/logback.xml

##ADD application.properties /home/${USR}/config/application.properties
##RUN chown -R $USR:$USR /home/${USR}
VOLUME ${data_dir}
## Port
EXPOSE 8080

COPY ./entrypoint.sh /home/${USR}
# Use numeric IDs for owner:group
RUN chown -R $SVOM_UID:$SVOM_GROUPID /home/${USR}
## Set user and workdir
USER ${USR}
WORKDIR /home/${USR}
## Entrypoint
ENTRYPOINT  [ "./entrypoint.sh" ]
