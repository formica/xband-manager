#!/bin/sh

## -Dlogging.config=/data/logs/logback.xml
echo "Setting JAVA_OPTS from file javaopts.properties"
joptfile=./javaopts.properties
echo "use opt : "
cat $joptfile
if [ -e $joptfile ]; then
   export JAVA_OPTS=
   while read line; do JAVA_OPTS="$JAVA_OPTS -D$line"; done < $joptfile
fi
if [ -e ./create-properties.sh ]; then
### . ./create-properties.sh
   echo "Skip properties creation for secrets"
fi
if [ -z "$xbandmgr_dir" ]; then
  echo "set xbandmgr_dir...."
  xbandmgr_dir=$PWD/xband-web/build/libs
fi 

echo "$USER is starting server with JAVA_OPTS : $JAVA_OPTS from user directory $PWD using ${xbandmgr_dir}"
if [ x"$1" = x"" ]; then
    sh -c "java $JAVA_OPTS -jar ${xbandmgr_dir}/xbandmgr.war 2>>/tmp/err.log"
else
    sh -c "$@"
fi
